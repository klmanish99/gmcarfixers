<?php

require_once("utility/exception/UnauthorizedException.php");
require_once("utility/exception/DuplicateEntryException.php");

require_once("endpoint/BaseRestApi.php");
require_once("endpoint/UserApi.php");
require_once("endpoint/InquiryApi.php");
require_once("endpoint/Tech_inquiryApi.php");
require_once("endpoint/WorkshopApi.php");
require_once("endpoint/Towfixers_inquiryApi.php");
require_once("endpoint/DriverApi.php");
require_once("endpoint/TechnicianApi.php");
require_once("endpoint/CompanyApi.php");
require_once("endpoint/DealerApi.php");
require_once("endpoint/MailerApi.php");
require_once("endpoint/Temp_companyApi.php");

$method = $_SERVER['REQUEST_METHOD'];
$view = "";
$action = $_GET["action"];
$resource = $_GET["resource"];

// if($method == "GET") {
// 	$action = $_GET["action"];
// 	$resource = $_GET["resource"]; 
// } elseif ($method == "POST") {
// 	$action = $_GET["action"];
// 	$resource = $_GET["resource"];  
// } 

$baseRestApi = new BaseRestApi();
$userApi = new UserApi();
$inquiryApi = new InquiryApi();
$tech_inquiryApi = new Tech_inquiryApi();
$workshopApi = new WorkshopApi();
$towfixers_inquiryApi = new Towfixers_inquiryApi();
$driverApi = new DriverApi();
$technicianApi = new TechnicianApi();
$companyApi = new CompanyApi();
$temp_companyApi = new Temp_companyApi();
$dealerApi = new DealerApi();
$mailerApi = new MailerApi();

try
{
    /* controls the RESTful services URL mapping */
    switch($resource) {
    	case "user":
    		if($action == "create") {
    			$content = file_get_contents("php://input");
    			//Attempt to decode the incoming RAW post data from JSON.
    			$payload = json_decode($content);
    			$userApi->login($payload);
    		} elseif($action == "update") {
    		    $content = file_get_contents("php://input");
    		    $payload = json_decode($content);
    		    $userApi->change($payload);
    		}elseif($action == "delete") {
    			$userApi->logout();
    		} elseif($action == "list") {
    			$userApi->getSession();
    		}elseif ($action=="listbyid"){
    		    $userName = $_GET['id'];
    		    $userApi->getUserDetailsByID($userName);
    		}
    		break;
    	
    	case "inquiry":
    	    if($action == "list") {
    	        $inquiryApi->getInquiries();
    	    } elseif ($action == "create") {
    	        $content = file_get_contents("php://input");
    	        $payload = json_decode($content);
    	        $inquiryApi->insertInquiry($payload);
    	    } elseif ($action == "listbyid"){
    	        $id = $_GET['id'];
    	        $inquiryApi->getInquiryById($id);
    	    } elseif ($action == "update") {
    	        $id = $_GET['id'];
    	        $content = file_get_contents("php://input");
    	        $payload = json_decode($content);
    	        $inquiryApi->updateInquiry($id, $payload);
    	    } elseif ($action == "delete") {
    	        $id = $_GET['id'];
    	        $inquiryApi->deleteInquiry($id);
    	    } elseif ($action == "upload") {
    	        $id = $_GET['id'];
    	        $type = $_POST['type'];
    	        $signBy = $_POST['signBy'];
    	        $file_name = $_FILES['file']['name'];
    	        $file_tmp =$_FILES['file']['tmp_name'];
    	        $inquiryApi->uploadInquiry($id, $file_name, $file_tmp, $type, $signBy);
    	    } elseif ($action === "export") {
    	        $inquiryApi -> exportInquiries();
    	    }
    	    break;

        case "tech_inquiry":
            if($action == "list") {
                $tech_inquiryApi->getTech_inquiries();
            } elseif ($action == "create") {
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $tech_inquiryApi->insertTech_inquiry($payload);
            } elseif ($action == "listbyid"){
                $id = $_GET['id'];
                $tech_inquiryApi->getTech_inquiryById($id);
            } elseif ($action == "update") {
                $id = $_GET['id'];
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $tech_inquiryApi->updateTech_inquiry($id, $payload);
            } elseif ($action == "delete") {
                $id = $_GET['id'];
                $tech_inquiryApi->deleteTech_inquiry($id);
            } elseif ($action == "upload") {
                $id = $_GET['id'];
                $type = $_POST['type'];
                $signBy = $_POST['signBy'];
                $file_name = $_FILES['file']['name'];
                $file_tmp =$_FILES['file']['tmp_name'];
                $tech_inquiryApi->uploadTech_inquiry($id, $file_name, $file_tmp, $type, $signBy);
            } elseif ($action === "export") {
                $tech_inquiryApi -> exportTech_inquiries();
            }
            break;
    	   
    	case "workshop":
    	    if($action == "list") {
    	        $workshopApi->getWorkshops();
    	    } elseif ($action == "create") {
    	        $content = file_get_contents("php://input");
    	        $payload = json_decode($content);
    	        $workshopApi->insertWorkshop($payload);
    	    } elseif ($action == "listbyid"){
    	        $id = $_GET['id'];
    	        $workshopApi->getWorkshopById($id);
    	    } elseif ($action == "update") {
    	        $id = $_GET['id'];
    	        $content = file_get_contents("php://input");
    	        $payload = json_decode($content);
    	        $workshopApi->updateWorkshop($id, $payload);
    	    } elseif ($action == "delete") {
    	        $id = $_GET['id'];
    	        $workshopApi->deleteWorkshop($id);
    	    }
    	    break;

        case "towfixers_inquiry":
            if($action == "list") {
                $towfixers_inquiryApi->getTowfixers_inquiries();
            } elseif ($action == "create") {
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $towfixers_inquiryApi->insertTowfixers_inquiry($payload);
            } elseif ($action == "listbyid"){
                $id = $_GET['id'];
                $towfixers_inquiryApi->getTowfixers_inquiryById($id);
            } elseif ($action == "update") {
                $id = $_GET['id'];
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $towfixers_inquiryApi->updateTowfixers_inquiry($id, $payload);
            } elseif ($action == "delete") {
                $id = $_GET['id'];
                $towfixers_inquiryApi->deleteTowfixers_inquiry($id);
            }
            break;
    	   
    	case "driver":
    	    if($action == "list") {
    	        $driverApi->getDrivers();
    	    } elseif ($action == "create") {
    	        $content = file_get_contents("php://input");
    	        $payload = json_decode($content);
    	        $driverApi->insertDriver($payload);
    	    } elseif ($action == "listbyid"){
    	        $id = $_GET['id'];
    	        $driverApi->getDriverById($id);
    	    } elseif ($action == "update") {
    	        $id = $_GET['id'];
    	        $content = file_get_contents("php://input");
    	        $payload = json_decode($content);
    	        $driverApi->updateDriver($id, $payload);
    	    } elseif ($action == "delete") {
    	        $id = $_GET['id'];
    	        $driverApi->deleteDriver($id);
    	    } elseif ($action == "upload") {
    	        $id = $_GET['id'];
    	        $type = $_POST['type'];
    	        $file_name = $_FILES['file']['name'];
    	        $file_tmp =$_FILES['file']['tmp_name'];
	            $driverApi->uploadDriver($id, $file_name, $file_tmp, $type);
    	    }
    	    break;
        case "technician":
            if($action == "list") {
                $technicianApi->getTechnicians();
            } elseif ($action == "create") {
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $technicianApi->insertTechnician($payload);
            } elseif ($action == "listbyid"){
                $id = $_GET['id'];
                $technicianApi->getTechnicianById($id);
            } elseif ($action == "update") {
                $id = $_GET['id'];
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $technicianApi->updateTechnician($id, $payload);
            } elseif ($action == "delete") {
                $id = $_GET['id'];
                $technicianApi->deleteTechnician($id);
            } elseif ($action == "upload") {
                $id = $_GET['id'];
                $type = $_POST['type'];
                $file_name = $_FILES['file']['name'];
                $file_tmp =$_FILES['file']['tmp_name'];
                $technicianApi->uploadTechnician($id, $file_name, $file_tmp, $type);
            }
            break;
    	case "company":
    	    if($action == "list") {
    	        $companyApi->getCompanies();
    	    } elseif ($action == "create") {
    	        $content = file_get_contents("php://input");
    	        $payload = json_decode($content);
    	        $companyApi->insertCompany($payload);
    	    } elseif ($action == "listbyid"){
    	        $id = $_GET['id'];
    	        $companyApi->getCompanyById($id);
    	    } elseif ($action == "update") {
    	        $id = $_GET['id'];
    	        $content = file_get_contents("php://input");
    	        $payload = json_decode($content);
    	        $companyApi->updateCompany($id, $payload);
    	    } elseif ($action == "delete") {
    	        $id = $_GET['id'];
    	        $companyApi->deleteCompany($id);
    	    }
    	    break;
        case "temp_company":
            if($action == "list") {
                $temp_companyApi->getTemp_companies();
            } elseif ($action == "create") {
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $temp_companyApi->insertTemp_company($payload);
            } elseif ($action == "listbyid"){
                $id = $_GET['id'];
                $temp_companyApi->getTemp_companyById($id);
            } elseif ($action == "update") {
                $id = $_GET['id'];
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $temp_companyApi->updateTemp_company($id, $payload);
            } elseif ($action == "delete") {
                $id = $_GET['id'];
                $temp_companyApi->deleteTemp_company($id);
            }
            break;
        case "dealer":
            if($action == "list") {
                $dealerApi->getDealers();
            } elseif ($action == "create") {
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $dealerApi->insertDealer($payload);
            } elseif ($action == "listbyid"){
                $id = $_GET['id'];
                $dealerApi->getDealerById($id);
            } elseif ($action == "update") {
                $id = $_GET['id'];
                $content = file_get_contents("php://input");
                $payload = json_decode($content);
                $dealerApi->updateDealer($id, $payload);
            } elseif ($action == "delete") {
                $id = $_GET['id'];
                $dealerApi->deleteDealer($id);
            } elseif ($action === "export") {
                $dealerApi -> exportDealers();
            }
            break;
    	case "mail":
    	    if($action == "customer") {
    	        $id = $_GET['id'];
    	        $mailerApi->sendCustomerCopy($id);
    	    } elseif($action === "workshop") {
    	        $id = $_GET['id'];
    	        $mailerApi->sendWorkshopCopy($id);
    	    }
    }
} catch(UnauthorizedException $e) {
    $baseRestApi->respond($e->getCode(), $e->getMessage());
}catch(DuplicateEntryException $e) {
    $baseRestApi->respond($e->getCode(), $e->getMessage());
}catch(Exception $e) {
    $baseRestApi->respond(500, $e->getMessage());
}
?>
