<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("utility/SessionManager.php");
require_once ("utility/exception/DuplicateEntryException.php");
require_once ("vo/Driver.php");
require_once ("dao/GenericDao.php");

class DriverApi extends BaseRestApi
{

    function __construct()
    {}

    public function getDrivers()
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $driverRows = $dao->getRows("driver", array(
            "where" => array(
                "idn_company" => $session['cmp-id'],
                "ind_active" => 1
            ),
            "order_by" => "nme_driver asc"
        ));

        $driverBO = Driver::getDriverBO($driverRows);

        parent::respond(200, json_encode($driverBO));
    }

    public function insertDriver($driver)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $existingUserNameRows = $dao->getRows("user", array(
            'where' => array(
                'nme_user' => $driver->userName
            )
        ));

        if (! empty($existingUserNameRows)) {
            throw new DuplicateEntryException("User Name already exists");
        }
        $str = str_replace("/","-",$driver->dob);
        $dob = date('Y-m-d',strtotime($str));
        $userId = $dao->insert("user", array(
            'nme_user' => $driver->userName,
            'txt_pwd' => $driver->password,
            'txt_role' => 'Driver'
        ));
        $driverId = $dao->insert("driver", array(
            'nme_driver' => $driver->driverName,
            'idn_user' => $userId,
            'idn_company' => $session['cmp-id'],
            'ind_active' => 1,
            'txt_email' => $driver->email,
            'txt_mobile' => $driver->mobileNumber,
            'txt_alternateno' => $driver->alternateNo,
            'dt_dob' => $dob,
            'dt_added' => date("Y-m-d H:i:s"),
            'txt_address' => $driver->address,
            'dt_modified' => date("Y-m-d H:i:s")
        ));

        $_response = array(
            'driverId' => $driverId
        );

        parent::respond(200, json_encode($_response));
    }

    public function getDriverById($driverId)
    {
        $session = SessionManager::getSession();

        $driverRow = self::getById($driverId);

        $_response = Driver::getDriverBO($driverRow);

        parent::respond(200, json_encode($_response));
    }

    public function updateDriver($driverId, $driver)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $str = str_replace("/","-",$driver->dob);
        $dob = date('Y-m-d',strtotime($str));
        $dao->update("driver", array(
            'nme_driver' => $driver->driverName,
            'txt_email' => $driver->email,
            'txt_mobile' => empty($driver->mobileNumber) ? null : $driver->mobileNumber,
            'txt_alternateno' => empty($driver->alternateNo) ? null : $driver->alternateNo,
            'dt_dob' => $dob,
            'txt_address' => $driver->address,
            'dt_modified' => date("Y-m-d H:i:s")
        ), array(
            'idn_driver' => $driverId
        ));

        $_response = array(
            'message' => "Updated Driver"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteDriver($driverId)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $dao->update("driver", array(
            'ind_active' => 0
        ), array(
            'idn_driver' => $driverId
        ));

        $_response = array(
            'message' => "Deleted Driver"
        );

        parent::respond(200, json_encode($_response));
    }

    public function uploadDriver($driverId, $fileName, $fileTmp, $type)
    {
        $session = SessionManager::getSession();
        if (! isset($fileName) || empty($fileName)) {
            throw new Exception("Please select an image of size less than 2 MB!");
        }

        $uploadDir = "../resources/uploads/driver/";

        $dao = new GenericDao();

        switch ($type) {
            case "profile":
                $fileName = "" . $driverId . "_profile_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);

                $dao->update("driver", array(
                    'url_profile' => "resources/uploads/driver/" . $fileName
                ), array(
                    'idn_driver' => $driverId
                ));
                break;

            case "license":
                $fileName = "" . $driverId . "_license_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);

                $dao->update("driver", array(
                    'url_license' => "resources/uploads/driver/" . $fileName
                ), array(
                    'idn_driver' => $driverId
                ));
                break;

            case "idproof":
                $fileName = "" . $driverId . "_idproof_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);

                $dao->update("driver", array(
                    'url_idproof' => "resources/uploads/driver/" . $fileName
                ), array(
                    'idn_driver' => $driverId
                ));
                break;
        }

        $_response = array(
            'driverId' => $driverId,
            'fileName' => "resources/uploads/driver/" . $fileName
        );

        parent::respond(200, json_encode($_response));
    }

    public function getById($id)
    {
        $dao = new GenericDao();

        $driverRow = $dao->getRows("driver", array(
            "where" => array(
                "idn_driver" => $id,
                "ind_active" => 1
            )
        ));

        return $driverRow;
    }
}

?>