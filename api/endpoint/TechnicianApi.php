<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("utility/SessionManager.php");
require_once ("utility/exception/DuplicateEntryException.php");
require_once ("vo/Technician.php");
require_once ("dao/GenericDao.php");

class TechnicianApi extends BaseRestApi
{

    function __construct()
    {}

    public function getTechnicians()
    {
        $session = SessionManager::getSession();
        $dao = new GenericDao();
        $technicianRows = $dao->getRows("technician", array(
            "where" => array(
                "dealer_id" => $session['user-id'],
                "ind_active" => 1
            ),
            "order_by" => "nme_technician asc"
        ));

        $technicianBO = Technician::getTechnicianBO($technicianRows);

        parent::respond(200, json_encode($technicianBO));
    }

    public function insertTechnician($technician)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $existingUserNameRows = $dao->getRows("user", array(
            'where' => array(
                'nme_user' => $technician->userName
            )
        ));

        if (! empty($existingUserNameRows)) {
            throw new DuplicateEntryException("User Name already exists");
        }
        //echo "<pre>"; print_r($technician);exit;
        $str = str_replace("/","-",$technician->dob);
        $dob = date('Y-m-d',strtotime($str));
        $userId = $dao->insert("user", array(
            'nme_user' => $technician->userName,
            'txt_pwd' => $technician->password,
            'txt_role' => 'Technician'
        ));
        $technicianId = $dao->insert("technician", array(
            'nme_technician' => $technician->technicianName,
            'idn_user' => $userId,
            'dealer_id' => $technician->dealer_id,
            'idn_company' => '0',
            'ind_active' => 1,
            'txt_email' => $technician->email,
            'txt_mobile' => $technician->mobileNumber,
            'txt_alternateno' => $technician->alternateNo,
            'dt_dob' => $dob,
            'dt_added' => date("Y-m-d H:i:s"),
            'txt_address' => $technician->address,
            'dt_modified' => date("Y-m-d H:i:s")
        ));

        $_response = array(
            'technicianId' => $technicianId
        );

        parent::respond(200, json_encode($_response));
    }

    public function getTechnicianById($technicianId)
    {   //echo "in"; exit;
        $session = SessionManager::getSession();

        $technicianRow = self::getById($technicianId);

        $_response = Technician::getTechnicianBO($technicianRow);

        parent::respond(200, json_encode($_response));
    }

    public function updateTechnician($technicianId, $technician)
    {
        $session = SessionManager::getSession();
        $str = str_replace("/","-",$technician->dob);
        $dob = date('Y-m-d',strtotime($str));

        $dao = new GenericDao();
        $dao->update("technician", array(
            'nme_technician' => $technician->technicianName,
            'txt_email' => $technician->email,
            'txt_mobile' => empty($technician->mobileNumber) ? null : $technician->mobileNumber,
            'txt_alternateno' => empty($technician->alternateNo) ? null : $technician->alternateNo,
            'dt_dob' => $dob,
            'txt_address' => $technician->address,
            'dt_modified' => date("Y-m-d H:i:s")
        ), array(
            'idn_technician' => $technicianId
        ));

        $_response = array(
            'message' => "Updated Technician"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteTechnician($technicianId)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $dao->update("technician", array(
            'ind_active' => 0
        ), array(
            'idn_technician' => $technicianId
        ));

        $_response = array(
            'message' => "Deleted Technician"
        );

        parent::respond(200, json_encode($_response));
    }

    public function uploadTechnician($technicianId, $fileName, $fileTmp, $type)
    {
        $session = SessionManager::getSession();
        if (! isset($fileName) || empty($fileName)) {
            throw new Exception("Please select an image of size less than 2 MB!");
        }

        $uploadDir = "../resources/uploads/technician/";

        $dao = new GenericDao();

        switch ($type) {
            case "profile":
                $fileName = "" . $technicianId . "_profile_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);

                $dao->update("technician", array(
                    'url_profile' => "resources/uploads/technician/" . $fileName
                ), array(
                    'idn_technician' => $technicianId
                ));
                break;

            case "license":
                $fileName = "" . $technicianId . "_license_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);

                $dao->update("technician", array(
                    'url_license' => "resources/uploads/technician/" . $fileName
                ), array(
                    'idn_technician' => $technicianId
                ));
                break;

            case "idproof":
                $fileName = "" . $technicianId . "_idproof_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);

                $dao->update("technician", array(
                    'url_idproof' => "resources/uploads/technician/" . $fileName
                ), array(
                    'idn_technician' => $technicianId
                ));
                break;
        }

        $_response = array(
            'technicianId' => $technicianId,
            'fileName' => "resources/uploads/technician/" . $fileName
        );

        parent::respond(200, json_encode($_response));
    }

    public function getById($id)
    {
        $dao = new GenericDao();

        $technicianRow = $dao->getRows("technician", array(
            "where" => array(
                "idn_technician" => $id,
                "ind_active" => 1
            )
        ));

        return $technicianRow;
    }
}

?>