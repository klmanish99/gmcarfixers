<?php
// 	require_once("./BaseRestApi.php");
// 	require_once("../dao/GenericDao.php");
// 	require_once("../utility/exception/ResourceNotFoundException.php");
// 	require_once("../utility/exception/UnauthorizedException.php");
	
	require_once("BaseRestApi.php");
	require_once("dao/GenericDao.php");
	require_once("utility/SessionManager.php");
	require_once("utility/exception/ResourceNotFoundException.php");
	require_once("utility/exception/UnauthorizedException.php");
	
	class UserApi extends BaseRestApi {

		function __construct() {		
				
		}	

		function getUser($userName) {
		    $dao = new GenericDao();
		    
		    $userRows = $dao -> getRows("user", array("where" => array("nme_user" => $userName)));
		    
		    if(empty($userRows)) {
		        throw new ResourceNotFoundException("User could not be found");
		    }
		    
		    return $userRows[0];
		}
		
		function getUserDetailsByID($userName)
		{
		    $user = self::getUser($userName);
		    $sql = "";
		    if($user['txt_role'] === "Driver") {
		      $sql = "select driver.txt_mobile, user.* from user inner join driver on user.idn_user = driver.idn_user where driver.idn_user = ". $user['idn_user'];   
		    } else if($user['txt_role'] === "Company") {
		       $sql = "select company.txt_mobile1, user.* from user inner join company on user.idn_user = company.idn_user where company.idn_user = ".$user['idn_user'];
		    }
		    
		    $userRows = array();
		    if($sql != "") {
		        $dao = new GenericDao();
		        $userRows = $dao -> executeSql($sql);
		        if(empty($userRows)) {
		            throw new ResourceNotFoundException("User could not be found");
		        }
		    }
		    
		    parent :: respond(200, json_encode($userRows[0]));
		}
		
		function verifyUserCredentials($user, $userName, $password) {
		    $result = false;
		    if($user['nme_user'] === $userName && $user['txt_pwd'] ===  $password) {
		        $result = true;
		    }
		    return $result;
		}
		
		public function getSession() {
		    session_set_cookie_params(86400);
		    session_start();
		    
		    $sessionObj["userName"] = "";
		    $sessionObj["role"] = "";
		    
		    if(!isset($_SESSION["user-id"])) {
		        parent :: respond(200, json_encode($sessionObj));
		    } else {
		        $sessionObj["userName"] = $_SESSION["user-name"];
		        $sessionObj["userId"] = $_SESSION["user-id"];
		        $sessionObj["role"] = $_SESSION["role"];
		        $sessionObj["dealer_id"] = $_SESSION["dealer_id"];
		        parent :: respond(200, json_encode($sessionObj));
		    }
		}
		
        public function login($payload) {
            try {
                $user = self::getUser($payload -> user);
                
                if(!self::verifyUserCredentials($user, $payload -> user, $payload -> password)) {
                    throw new UnauthorizedException("Invalid Username or password. Please try again!");
                } 
                
                $compId = '';
                $driverId = '';
                $dao = new GenericDao();
                if($user['txt_role'] === 'Driver') {
                    $driver = $dao -> getRows('driver', array('where' => array('idn_user' => $user['idn_user'])));
                    if(!empty($driver)) {
                        $compId = $driver[0]['idn_company'];
                        $driverId = $driver[0]['idn_driver'];
                    }
                } else {
                    $company = $dao -> getRows('company', array('where' => array('idn_user' => $user['idn_user'])));
                    if(!empty($company)) {
                        $compId = $company[0]['idn_company'];
                    }
                }
                
                SessionManager::setSession($user['idn_user'],$user['nme_user'], $driverId, $compId, $user['txt_role']);
                
                parent :: respond(200, json_encode($_SESSION));
                
            } catch(ResourceNotFoundException $e) {
                throw new UnauthorizedException("Invalid Username or old password. Please try again!");
            }
        }
        
        public function logout() {
            SessionManager::destroySession();
            parent :: respond(200, "");
        }
        
        public function change($payload) {
            
            try {
                
                $user = self::getUser($payload -> user);
                
                $compId = 1;
                $driverId = '';
                $dao = new GenericDao();
                
                $dao->update("user", array("txt_pwd" => $payload -> newPassword), array("nme_user" => $payload -> user));

                $_response = array(
                    'message' => "Updated User Credentials"
                );
                
                parent::respond(200, json_encode($_response));
            } catch(ResourceNotFoundException $e) {
                throw new UnauthorizedException("Invalid Username or password. Please try again!");
            }
        }
    }
?>