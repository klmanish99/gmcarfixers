<?php 

require_once ("endpoint/BaseRestApi.php");
require_once ("endpoint/InquiryApi.php");
require_once ("vo/Inquiry.php");
require_once ("endpoint/Tech_inquiryApi.php");
require_once ("vo/Tech_inquiry.php");

require("utility/mailer/pdftemplate/pdfCreator.php");
require_once("utility/mailer/sendmail.php");
require_once("utility/mailer/sendmailnew.php");

class MailerApi extends BaseRestApi
{
    function __construct()
    {}
    
    function sendCustomerCopy($inquiryId) {
	$inquiryApi = new InquiryApi();
	$inquiry = $inquiryApi->getById($inquiryId);
	$inquiryBO = Inquiry::getInquiryBO($inquiry);
        
        if($inquiryBO[0]['customerEmail'] != '')
        {
            $pdfDoc = self::createPdf($inquiryBO);
            $pdfDoc->output();
            $mailSender = new MailSender();
            $mailSender->send($pdfDoc->output("","S"),$inquiryBO[0]['customerEmail'],"Car Towing Job Sheet ".$inquiryBO[0]['carRegNo'], 
                "<p> Hi ".$inquiryBO[0]['customerName']. ', <br/> Attached hereby is the job sheet for your car tow. <br/> 
                Thank you for choosing Car Fixers. <br /> We are pleased to provide you the best service. <br/> Thanks, <br/>CarFixers');
            //parent::respond(200, $pdfDoc->output());
        }
    }

    function sendWorkshopCopy($inquiryId) {
	$inquiryApi = new InquiryApi();
        $inquiry = $inquiryApi->getById($inquiryId);
        $inquiryBO = Inquiry::getInquiryBO($inquiry);

        if($inquiryBO[0]['workshopEmail'] != '')
        {
            $pdfDoc = self::createPdf($inquiryBO);
            $pdfDoc->output();
            $mailSender = new MailSender();
            $mailSender->send($pdfDoc->output("","S"),$inquiryBO[0]['workshopEmail'],"Car Towing Job Sheet ".$inquiryBO[0]['carRegNo'],
                "<p> Hi,".' <br/> The car has been dropped at your workshop.<br/> Please find attached hereby is the job sheet. <br/>
             <br/> Thanks, <br/>CarFixers');
            //parent::respond(200, $pdfDoc->output());
        }

    } 
    function inquiryAssignedToTech($technician_id) {
        include 'dao/config.php';
        $getTechData = mysqli_query($con,"SELECT * FROM technician WHERE idn_technician = '".$technician_id."' ");
        $row = mysqli_fetch_assoc($getTechData);
        if ($row['txt_email'] != '') {
            $mailSender = new MailSenderNew();
            $mailSender->send(
                $row['txt_email'],
                "New Inquiry",
                "<p> Hi,".' <br/> New inquiry has been assigned to you.<br/> For more check here http://carfixers.in <br/> <br/> Thanks, <br/>CarFixers');
        }
    }

    function inquiryAssignedToComp($comp_id) {
        include 'dao/config.php';
        $getCompData = mysqli_query($con,"SELECT * FROM company WHERE idn_company = '".$comp_id."' ");
        $row = mysqli_fetch_assoc($getCompData);
        if ($row['txt_email'] != '') {
            $mailSender = new MailSenderNew();
            $mailSender->send(
                $row['txt_email'],
                "New Inquiry",
                "<p> Hi,".' <br/> New inquiry has been assigned to your company.<br/> For more check here http://carfixers.in <br/> <br/> Thanks, <br/>CarFixers');
        }
    }
    // function inqueryFromDealer($comp_id) {
    //     include 'dao/config.php';
    //     $getCompData = mysqli_query($con,"SELECT * FROM company WHERE idn_company = '".$comp_id."' ");
    // }

    function createPdf($inquiry) {
        
        //$pdf = new PDF('P','mm','A4');
        $pdf = new PDF($inquiry);
        //$pdf->inquiry = array('customerName' => 'Ronak Thakker');
	$pdf->AddPage();
	//$pdf->SetFont('Courier','I',22);
	//$pdf->Cell(75,30, $pdf->Image(__DIR__.'/../../../../resources/img/logo_3.png',15,12,65),'LTB', 0);

	//$pdf->Cell(115,10,'','LTR',2,'C');
	//$pdf->Cell(115,1,$pdf->inquiry['companyName'],'LR',2,'C');

	//$pdf->SetFont('Courier','I',16);
	//$pdf->Cell(115,10,$pdf->inquiry['companyNumber'],'LRB',0,'R');
        $pdf->SetFont('Arial','',12);
        $pdf->Cell(95,10,'Customer Name: '.$inquiry[0]['customerName'],'LT',0, 'B');
	$pdf->Cell(95,10,'Date: '.$inquiry[0]['caseDate'],'TR',1, 'B');
	$custContactNo = $inquiry[0]['customerMobile']!=""?$inquiry[0]['customerMobile']:$inquiry[0]['custDriverMobile'];
        $pdf->Cell(95,10,'Customer No.: 0'.$custContactNo,'L',0, 'B');
        $pdf->Cell(95,10,'Technician Name: '.$inquiry[0]['driverName'],'R',1, 'B');
        $pdf->Cell(95,10,'Car Brand: '.$inquiry[0]['make'],'L',0, 'B');
        $pdf->Cell(95,10,'Pickup Location: '.$inquiry[0]['pickup'],'R',1, 'B');
	$pdf->Cell(95,10,'Car Model: '.$inquiry[0]['model'],'L',0, 'B');

	if($inquiry[0]['driverArriveHour'] != '' && $inquiry[0]['driverArriveMin'] != '') {
		$dh = $inquiry[0]['driverArriveHour'] < 10 ? '0'.$inquiry[0]['driverArriveHour'] : $inquiry[0]['driverArriveHour'];
		$dm = $inquiry[0]['driverArriveMin'] < 10 ? '0'.$inquiry[0]['driverArriveMin'] : $inquiry[0]['driverArriveMin'];
		$pdf->Cell(95,10,'Reaching Time: '.$dh.':'.$dm,'R',1, 'B');
	} else {
		$pdf->Cell(95,10,'Reaching Time: --:--','R',1, 'B');
	}
        $pdf->Cell(95,10,'Car Registration No: '.$inquiry[0]['carRegNo'],'L',0, 'B');
        $pdf->Cell(95,10,'Drop Location: '.$inquiry[0]['workshopName'].', '.$inquiry[0]['drop'],'R',1, 'B');
	$pdf->Cell(95,10,'Odometer Reading: '.number_format($inquiry[0]['kmsReading']).' kms','LB',0, 'B');
        $pdf->Cell(95,10,'Delivery to Workshop: '.$inquiry[0]['jobCompletionTime'],'RB',1, 'B');
        // $pdf->Cell(95,10,'R7C1!',1,0, 'B');
        // $pdf->Cell(95,10,'R7C2!',1,1, 'B');
        
        $pdf->Ln(5);
        $pdf->Cell(190,110, $pdf->Image(__DIR__.'/../../resources/img/car_view_3.jpg',30,115,150),1);
        $pdf->Image(__DIR__.'/../../'.str_replace('\\\\\\','',$inquiry[0]['carInvPic']),-77,115,255);
        //$pdf->Cell(190,130, $pdf->Image('../../../../resources/uploads/inquiry/26_car_1541183942.png',-77,100,255), 1, 1);
        //$pdf->Ln(10);
        $pdf->Ln(110);
        $pdf->Cell(190,10,'Other Comments: '.$inquiry[0]['carInvComments'],1,1);
        $pdf->Ln(5);
        
        $pdf->Cell(95,10,'Customer Sign By: '.$inquiry[0]['custSignBy'],1,0, 'B');
	$pdf->Cell(95,10,'Workshop Sign By: '.$inquiry[0]['wsSignBy'],1,1, 'B');
	$cs = str_replace('\\\\\\','',$inquiry[0]['custSignPic']);

	if($cs != '')
		$pdf->Cell(95,30,$pdf->Image(__DIR__.'/../../'.$cs,30,250,30),1,0);
	else
		$pdf->Cell(95,30,'',1,0);
	$wsBy = str_replace('\\\\\\','',$inquiry[0]['workshopSignPic']);
	
	if($wsBy != '')
		$pdf->Cell(95,30,$pdf->Image(__DIR__.'/../../'.$wsBy,135,250,30),1,1);
	else
		$pdf->Cell(95,30,'',1,1);
        return $pdf;
    }
}

?>
