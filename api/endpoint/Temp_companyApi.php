<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("utility/SessionManager.php");
require_once ("vo/Temp_company.php");
require_once ("dao/GenericDao.php");

class Temp_companyApi extends BaseRestApi
{

    function __construct()
    {}

    public function getTemp_companies()
    {   //echo "in 1";exit;
        $session = SessionManager::getSession();
        $dao = new GenericDao();
        $temp_companyRows = $dao->getRows("temp_company", array(
            "where" => array(
                "ind_active" => 1
                //"dealer_id" => $session['user-id']
            )
        ));
        $temp_companyBO = Temp_company::getTemp_companyBO($temp_companyRows);
        parent::respond(200, json_encode($temp_companyBO));
    }

    public function insertTemp_company($temp_company)
    {   
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $existingUserNameRows = $dao->getRows("user", array(
            'where' => array(
                'nme_user' => $temp_company->userName
            )
        ));

        if (! empty($existingUserNameRows)) {
            throw new DuplicateEntryException("User Name already exists");
        }

        $userId = $dao->insert("user", array(
            'nme_user' => $temp_company->userName,
            'txt_pwd' => $temp_company->password,
            'txt_role' => 'Temp_company'
        ));

        $dao->insert("temp_company", array(
            'nme_company' => $temp_company->temp_companyName,
            'ind_active' => 1,
            'idn_user' => $userId,
            'txt_email' => $temp_company->email,
            'txt_mobile1' => $temp_company->mobileNumber1,
            'txt_mobile2' => $temp_company->mobileNumber2,
            'txt_mobile3' => $temp_company->mobileNumber3,
            'txt_mobile4' => $temp_company->mobileNumber4,
            'txt_mobile5' => $temp_company->mobileNumber5,
            'dealer_id'   => $temp_company->dealer_id,
            'txt_firm_type' => $temp_company->temp_companyType,
            'txt_address' => $temp_company->address,
            'txt_tnc' => $temp_company->tnc,
            'dt_added' => date("Y-m-d H:i:s"),
            'dt_modified' => date("Y-m-d H:i:s")
        ));

        $_response = array(
            "companyId" => $userId
        );

        parent::respond(200, json_encode($_response));
    }

    public function getTemp_companyById($temp_companyId)
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();

        $temp_companyRow = self::getById($temp_companyId);
        $dao = new GenericDao();
        $dao->insert("company", array(
            'nme_company'   => $temp_companyRow[0]['nme_company'],
            'dealer_id'     => $temp_companyRow[0]['dealer_id'],
            'ind_active'    => $temp_companyRow[0]['ind_active'],
            'idn_user'      => $temp_companyRow[0]['idn_user'],
            'txt_email'     => $temp_companyRow[0]['txt_email'],
            'txt_mobile1'   => $temp_companyRow[0]['txt_mobile1'],
            'txt_mobile2'   => $temp_companyRow[0]['txt_mobile2'],
            'txt_mobile3'   => $temp_companyRow[0]['txt_mobile3'],
            'txt_mobile4'   => $temp_companyRow[0]['txt_mobile4'],
            'txt_mobile5'   => $temp_companyRow[0]['txt_mobile5'],
            'txt_firm_type' => $temp_companyRow[0]['txt_firm_type'],
            'txt_address'   => $temp_companyRow[0]['txt_address'],
            'lat'           => $temp_companyRow[0]['lat'],
            'lng'           => $temp_companyRow[0]['lng'],
            'txt_tnc'       => $temp_companyRow[0]['txt_tnc'],
            'dt_added'      => $temp_companyRow[0]['dt_added'],
            'dt_modified'   => $temp_companyRow[0]['dt_modified']
        ));
        $dao->update("temp_company", array(
            'ind_active' => 0
        ), array(
            'idn_company' => $temp_companyId
        ));
        $query = mysqli_query($con,"SELECT idn_company, dealer_id FROM company ORDER BY idn_company DESC LIMIT 1 ");
        $last_id = mysqli_fetch_assoc($query);
        $dao->insert("company_to_dealer", array(
            'dealer_id'   => $last_id['dealer_id'],
            'company_id'  => $last_id['idn_company']
        ));
        $_response = Temp_company::getTemp_companyBO($temp_companyRow);
        parent::respond(200, json_encode($_response));
    }

    public function updateTemp_company($temp_companyId, $temp_company)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $dao->update("temp_company", array(
            'nme_company' => $temp_company->temp_companyName,
            'txt_email' => $temp_company->email,
            'txt_mobile1' => empty($temp_company->mobileNumber1) ? null : $temp_company->mobileNumber1,
            'txt_mobile2' => empty($temp_company->mobileNumber2) ? null : $temp_company->mobileNumber2,
            'txt_mobile3' => empty($temp_company->mobileNumber3) ? null : $temp_company->mobileNumber3,
            'txt_mobile4' => empty($temp_company->mobileNumber4)? null : $temp_company->mobileNumber4,
            'txt_mobile5' => empty($temp_company->mobileNumber5 )? null : $temp_company->mobileNumber5,
            'txt_firm_type' => $temp_company->temp_companyType,
            'txt_address' => $temp_company->address,
            'txt_tnc' => $temp_company->tnc,
            'dt_modified' => date("Y-m-d H:i:s")
        ), array(
            'idn_company' => $temp_companyId
        ));

        $_response = array(
            'message' => "Updated Temp_company"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteTemp_company($temp_companyId)
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $str = explode('123456789123456789', $temp_companyId);
        $temp_id = $str[0];
        $comp_id = $str[1];
        $query = mysqli_query($con,"SELECT dealer_id FROM temp_company WHERE idn_company = '".$temp_id."' ");
        $row = mysqli_fetch_assoc($query);

        $dao->insert("company_to_dealer", array(
            'dealer_id'   => $row['dealer_id'],
            'company_id'  => $comp_id
        ));

        $dao->update("temp_company", array(
            'ind_active' => 0
        ), array(
            'idn_company' => $temp_id
        ));

        $_response = array(
            'message' => "Company Assigned"
        );

        parent::respond(200, json_encode($_response));
    }

    public function move_companyById($id){
        $dao = new GenericDao();

        $temp_companyRow = $dao->getRows("temp_company", array(
            "where" => array(
                "idn_company" => $id,
                "ind_active" => 0
            )
        ));
        return $temp_companyRow;

    }
    public function getById($id)
    {
        $dao = new GenericDao();

        $temp_companyRow = $dao->getRows("temp_company", array(
            "where" => array(
                "idn_company" => $id,
                "ind_active" => 1
            )
        ));

        return $temp_companyRow;
    }

    public function getByUserId($userId)
    {
        $dao = new GenericDao();

        $temp_companyRow = $dao->getRows("temp_company", array(
            "where" => array(
                "idn_user" => $id,
                "ind_active" => 1
            )
        ));

        return $temp_companyRow;
    }
}

?>