<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("endpoint/MailerApi.php");
require_once ("endpoint/SmsApi.php");
require_once ("endpoint/CompanyApi.php");
require_once ("endpoint/WorkshopApi.php");
require_once ("utility/SessionManager.php");
require_once ("utility/exception/DuplicateEntryException.php");
require_once ("vo/Inquiry.php");
require_once ("vo/Company.php");
require_once ("vo/Workshop.php");
require_once ("dao/GenericDao.php");

class InquiryApi extends BaseRestApi
{

    function __construct()
    {}

    public function getInquiries()
    {
        $inquiries = self::getAllInquiries();
        //echo "<pre>"; print_r($inquiries);exit;
        parent::respond(200, json_encode($inquiries));
    }

    public function insertInquiry($inquiry)
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();
        if ($session['cmp-id'] != '') {
            $comp_id = $session['cmp-id'];
        }
        else{
            $comp_id = $inquiry->company_id;
            $query = mysqli_query($con,"SELECT nme_company, txt_mobile1 from company where idn_company = '".$comp_id."' ");
            foreach ($query as $value) {
                $companyName = $value['nme_company'];
                $phoneNum = $value['txt_mobile1'];
            }
            $msgToTech = "A new enquiry has been assigned to you.%0aCustomer Name: ".$inquiry->customerName."%0aCustomer phone number: ".$inquiry->customerMobile."%0aFor More Click http://carfixers.in";
            $msgToTech = str_replace(' ', '%20', $msgToTech);
            $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
            //echo $link;
            file($link);

            $mailerApi = new MailerApi();
            $mailerApi->inquiryAssignedToComp($comp_id);
        }
        if ($session['role'] == 'Dealer') {
            $amount = '0';
        }
        else{
            $amount = $inquiry->amount;
        }
        if ($session['role'] == 'Dealer') {
            $dealer_id = $session['user-id'];
        }
        elseif ($session['role'] == 'Company') {
            $sel = mysqli_query($con,"select dealer_id from company where idn_user = '".$session['user-id']."' ");
            $row = mysqli_fetch_array($sel);
            $dealer_id = $row['dealer_id'];
        }
        if ($inquiry->driverId != "") {
            $query = mysqli_query($con,"SELECT nme_driver, txt_mobile from driver where idn_driver = '".$inquiry->driverId."' ");
            foreach ($query as $value) {
                $driverName = $value['nme_driver'];
                $phoneNum = $value['txt_mobile'];
            }
            $msgToTech = "A new enquiry has been assigned to you.%0aFor More Click http://carfixers.in";
            $msgToTech = str_replace(' ', '%20', $msgToTech);
            $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
            //echo $link;exit;
            file($link);
        }
        if ($inquiry->workshopId != "") {
            $query = mysqli_query($con,"SELECT nme_workshop, txt_mobile from workshop where idn_workshop = '".$inquiry->workshopId."' ");
            foreach ($query as $value) {
                $workshopName = $value['nme_workshop'];
                $phoneNum = $value['txt_mobile'];
            }
            $msgToTech = "A new enquiry has been assigned to your Workshop.%0aFor More Click http://carfixers.in";
            $msgToTech = str_replace(' ', '%20', $msgToTech);
            $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
            //echo $link;exit;
            file($link);
        }
        $dao = new GenericDao();
        if ($inquiry->driverArriveHour == '') {
            $driverArriveHour = 0;
        } else {
            $driverArriveHour = $inquiry->driverArriveHour;
        }
        //echo "<pre>"; print_r($inquiry);exit;
        //echo "<pre>"; print_r($session);exit;
        $inquiryId = $dao->insert("inquiry", array(
            'idn_company' => $comp_id,
            'idn_driver' => $inquiry->driverId,
            'idn_workshop' => $inquiry->workshopId,
            'dt_casedate'=> $inquiry->caseDate,
            'num_driver_arrive_hr' => $driverArriveHour,
            'num_driver_arrive_min' => $inquiry->driverArriveMin,
            'txt_casetype'=> $inquiry->caseType,
            'txt_fileno'=>$inquiry->fileNo,
            'txt_casereference'=>$inquiry->caseReference,
            'txt_make'=>$inquiry->make,
            'txt_model'=>$inquiry->model,
            'int_kms'=>$inquiry->kmsReading,
            'txt_carregno'=>$inquiry->carRegNo,
            'txt_custname'=>$inquiry->customerName,
            'txt_custemail'=>$inquiry->customerEmail,
            'txt_custmobile'=>empty($inquiry->customerMobile)?null:$inquiry->customerMobile,
            'txt_custdrivermobile'=> empty($inquiry->custDriverMobile) ? null : $inquiry->custDriverMobile,
            'dt_added' => date("Y-m-d H:i:s"),
            'dt_modified' => date("Y-m-d H:i:s"),
            'txt_pickup'=>$inquiry->pickup,
            'txt_drop'=>$inquiry->drop,
            'txt_drivercomments'=>$inquiry->driverComments,
            'num_amount'=>$amount,
            'dealer_id'=>$dealer_id,
            'role'=>$inquiry->role
        ));

        $_response = array(
            'inquiryId' => $inquiryId
        );

        parent::respond(200, json_encode($_response));
    }

    public function getInquiryById($inquiryId)
    {   
        $session = SessionManager::getSession();

        $inquiryRow = self::getById($inquiryId);

        $_response = Inquiry::getInquiryBO($inquiryRow);
        
        parent::respond(200, json_encode($_response));
    }

    public function updateInquiry($inquiryId, $inquiry)
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();
        if ($session['cmp-id'] != '') {
            $comp_id = $session['cmp-id'];
        } else{
            if ($inquiry->company_id != "") {
                $comp_id = $inquiry->company_id;
                $query = mysqli_query($con,"SELECT nme_company, txt_mobile1 from company where idn_company = '".$comp_id."' ");
                foreach ($query as $value) {
                    $companyName = $value['nme_company'];
                    $phoneNum = $value['txt_mobile1'];
                }
                $msgToTech = "A new enquiry has been assigned to you.%0aCustomer Name: ".$inquiry->customerName."%0aCustomer phone number: ".$inquiry->customerMobile."%0aFor More Click http://carfixers.in";
                $msgToTech = str_replace(' ', '%20', $msgToTech);
                $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
                //echo $link;exit;
                file($link);
                
                $mailerApi = new MailerApi();
                $mailerApi->inquiryAssignedToComp($comp_id);
            } else {
                $comp_id = $inquiry->companyId;
            }
        }
        if ($session['role'] == 'Dealer') {
            $amount = '0';
        } else{
            $amount = $inquiry->amount;
        }
        if ($session['role'] == 'Dealer') {
            $dealer_id = $session['user-id'];
        } elseif ($session['role'] == 'Company') {
            $sel = mysqli_query($con,"select dealer_id from company where idn_user = '".$session['user-id']."' ");
            $row = mysqli_fetch_array($sel);
            $dealer_id = $row['dealer_id'];
        }
        $sql = mysqli_query($con,"SELECT idn_driver, idn_workshop FROM inquiry WHERE idn_inquiry = '".$inquiry->inquiryId."' ");
        foreach ($sql as $value) {
            $driverId = $value['idn_driver'];
            $wrkshpId = $value['idn_workshop'];
        }
        if ($inquiry->driverId != "" && $inquiry->driverId != $driverId) {
            $query = mysqli_query($con,"SELECT nme_driver, txt_mobile from driver where idn_driver = '".$inquiry->driverId."' ");
            foreach ($query as $value) {
                $driverName = $value['nme_driver'];
                $phoneNum = $value['txt_mobile'];
            }
            $msgToTech = "A new enquiry has been assigned to you.%0aFor More Click http://carfixers.in";
            $msgToTech = str_replace(' ', '%20', $msgToTech);
            $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
            //echo $link;exit;
            file($link);
        }
        if ($inquiry->workshopId != "" && $inquiry->workshopId != $wrkshpId) {
            $query = mysqli_query($con,"SELECT nme_workshop, txt_mobile from workshop where idn_workshop = '".$inquiry->workshopId."' ");
            foreach ($query as $value) {
                $workshopName = $value['nme_workshop'];
                $phoneNum = $value['txt_mobile'];
            }
            $msgToTech = "A new enquiry has been assigned to your Workshop.%0aFor More Click http://carfixers.in";
            $msgToTech = str_replace(' ', '%20', $msgToTech);
            $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
            //echo $link;exit;
            file($link);
        }
        //echo "<pre>"; print_r($inquiry);exit;
        $dao = new GenericDao();
        $dao->update("inquiry", array(
            'idn_driver' => $inquiry->driverId,
            'idn_workshop' => $inquiry->workshopId,
            'idn_company' => $comp_id,
            'ind_complete' => $inquiry->isComplete,
            'dt_casedate'=> $inquiry->caseDate,
            'txt_casetype'=> $inquiry->caseType,
            'num_driver_arrive_hr' => $inquiry->driverArriveHour,
            'num_driver_arrive_min' => $inquiry->driverArriveMin,
            'txt_fileno'=>$inquiry->fileNo,
            'txt_casereference'=>$inquiry->caseReference,
            'txt_make'=>$inquiry->make,
            'txt_model'=>$inquiry->model,
            'int_kms'=>$inquiry->kmsReading,
            'txt_carregno'=>$inquiry->carRegNo,
            'txt_custname'=>$inquiry->customerName,
            'txt_custemail'=>$inquiry->customerEmail,
            'txt_custmobile'=>empty($inquiry->customerMobile) ? null : $inquiry->customerMobile,
            'txt_custdrivermobile'=>empty($inquiry->custDriverMobile) ? null : $inquiry->custDriverMobile,
            'dt_modified' => date("Y-m-d H:i:s"),
            'txt_pickup'=>$inquiry->pickup,
            'txt_drop'=>$inquiry->drop,
            'txt_drivercomments'=>$inquiry->driverComments,
            'num_amount'=>$inquiry->amount
        ), array(
            'idn_inquiry' => $inquiryId
        ));

        $_response = array(
            'message' => "Updated Inquiry"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteInquiry($inquiryId)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $dao->update("inquiry", array(
            'ind_active' => 0
        ), array(
            'idn_inquiry' => $inquiryId
        ));

        $_response = array(
            'message' => "Deleted Inquiry"
        );

        parent::respond(200, json_encode($_response));
    }

    public function uploadInquiry($inquiryId, $fileName, $fileTmp, $type, $signBy)
    {
        $session = SessionManager::getSession();
        if (! isset($fileName) || empty($fileName)) {
            throw new Exception("Please select an image of size less than 2 MB!");
        }
        
        $uploadDir = "../resources/uploads/inquiry/";
        
        $dao = new GenericDao();
        
        switch ($type) {
            case "custSign":
                $fileName = "" . $inquiryId . "_custSign_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);
                
                $dao->update("inquiry", array(
                    'url_cust_sign' => "resources/uploads/inquiry/" . $fileName,
                    'txt_custsign_by' => $signBy
                ), array(
                    'idn_inquiry' => $inquiryId
                ));
                
                $mailerApi = new MailerApi();
		$mailerApi->sendCustomerCopy($inquiryId);

		$inquiryRow = self::getById($inquiryId);
		$inquiries = Inquiry::getInquiryBO($inquiryRow);
		$inquiry = $inquiries[0];

		$smsApi = new SmsApi();
		$sendto = $inquiry['customerMobile'] != ""? $inquiry['customerMobile'] : $inquiry['custDriverMobile'];

		$message = self::createMessage($inquiry, 'Pickup');
		$smsApi->sendMessage($message,$sendto);
                break;
                
            case "wsSign":
                $fileName = "" . $inquiryId . "_wsSign_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);
                
                $dao->update("inquiry", array(
                    'url_workshop_sign' => "resources/uploads/inquiry/" . $fileName,
                    'txt_wssign_by' => $signBy,
                    'dt_jobcompletiontime' => date("Y-m-d H:i:s")
                ), array(
                    'idn_inquiry' => $inquiryId
	    	));

		$mailerApi = new MailerApi();
                $mailerApi->sendWorkshopCopy($inquiryId);

		$inquiryRow = self::getById($inquiryId);
		$inquiries = Inquiry::getInquiryBO($inquiryRow);
		$inquiry = $inquiries[0];

		$smsApi = new SmsApi();
		$sendto = "";
		/*$sendto = $inquiry['customerMobile'] != "" ? $inquiry['customerMobile'] : customer['custDriverMobile'];*/

		$message = self::createMessage($inquiry, 'DeliveredToWs');
		
		if($sendto != ""){
			$smsApi->sendMessage($message,$sendto);
		}

		$companyApi = new CompanyApi();
	        $company = $companyApi->getCompanyById($inquiry['companyId']);

		if($company[0]['mobileNumber1'] != ""){
			$smsApi->sendMesssage($message, $company[0]['mobileNumber1']);
		}

                break;
                
            case "carInv":
                $fileName = "" . $inquiryId . "_car_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);
                
                $dao->update("inquiry", array(
                    'url_car' => "resources/uploads/inquiry/" . $fileName,
                    'txt_inventorycomments' => $signBy
                ), array(
                    'idn_inquiry' => $inquiryId
                ));
                break;
        }
        
        $_response = array(
            'inquiryId' => $inquiryId,
	    'fileName' => "resources/uploads/inquiry/" . $fileName,
	    'message' => $message,
	    'sentTo' => $sendto
        );
        
        parent::respond(200, json_encode($_response));
    }
    
    public function exportInquiries() 
    {
        $inquiries = self::getAllInquiries();
        
        $fp = fopen(__DIR__ . '/../../resources/tmp/inquiries.csv', 'w'); #WINDOWS fopen(__DIR__ . '\..\..\resources\tmp\inquiries.csv', 'w');
        
        $delimiter = ',';
        $csvHeaders = ["File Number", "Case Date", "Case Type", "Reference", "Driver Name", "Workshop Name", "Customer Name", "Make", "Model", "Car Registration Number", "Customer Mobile", "Customer Email", "Pickup Location", "Drop Location"];
        
        // Save header
        $x = fputcsv($fp, $csvHeaders, $delimiter);
        
        if(count($inquiries) > 0) {
            
            // Save Data
            foreach ($inquiries as $elem) {
                $y = fputcsv($fp, Inquiry::getExportProperties($elem), $delimiter);
            }
        }
        
        // output headers so that the file is downloaded rather than displayed
        header("Content-type: text/csv");
        header("Content-disposition: attachment; filename = inquiries.csv");
        # WINDOWS readfile(__DIR__ . '\..\..\resources\tmp\inquiries.csv');
        readfile(__DIR__ . '/../../resources/tmp/inquiries.csv');
    }
    
    
    public function getById($id)
    {
        $dao = new GenericDao();

    //    $inquiryRow = $dao->getRows("inquiry", array(
    //        "where" => array(
    //            "idn_inquiry" => $id
    //        )
	//    ));
	//
	$inquiryRow = $dao->getDetailedInquiry(array(
            "i.idn_inquiry" => $id
        ), '');
        //echo "<pre>"; print_r($inquiryRow);exit;
        return $inquiryRow;
    }

    public function createMessage($inquiry, $messageType) 
    {
	$message = "";
	
	$companyApi = new CompanyApi();
	$companyRow = $companyApi->getById($inquiry['companyId']);
	$company = Company::getCompanyBO($companyRow);


	$workshopApi = new WorkshopApi();
	$workshopRow = $workshopApi->getById($inquiry['workshopId']);
	$workshop = Workshop::getWorkshopBO($workshopRow);

    	switch($messageType) {
		case "Pickup":

	$companyName = $company[0]['companyName'];
	echo "<pre>". var_dump($companyName) ."</pre>";

	$message = "Your car has been picked up by ".$companyName." and will be taken to the workshop. For any queries, please contact 0".$company[0]['mobileNumber1'];
			break;

		case "DeliveredToWs":
			$message = "Car ".$inquiry['carRegNo']." has been delivered at the workshop ". $inquiry['workshopName'].". For any queries, please contant the workshop on 0".$workshop[0]['mobileNumber'].".";
		 	break;		
	}
	return $message;
    }
    
    public function getAllInquiries() 
    {      
        include 'dao/config.php';
        $session = SessionManager::getSession();
        
        $dao = new GenericDao();
        $inquiryRows = array();
        
        if ($session['role'] === 'Driver') {
            $inquiryRows = $dao->getDetailedInquiry(array(
                'd.idn_driver' => $session['driver-id']
            ), $session['role']);
        } else {
            if ($session['role'] === 'Dealer') {
                $dealer_id = $session['user-id'];
                $query = mysqli_query($con, "SELECT * FROM `inquiry` WHERE `role` = 'Dealer' AND `dealer_id` = '".$dealer_id."' ORDER BY dt_added DESC ");
                foreach ($query as $row) {
                    $inquiryRows[] = array(
                        'idn_inquiry'           => $row["idn_inquiry"],
                        'dealer_id'             => $row["dealer_id"],
                        'role'                  => $row["role"],
                        'idn_company'           => $row["idn_company"],
                        'idn_driver'            => $row["idn_driver"],
                        'idn_workshop'          => $row["idn_workshop"],
                        'dt_casedate'           => $row["dt_casedate"],
                        'txt_casetype'          => $row["txt_casetype"],
                        'txt_carregno'          => $row["txt_carregno"],
                        'dt_jobcompletiontime'  => $row["dt_jobcompletiontime"],
                        'txt_make'              => $row["txt_make"],
                        'txt_model'             => $row["txt_model"],
                        'int_kms'               => $row["int_kms"],
                        'txt_pickup'            => $row["txt_pickup"],
                        'txt_drop'              => $row["txt_drop"],
                        'url_inventory'         => $row["url_inventory"],
                        'txt_inventorycomments' => $row["txt_inventorycomments"],
                        'txt_drivercomments'    => $row["txt_drivercomments"],
                        'txt_casereference'     => $row["txt_casereference"],
                        'dt_added'              => $row["dt_added"],
                        'dt_modified'           => $row["dt_modified"],
                        'txt_fileno'            => $row["txt_fileno"],
                        'txt_custdrivermobile'  => $row["txt_custdrivermobile"],
                        'txt_custemail'         => $row["txt_custemail"],
                        'txt_custname'          => $row["txt_custname"],
                        'txt_custmobile'        => $row["txt_custmobile"],
                        'url_cust_sign'         => $row["url_cust_sign"],
                        'url_workshop_sign'     => $row["url_workshop_sign"],
                        'url_car'               => $row["url_car"],
                        'num_driver_arrive_hr'  => $row["num_driver_arrive_hr"],
                        'num_driver_arrive_min' => $row["num_driver_arrive_min"],
                        'txt_custsign_by'       => $row["txt_custsign_by"],
                        'txt_wssign_by'         => $row["txt_wssign_by"],
                        'ind_complete'          => $row["ind_complete"],
                        'num_amount'            => $row["num_amount"]
                    );
                }
                /*$inquiryRows = $dao->getRows("inquiry", array(
                        "where" => array(
                        "role" => 'Dealer',
                        'dealer_id' => $dealer_id
                    )
                ));*/
                //echo "<pre>"; print_r($inquiryRows);exit;
                /*$inquiryRows = $dao->getDetailedInquiry(array(
                    'i.dealer_id' => $dealer_id
                ), $session['role']);*/

            }elseif ($session['role'] === 'Company') {
                $sel = mysqli_query($con,"select idn_company from company where idn_user = '".$session['user-id']."' ");
                $row = mysqli_fetch_array($sel);
                $dealer_id = $row['idn_company'];
                $inquiryRows = $dao->getDetailedInquiry(array(
                    'i.idn_company' => $dealer_id
                ), $session['role']);
            }
        }
        //orderBy('idn_inquiry', 'DESC')
        $inquiryBO = Inquiry::getInquiriesBO($inquiryRows);
        
        //  echo "<pre>"; print_r($dealer_id);
        return $inquiryBO;
    }
}

?>
