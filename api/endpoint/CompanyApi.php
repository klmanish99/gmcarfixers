<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("utility/SessionManager.php");
require_once ("vo/Company.php");
require_once ("dao/GenericDao.php");


class CompanyApi extends BaseRestApi
{

    function __construct()
    {}

    public function getCompanies()
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();
        $dao = new GenericDao();
        $dealer_to_companyRows = $dao->getRows("company_to_dealer", array(
            "where" => array(
                "dealer_id" => $session['user-id']
            )
        ));
        $company_idsss = '';
        foreach ($dealer_to_companyRows as $dtc) {
            $company_idsss .= $dtc['company_id'].',';
        }
        $company_idss = rtrim($company_idsss,',');
        $companyRows = mysqli_query($con,"SELECT * FROM company where ind_active = 1 AND idn_company IN(".$company_idss.")");
        //echo "SELECT * FROM company where ind_active = 1 AND idn_company IN(".$company_idss.")";
        /*$companyRows = $dao->getRows("company", array(
            "where" => array(
                "ind_active" => 1
                "idn_company" => $company_idss
            )
        ));*/

        $companyBO = Company::getCompanyBO($companyRows);

        parent::respond(200, json_encode($companyBO));
    }

    public function insertCompany($company)
    {   
        include 'dao/config.php';
        
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $existingUserNameRows = $dao->getRows("user", array(
            'where' => array(
                'nme_user' => $company->userName
            )
        ));

        if (! empty($existingUserNameRows)) {
            throw new DuplicateEntryException("User Name already exists");
        }

        if ($company->address) {
            $trimmed = trim($company->address);
            $string = str_replace(' ', '+' , $trimmed);
            $address = $string.'+Maharashtra+India';
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.GEOCODING_API;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            $response = json_decode(curl_exec($ch), true);
            foreach ($response['results'] as $key => $value) {
                $lat = $value['geometry']['location']['lat'];
                $lng = $value['geometry']['location']['lng'];
            }
        } else {
            $lat = '';
            $lng = '';
        }

        $userId = $dao->insert("user", array(
            'nme_user' => $company->userName,
            'txt_pwd' => $company->password,
            'txt_role' => 'Company'
        ));

        $dao->insert("temp_company", array(
            'nme_company' => $company->companyName,
            'ind_active' => 1,
            'idn_user' => $userId,
            'txt_email' => $company->email,
            'txt_mobile1' => $company->mobileNumber1,
            'txt_mobile2' => $company->mobileNumber2,
            'txt_mobile3' => $company->mobileNumber3,
            'txt_mobile4' => $company->mobileNumber4,
            'txt_mobile5' => $company->mobileNumber5,
            'dealer_id'   => $company->dealer_id,
            'txt_firm_type' => $company->companyType,
            'txt_address' => $company->address,
            'lat' => $lat,
            'lng' => $lng,
            'txt_tnc' => $company->tnc,
            'dt_added' => date("Y-m-d H:i:s"),
            'dt_modified' => date("Y-m-d H:i:s")
        ));

        $_response = array(
            "companyId" => $userId
        );

        parent::respond(200, json_encode($_response));
    }

    public function getCompanyById($companyId)
    {
        $session = SessionManager::getSession();

        $companyRow = self::getById($companyId);

        $_response = Company::getCompanyBO($companyRow);

        parent::respond(200, json_encode($_response));
    }

    public function updateCompany($companyId, $company)
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();

        if ($company->address != $company->address_old) {
            if ($company->address) {
                $trimmed = trim($company->address);
                $string = str_replace(' ', '+' , $trimmed);
                $address = $string.'+Maharashtra+India';
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key='.GEOCODING_API;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                $response = json_decode(curl_exec($ch), true);
                foreach ($response['results'] as $key => $value) {
                    $lat = $value['geometry']['location']['lat'];
                    $lng = $value['geometry']['location']['lng'];
                }
            } else {
                $lat = '';
                $lng = '';
            }
        }

        $dao = new GenericDao();
        $dao->update("company", array(
            'nme_company' => $company->companyName,
            'txt_email' => $company->email,
            'txt_mobile1' => empty($company->mobileNumber1) ? null : $company->mobileNumber1,
            'txt_mobile2' => empty($company->mobileNumber2) ? null : $company->mobileNumber2,
            'txt_mobile3' => empty($company->mobileNumber3) ? null : $company->mobileNumber3,
            'txt_mobile4' => empty($company->mobileNumber4)? null : $company->mobileNumber4,
            'txt_mobile5' => empty($company->mobileNumber5 )? null : $company->mobileNumber5,
            'txt_firm_type' => $company->companyType,
            'txt_address' => $company->address,
            'lat' => $lat,
            'lng' => $lng,
            'txt_tnc' => $company->tnc,
            'dt_modified' => date("Y-m-d H:i:s")
        ), array(
            'idn_company' => $companyId
        ));

        $_response = array(
            'message' => "Updated Company"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteCompany($companyId)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $dao->update("company", array(
            'ind_active' => 0
        ), array(
            'idn_company' => $companyId
        ));

        $_response = array(
            'message' => "Deleted Company"
        );

        parent::respond(200, json_encode($_response));
    }

    public function getById($id)
    {
        $dao = new GenericDao();

        $companyRow = $dao->getRows("company", array(
            "where" => array(
                "idn_company" => $id,
                "ind_active" => 1
            )
        ));

        return $companyRow;
    }

    public function getByUserId($userId)
    {
        $dao = new GenericDao();

        $companyRow = $dao->getRows("company", array(
            "where" => array(
                "idn_user" => $id,
                "ind_active" => 1
            )
        ));

        return $companyRow;
    }
}

?>