<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("utility/SessionManager.php");
require_once ("vo/Workshop.php");
require_once ("dao/GenericDao.php");

class WorkshopApi extends BaseRestApi
{

    function __construct()
    {}

    public function getWorkshops()
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $workshopRows = $dao->getRows("workshop", array(
            "where" => array(
                "idn_company" => $session['cmp-id'],
                "ind_active" => 1
            )
        ));

        $workshopBO = Workshop::getWorkshopBO($workshopRows);

        parent::respond(200, json_encode($workshopBO));
    }

    public function insertWorkshop($workshop)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $dao->insert("workshop", array(
            'nme_workshop' => $workshop->workshopName,
            'idn_company' => $session['cmp-id'],
            'ind_active' => 1,
            'txt_email' => $workshop->email,
            'txt_mobile' => $workshop->mobileNumber,
            'txt_address' => $workshop->address,
            'dt_added' => date("Y-m-d H:i:s"),
            'dt_modified' => date("Y-m-d H:i:s")
        ));

        $_response = array(
            'message' => "Insert Workshop"
        );

        parent::respond(200, json_encode($_response));
    }

    public function getWorkshopById($workshopId)
    {
        $session = SessionManager::getSession();

        $workshopRow = self::getById($workshopId);

        $_response = Workshop::getWorkshopBO($workshopRow);

        parent::respond(200, json_encode($_response));
    }

    public function updateWorkshop($workshopId, $workshop)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $dao->update("workshop", array(
            'nme_workshop' => $workshop->workshopName,
            'txt_email' => $workshop->email,
            'txt_mobile' => empty($workshop->mobileNumber) ? null : $workshop->mobileNumber,
            'txt_address' => $workshop->address,
            'dt_modified' => date("Y-m-d H:i:s")
        ), array(
            'idn_workshop' => $workshopId
        ));

        $_response = array(
            'message' => "Updated Workshop"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteWorkshop($workshopId)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $dao->update("workshop", array(
            'ind_active' => 0
        ), array(
            'idn_workshop' => $workshopId
        ));

        $_response = array(
            'message' => "Deleted Workshop"
        );

        parent::respond(200, json_encode($_response));
    }

    public function getById($id)
    {
        $dao = new GenericDao();

        $workshopRow = $dao->getRows("workshop", array(
            "where" => array(
                "idn_workshop" => $id,
                "ind_active" => 1
            )
        ));

        return $workshopRow;
    }
}

?>