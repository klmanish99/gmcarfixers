<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("utility/SessionManager.php");
require_once ("vo/Towfixers_inquiry.php");
require_once ("dao/GenericDao.php");

class Towfixers_inquiryApi extends BaseRestApi
{

    function __construct()
    {}

    public function getTowfixers_inquiries()
    {   
        $session = SessionManager::getSession();

        include 'dao/config.php';
        $dao = new GenericDao();
        /*$towfixers_inquiryRows = $dao->getRows("towfixers_inquiry", array(
            "where" => array(
                "idn_company" => $session['cmp-id'],
                "ind_active" => 1
            )
        ));*/

        $towfixers_inquiryRows = mysqli_query($con, "SELECT * FROM `towfixers_inquiry` WHERE `idn_company` = '".$session['cmp-id']."' AND `ind_active` = 1 ORDER BY dt_added DESC ");

        $towfixers_inquiryBO = Towfixers_inquiry::getTowfixers_inquiryBO($towfixers_inquiryRows);

        //echo "<pre>"; print_r($towfixers_inquiryBO);exit;
        parent::respond(200, json_encode($towfixers_inquiryBO));
    }

    public function insertTowfixers_inquiry($towfixers_inquiry)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $dao->insert("towfixers_inquiry", array(
            'nme_towfixers_inquiry' => $towfixers_inquiry->towfixers_inquiryName,
            'idn_company' => $session['cmp-id'],
            'ind_active' => 1,
            'txt_email' => $towfixers_inquiry->email,
            'txt_mobile' => $towfixers_inquiry->mobileNumber,
            'txt_address' => $towfixers_inquiry->address,
            'dt_added' => date("Y-m-d H:i:s"),
            'dt_modified' => date("Y-m-d H:i:s")
        ));

        $_response = array(
            'message' => "Insert Towfixers_inquiry"
        );

        parent::respond(200, json_encode($_response));
    }

    public function getTowfixers_inquiryById($towfixers_inquiryId)
    {   
        $session = SessionManager::getSession();
        include 'dao/config.php';

        $towfixers_inquiryRow = self::getById($towfixers_inquiryId);

        $quotation = mysqli_query($con, "SELECT `amount`, `service_type`, `arrival_time` FROM `quotation` WHERE `inquiry_id` = '".$towfixers_inquiryId."' ");
        if ($quotation->num_rows == 1) {
            foreach ($quotation as $value) {
                $quotationData = array(
                    'amount'       => (int)$value['amount'],
                    'service_type' => $value['service_type'],
                    'arrival_time' => (int)$value['arrival_time']
                );
            }
            $get_data = Towfixers_inquiry::getTowfixers_inquiryBO($towfixers_inquiryRow);
            $_response[0] = array_merge($get_data[0],$quotationData);
        }
        else {
            $_response = Towfixers_inquiry::getTowfixers_inquiryBO($towfixers_inquiryRow);
        }
        parent::respond(200, json_encode($_response));
    }

    public function updateTowfixers_inquiry($towfixers_inquiryId, $towfixers_inquiry)
    {  
        //echo "<pre>"; print_r($towfixers_inquiry);exit;
        date_default_timezone_set('Asia/Kolkata');
        $session = SessionManager::getSession();

        include 'dao/config.php';
        $dao = new GenericDao();
        // $dao->update("towfixers_inquiry", array(
        //     'nme_towfixers_inquiry' => $towfixers_inquiry->towfixers_inquiryName,
        //     'txt_email' => $towfixers_inquiry->email,
        //     'txt_mobile' => empty($towfixers_inquiry->mobileNumber) ? null : $towfixers_inquiry->mobileNumber,
        //     'txt_address' => $towfixers_inquiry->address,
        //     'dt_modified' => date("Y-m-d H:i:s")
        // ), array(
        //     'idn_towfixers_inquiry' => $towfixers_inquiryId
        // ));

        if (isset($towfixers_inquiry->amount) && isset($towfixers_inquiry->arrival_time) && isset($towfixers_inquiry->service_type)) {
            $chekQuery = mysqli_query($con, "SELECT * FROM quotation WHERE inquiry_id = '".$towfixers_inquiry->towfixers_inquiryId."' ");
            $comp_info = mysqli_query($con, "SELECT `nme_company`, `txt_mobile1` FROM `company` WHERE `idn_company` = '".$towfixers_inquiry->idn_company."' ");
            $row = mysqli_fetch_assoc($comp_info);
            $quotationCount = mysqli_query($con, "SELECT customer_id FROM quotation WHERE customer_id = '".$towfixers_inquiry->customer_id."' ");
            $memberCheck = mysqli_query($con2,"SELECT `membership_status` FROM `customer` WHERE customer_id = '".$towfixers_inquiry->customer_id."' ");
            $isMember = mysqli_fetch_assoc($memberCheck);
           // echo "<pre>"; print_r($isMember['membership_status']);exit;
            if ( $quotationCount->num_rows < 3 ) {
                if ($chekQuery->num_rows == 0) {
                    $quotationQuery = mysqli_query($con,"INSERT INTO `quotation` SET
                                        `inquiry_id`     = '".$towfixers_inquiry->towfixers_inquiryId."',
                                        `customer_id`    = '".$towfixers_inquiry->customer_id."',
                                        `company_id`     = '".$towfixers_inquiry->idn_company."',
                                        `company_name`   = '".$row['nme_company']."',
                                        `company_number` = '".$row['txt_mobile1']."',
                                        `amount`         = '".$towfixers_inquiry->amount."',
                                        `service_type`   = '".$towfixers_inquiry->service_type."',
                                        `arrival_time`   = '".$towfixers_inquiry->arrival_time."',
                                        `date_added`     = '".date('Y-m-d H:i:s')."',
                                        `status`         = '1'
                                    ");
                    $last_id = mysqli_insert_id($con);
                    $quotationQuery_date_info_query = mysqli_query($con, "SELECT `date_added` FROM `quotation` WHERE `id` = '".$last_id."' ");
                    $quotationQuery_date_info_fetch = mysqli_fetch_assoc($quotationQuery_date_info_query);
                    $quotationQuery_date_info = $quotationQuery_date_info_fetch['date_added'];
                    $current_quotation = base64_encode($towfixers_inquiry->customer_id.'_'.$towfixers_inquiry->idn_company.'_'.$quotationQuery_date_info);

                    if ($quotationQuery && $isMember['membership_status'] == 0) {
                        //echo "in";exit;
                        $phone_number_query = mysqli_query($con2,"SELECT `telephone`, `firstname` FROM customer WHERE customer_id = '".$towfixers_inquiry->customer_id."' ");
                        $run = mysqli_fetch_assoc($phone_number_query);
                        $phoneNum = $run['telephone'];
                        //$msgToCust = "Dear ".$run['firstname']." Please check breakdown assistance service quotation here. http://towfixers.in/cnfrm.php?sjYpbsUn=".$row['customer_id']."%26yqlNrxt=".$query->row['customer_transaction_id']." ";
                       //$msgToCust = "Dear ".$run['firstname']." Please check breakdown assistance service quotation here. http://towfixers.in/cnfrm.php?sjYpbsUn=".$towfixers_inquiry->customer_id." ";
                        $msgToCust1 = "Dear ".$run['firstname']." Please check breakdown assistance service quotation here. http://towfixers.in/cnfrm.php?sjYpbsUn=".$current_quotation." ";
                        
                        $msgToCust = str_replace(' ', '%20', $msgToCust1);
                        $link = MESSAGE.$phoneNum."&msg=".$msgToCust."";
                       /* echo $link;exit;*/
                        file($link);
                    }
                    elseif ($quotationQuery && $isMember['membership_status'] == 1) {
                        $getCustInfo = mysqli_query($con2,"SELECT * FROM `customer` WHERE `customer_id` = '".$towfixers_inquiry->customer_id."' ");
                        foreach ($getCustInfo as $value) {
                            $insertQuery = mysqli_query($con,"INSERT INTO `inquiry` SET 
                                `idn_company`           = '".$towfixers_inquiry->idn_company."',
                                `customer_id`           = '".$towfixers_inquiry->customer_id."',
                                `txt_custname`          = '".$value['firstname']."',
                                `txt_custmobile`        = '".$value['telephone']."',
                                `txt_custemail`         = '".$value['email']."',
                                `txt_carregno`          = '".$value['car_reg_no']."',
                                `txt_make`              = '".$value['car_brand']."',
                                `txt_model`             = '".$value['car_model']."',
                                `txt_pickup`            = '".$value['brk_spot_add']."',
                                `txt_drop`              = '".$value['drop_add']."',
                                `num_amount`            = '".$towfixers_inquiry->amount."',
                                `dt_added`              = '".date('Y-m-d H:i:s')."'
                            ");
                        }

                        $phone_number_query = mysqli_query($con2,"SELECT `telephone`, `firstname` FROM customer WHERE customer_id = '".$towfixers_inquiry->customer_id."' ");
                        $run = mysqli_fetch_assoc($phone_number_query);
                        $phoneNum = $run['telephone'];
                        $msgToCust = "Dear ".$run['firstname']." Our driver is on the way.";
                        $msgToCust = str_replace(' ', '%20', $msgToCust);
                        $link = MESSAGE.$phoneNum."&msg=".$msgToCust."";
                        //echo $link;exit;
                        file($link);
                    }
                }
                else{
                    echo "inquiry updation is not allowed.";exit;
                }
            }
            else {
                echo "3 quotations already inserted.";exit;
            }
            if ($isMember['membership_status'] == 1 && $quotationCount->num_rows == 0) {
                mysqli_query($con, "DELETE FROM `towfixers_inquiry` WHERE customer_id = '".$towfixers_inquiry->customer_id."' ");
            }
            if($quotationCount->num_rows == 2){
                $idsss = mysqli_query($con,"SELECT `inquiry_id` FROM `quotation` WHERE customer_id = '".$towfixers_inquiry->customer_id."' ");
                foreach ($idsss as $id) {
                    $idss[] = $id['inquiry_id'];
                }
                $ids = implode(",",$idss);
                mysqli_query($con, "DELETE FROM `towfixers_inquiry` WHERE customer_id = '".$towfixers_inquiry->customer_id."' AND idn_towfixers_inquiry NOT IN (".$ids.") ");
            }
        }
        else{
            echo "please fill the fields.";exit;
        }

        $_response = array(
            'message' => "Updated Towfixers_inquiry"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteTowfixers_inquiry($towfixers_inquiryId)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $dao->update("towfixers_inquiry", array(
            'ind_active' => 0
        ), array(
            'idn_towfixers_inquiry' => $towfixers_inquiryId
        ));

        $_response = array(
            'message' => "Deleted Towfixers_inquiry"
        );

        parent::respond(200, json_encode($_response));
    }

    public function getById($id)
    {
        $dao = new GenericDao();

        $towfixers_inquiryRow = $dao->getRows("towfixers_inquiry", array(
            "where" => array(
                "idn_towfixers_inquiry" => $id,
                "ind_active" => 1
            )
        ));

        return $towfixers_inquiryRow;
    }
}

?>