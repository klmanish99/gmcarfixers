<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("utility/SessionManager.php");
require_once ("vo/Dealer.php");
require_once ("dao/GenericDao.php");

class DealerApi extends BaseRestApi
{

    function __construct()
    {}

    public function getDealers()
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $dealerRows = $dao->getRows("dealer", array(
            "where" => array(
                "ind_active" => 1
            )
        ));

        $dealerBO = Dealer::getDealerBO($dealerRows);

        parent::respond(200, json_encode($dealerBO));
    }

    public function insertDealer($dealer)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $existingUserNameRows = $dao->getRows("user", array(
            'where' => array(
                'nme_user' => $dealer->userName
            )
        ));

        if (! empty($existingUserNameRows)) {
            throw new DuplicateEntryException("User Name already exists");
        }

        $userId = $dao->insert("user", array(
            'nme_user' => $dealer->userName,
            'txt_pwd' => $dealer->password,
            'txt_role' => 'Dealer'
        ));

        $dao->insert("dealer", array(
            'nme_dealer' => $dealer->dealerName,
            'ind_active' => 1,
            'idn_user' => $userId,
            'txt_email' => $dealer->email,
            'txt_mobile1' => $dealer->mobileNumber1,
            'txt_mobile2' => $dealer->mobileNumber2,
            'txt_mobile3' => $dealer->mobileNumber3,
            'txt_mobile4' => $dealer->mobileNumber4,
            'txt_mobile5' => $dealer->mobileNumber5,
            'txt_firm_type' => $dealer->dealerType,
            'txt_address' => $dealer->address,
            'txt_tnc' => $dealer->tnc,
            'dt_added' => date("Y-m-d H:i:s"),
            'dt_modified' => date("Y-m-d H:i:s")
        ));

        $_response = array(
            "dealerId" => $userId
        );

        parent::respond(200, json_encode($_response));
    }

    public function getDealerById($dealerId)
    {
        $session = SessionManager::getSession();

        $dealerRow = self::getById($dealerId);

        $_response = Dealer::getDealerBO($dealerRow);

        parent::respond(200, json_encode($_response));
    }

    public function updateDealer($dealerId, $dealer)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();
        $dao->update("dealer", array(
            'nme_dealer' => $dealer->dealerName,
            'txt_email' => $dealer->email,
            'txt_mobile1' => empty($dealer->mobileNumber1) ? null : $dealer->mobileNumber1,
            'txt_mobile2' => empty($dealer->mobileNumber2) ? null : $dealer->mobileNumber2,
            'txt_mobile3' => empty($dealer->mobileNumber3) ? null : $dealer->mobileNumber3,
            'txt_mobile4' => empty($dealer->mobileNumber4)? null : $dealer->mobileNumber4,
            'txt_mobile5' => empty($dealer->mobileNumber5 )? null : $dealer->mobileNumber5,
            'txt_firm_type' => $dealer->dealerType,
            'txt_address' => $dealer->address,
            'txt_tnc' => $dealer->tnc,
            'dt_modified' => date("Y-m-d H:i:s")
        ), array(
            'idn_dealer' => $dealerId
        ));

        $_response = array(
            'message' => "Updated Dealer"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteDealer($dealerId)
    {
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $dao->update("dealer", array(
            'ind_active' => 0
        ), array(
            'idn_dealer' => $dealerId
        ));

        $_response = array(
            'message' => "Deleted Dealer"
        );

        parent::respond(200, json_encode($_response));
    }

    public function getById($id)
    {
        $dao = new GenericDao();

        $dealerRow = $dao->getRows("dealer", array(
            "where" => array(
                "idn_dealer" => $id,
                "ind_active" => 1
            )
        ));

        return $dealerRow;
    }

    public function getByUserId($userId)
    {
        $dao = new GenericDao();

        $dealerRow = $dao->getRows("dealer", array(
            "where" => array(
                "idn_user" => $id,
                "ind_active" => 1
            )
        ));

        return $dealerRow;
    }

    public function exportDealers() 
    {
        $dealers = self::getAllDealers();
        $fp = fopen(__DIR__ . '/../../resources/tmp/dealers.csv', 'w'); #WINDOWS fopen(__DIR__ . '\..\..\resources\tmp\dealers.csv', 'w');
        
        $delimiter = ',';
        $csvHeaders = ["D_name", "D_address", "D_mobile", "D_email"];
        $increment = 0;
        $final_array = array();
        foreach ($dealers as $temp) {
            $final_array[] = array_values($dealers[$increment]);
            $increment++;
        }
        // Save header
        $x = fputcsv($fp, $csvHeaders, $delimiter);
        if(count($dealers) > 0) {
        // Save Data
            foreach ($final_array as $value) {
                $y = fputcsv($fp, $value, $delimiter);
            }
        }
        // output headers so that the file is downloaded rather than displayed
        header("Content-type: text/csv");
        header("Content-disposition: attachment; filename = dealers.csv");
        # WINDOWS readfile(__DIR__ . '\..\..\resources\tmp\dealers.csv');
        readfile(__DIR__ . '/../../resources/tmp/dealers.csv');
    }
    public function getAllDealers() 
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();
        
        $dao = new GenericDao();
        $dealerRows = array();
        $get_dealers = mysqli_query($con,"select nme_dealer,txt_address,txt_mobile1,txt_email from dealer where ind_active = 1 ");
        foreach ($get_dealers as $get_dealer) {
            $dealerRows[] = array(
                'd_name'     => $get_dealer["nme_dealer"],
                'd_address'  => $get_dealer["txt_address"],
                'd_mobile'   => $get_dealer["txt_mobile1"],
                'd_email'    => $get_dealer["txt_email"]
            );
        }
        return $dealerRows;
    }
}

?>