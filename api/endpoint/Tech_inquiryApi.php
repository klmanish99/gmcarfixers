<?php
require_once ("endpoint/BaseRestApi.php");
require_once ("endpoint/MailerApi.php");
require_once ("endpoint/SmsApi.php");
require_once ("endpoint/CompanyApi.php");
require_once ("endpoint/WorkshopApi.php");
require_once ("utility/SessionManager.php");
require_once ("utility/exception/DuplicateEntryException.php");
require_once ("vo/Tech_inquiry.php");
require_once ("vo/Company.php");
require_once ("vo/Workshop.php");
require_once ("dao/GenericDao.php");

class Tech_inquiryApi extends BaseRestApi
{

    function __construct()
    {}

    public function getTech_inquiries()
    {
        $tech_inquiries = self::getAllTech_inquiries();
        //echo "<pre>"; print_r($tech_inquiries);exit;
        parent::respond(200, json_encode($tech_inquiries));
    }

    public function insertTech_inquiry($tech_inquiry)
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();
        if ($session['role'] == 'Dealer') {
            $dealer_id = $session['user-id'];
        }
        elseif ($session['role'] == 'Company') {
            $sel = mysqli_query($con,"select dealer_id from company where idn_user = '".$session['user-id']."' ");
            $row = mysqli_fetch_array($sel);
            $dealer_id = $row['dealer_id'];
        }
        $query = mysqli_query($con,"SELECT nme_technician, txt_mobile from technician where idn_technician = '".$tech_inquiry->company_id."' ");
        foreach ($query as $value) {
            $technician_name = $value['nme_technician'];
            $phoneNum = $value['txt_mobile'];
        }
        $dao = new GenericDao();
        //echo "<pre>"; print_r($tech_inquiry);exit;
        $tech_inquiryId = $dao->insert("tech_inquiry", array(
            'technician_id' => $tech_inquiry->company_id,
            'technician_name' => $technician_name,
            'ind_active' => 1,
            'txt_custname'=>$tech_inquiry->custName,
            'txt_custemail'=>$tech_inquiry->custEmail,
            'txt_custmobile'=>empty($tech_inquiry->custMobile)?null:$tech_inquiry->custMobile,
            'customerAltMobile'=> empty($tech_inquiry->custAltMobile) ? null : $tech_inquiry->custAltMobile,
            'dt_added' => date("Y-m-d H:i:s"),
            'dt_modified' => date("Y-m-d H:i:s"),
            'num_amount'=>$tech_inquiry->amount,
            'carRegNo'=>$tech_inquiry->carRegNo,
            'carBrand' => $tech_inquiry->carBrand,
            'carModel' => $tech_inquiry->carModel,
            'dt_casedate' => $tech_inquiry->caseDate,
            'customerLocation' => $tech_inquiry->customerLocation,
            'custPrblmDescription' => $tech_inquiry->custPrblmDesc,
            'techObservation' => $tech_inquiry->techObservation,
            'dealer_id'=>$dealer_id,
            'role'=>$tech_inquiry->role
        ));
        $_response = array(
            'tech_inquiryId' => $tech_inquiryId
        );

        parent::respond(200, json_encode($_response));

        $msgToTech = "A new enquiry has been assigned to you. For More Click http://carfixers.in";
        $msgToTech = str_replace(' ', '%20', $msgToTech);
        $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
        //echo $link;exit;
        file($link);
    }

    public function getTech_inquiryById($tech_inquiryId)
    {
        $session = SessionManager::getSession();

        $tech_inquiryRow = self::getById($tech_inquiryId);

        $_response = Tech_inquiry::getTech_inquiryBO($tech_inquiryRow);

        parent::respond(200, json_encode($_response));
    }

    public function updateTech_inquiry($tech_inquiryId, $tech_inquiry)
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();
        if ($session['role'] == 'Dealer') {
            $dealer_id = $session['user-id'];
        }
        else{
            $dealer_id = $tech_inquiry->dealer_id;
        }
        if (isset($tech_inquiry->company_id)) {
            $technician_id = $tech_inquiry->company_id;
            $query = mysqli_query($con,"SELECT nme_technician, txt_mobile from technician where idn_technician = '".$technician_id."' ");
            foreach ($query as $value) {
                $tech_name = $value['nme_technician'];
                $phoneNum = $value['txt_mobile'];
            }
            $technician_name = $tech_name;
            $msgToTech = "A new enquiry has been assigned to you. For More Click http://carfixers.in";
            $msgToTech = str_replace(' ', '%20', $msgToTech);
            $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
            //echo $link;exit;
            file($link);

            $mailerApi = new MailerApi();
            $mailerApi->inquiryAssignedToTech($technician_id);

        } else {
            $technician_id = $tech_inquiry->technician_id;
            $technician_name = $tech_inquiry->technician_name;
        }
        if(!isset($tech_inquiry->isPrblmSolved)){
            $tech_inquiry->isPrblmSolved = '';
        }
        if ($tech_inquiry->isPrblmSolved == 'No') {
            if(!isset($tech_inquiry->isTowed)){
                $tech_inquiry->isTowed = '';
            }
            if(!isset($tech_inquiry->company_idFromTech)){
                $tech_inquiry->company_idFromTech = 0;
            }
        } else {
            $tech_inquiry->isTowed = '';
            $tech_inquiry->company_idFromTech = 0;
        }
        if ($tech_inquiry->isTowed == 'Yes') {
            if(!isset($tech_inquiry->company_idFromTech)){
                $tech_inquiry->company_idFromTech = 0;
            }
        } else {
            $tech_inquiry->company_idFromTech = 0;
        }
        if ($tech_inquiry->company_idFromTech == 0) {
            $comp_id = $tech_inquiry->companyId;
        } else{
            $comp_id = $tech_inquiry->company_idFromTech;
            $query = mysqli_query($con,"SELECT nme_company, txt_mobile1 from company where idn_company = '".$comp_id."' ");
            foreach ($query as $value) {
                $companyName = $value['nme_company'];
                $phoneNum = $value['txt_mobile1'];
            }
            $msgToTech = "A new enquiry has been assigned to you.%0aCustomer Name: ".$tech_inquiry->custName."%0aCustomer phone number: ".$tech_inquiry->custMobile."%0aTo add enquiry Click http://carfixers.in";
            $msgToTech = str_replace(' ', '%20', $msgToTech);
            $link = MESSAGE.$phoneNum."&msg=".$msgToTech.""; 
            //echo $link;exit;
            file($link);
            $mailerApi = new MailerApi();
            $mailerApi->inquiryAssignedToComp($comp_id);
        }
        //echo "<pre>"; print_r($tech_inquiry);exit;
        $dao = new GenericDao();
        $dao->update("tech_inquiry", array(
            'technician_id' => $technician_id,
            'technician_name' =>  $technician_name,
            'txt_custname'=>$tech_inquiry->custName,
            'txt_custemail'=>$tech_inquiry->custEmail,
            'txt_custmobile'=>empty($tech_inquiry->custMobile)?null:$tech_inquiry->custMobile,
            'customerAltMobile'=> empty($tech_inquiry->custAltMobile) ? null : $tech_inquiry->custAltMobile,
            'dt_added' => date("Y-m-d H:i:s"),
            'dt_modified' => date("Y-m-d H:i:s"),
            'num_amount'=>$tech_inquiry->amount,
            'carRegNo'  => $tech_inquiry->carRegNo,
            'carBrand' => $tech_inquiry->carBrand,
            'carModel' => $tech_inquiry->carModel,
            'customerLocation' => $tech_inquiry->customerLocation,
            'custPrblmDescription' => $tech_inquiry->custPrblmDesc,
            'techObservation' => $tech_inquiry->techObservation,
            'dealer_id'=>$dealer_id,
            'role'=>$tech_inquiry->role,
            'isPrblmSolved'=>$tech_inquiry->isPrblmSolved,
            'isTowed'=>$tech_inquiry->isTowed,
            'idn_company'=>$comp_id
        ), array(
            'idn_tech_inquiry' => $tech_inquiryId
        ));

        if($comp_id != 0){
            $query = mysqli_query($con,"SELECT tech_Inq_Id FROM inquiry WHERE tech_Inq_Id = '".$tech_inquiryId."' ");
            if(mysqli_num_rows($query) > 0){
                $query = mysqli_query($con,"UPDATE inquiry SET
                    `dealer_id` = '".$tech_inquiry->dealer_id."',
                    `idn_company` = '".$comp_id."',
                    `tech_Id`    = '".$technician_id."',
                    `role`       = '".$tech_inquiry->role."',
                    `txt_custname`  = '".$tech_inquiry->custName."',
                    `txt_custmobile` = '".$tech_inquiry->custMobile."',
                    `txt_custdrivermobile` = '".$tech_inquiry->custAltMobile."',
                    `txt_custemail` = '".$tech_inquiry->custEmail."',
                    `txt_carregno` = '".$tech_inquiry->carRegNo."',
                    `txt_make` = '".$tech_inquiry->carBrand."',
                    `txt_model` = '".$tech_inquiry->carModel."',
                    `dt_added` = '".date("Y-m-d H:i:s")."',
                    `txt_pickup` = '".$tech_inquiry->customerLocation."'
                    WHERE `tech_Inq_Id` = '".$tech_inquiryId."'
                ");
            } else {
                $query = mysqli_query($con,"INSERT INTO inquiry SET 
                    `dealer_id` = '".$tech_inquiry->dealer_id."',
                    `tech_Inq_Id` = '".$tech_inquiryId."', 
                    `idn_company` = '".$comp_id."',
                    `tech_Id`    = '".$technician_id."',
                    `role`       = '".$tech_inquiry->role."',
                    `txt_custname`  = '".$tech_inquiry->custName."',
                    `txt_custmobile` = '".$tech_inquiry->custMobile."',
                    `txt_custdrivermobile` = '".$tech_inquiry->custAltMobile."',
                    `txt_custemail` = '".$tech_inquiry->custEmail."',
                    `txt_carregno` = '".$tech_inquiry->carRegNo."',
                    `txt_make` = '".$tech_inquiry->carBrand."',
                    `txt_model` = '".$tech_inquiry->carModel."',
                    `dt_added` = '".date("Y-m-d H:i:s")."',
                    `dt_modified` = '".date("Y-m-d H:i:s")."',
                    `txt_pickup` = '".$tech_inquiry->customerLocation."'
                ");
            }
        }

        $_response = array(
            'message' => "Updated Tech_inquiry"
        );

        parent::respond(200, json_encode($_response));
    }

    public function deleteTech_inquiry($tech_inquiryId)
    {   
        $session = SessionManager::getSession();

        $dao = new GenericDao();

        $dao->update("tech_inquiry", array(
            'ind_active' => 0
        ), array(
            'idn_tech_inquiry' => $tech_inquiryId
        ));

        $_response = array(
            'message' => "Deleted Tech_inquiry"
        );

        parent::respond(200, json_encode($_response));
    }

    public function uploadTech_inquiry($tech_inquiryId, $fileName, $fileTmp, $type, $signBy)
    {   
        include 'dao/config.php';
        date_default_timezone_set('Asia/Kolkata');
        $session = SessionManager::getSession();
        if (! isset($fileName) || empty($fileName)) {
            throw new Exception("Please select an image of size less than 2 MB!");
        }
        
        $uploadDir = "../resources/uploads/tech_inquiry/";
        
        $dao = new GenericDao();
        
        switch ($type) {
            case "custSign":
                $fileName = "" . $tech_inquiryId . "_custSign_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);
                
                $dao->update("tech_inquiry", array(
                    'url_cust_sign' => "resources/uploads/tech_inquiry/" . $fileName,
                    'txt_custsign_by' => $signBy,
                    'cust_sign_time' => date("Y-m-d H:i:s")
                ), array(
                    'idn_tech_inquiry' => $tech_inquiryId
                ));

                $query = mysqli_query($con,"SELECT txt_custname, txt_custmobile, dealer_id from tech_inquiry where idn_tech_inquiry = '".$tech_inquiryId."' ");
                foreach ($query as $value) {
                    $custName  = $value['txt_custname'];
                    $phoneNum  = $value['txt_custmobile'];
                    $dealer_id = $value['dealer_id'];
                }
                $sql = mysqli_query($con,"SELECT txt_mobile1 from dealer where idn_user = '".$dealer_id."' ");
                $row = mysqli_fetch_assoc($sql);
                $dealerPhoneNumber = $row['txt_mobile1'];
                $msgToCust = "Your vehicle was checked by the technician and you may contact the dealer for further process.%0aDealer phone number: ".$dealerPhoneNumber;
                $msgToCust = str_replace(' ', '%20', $msgToCust);
                $link = MESSAGE.$phoneNum."&msg=".$msgToCust.""; 
                //echo $link;exit;
                file($link);
                
                $mailerApi = new MailerApi();
		$mailerApi->sendCustomerCopyTech($tech_inquiryId);

		$tech_inquiryRow = self::getById($tech_inquiryId);
		$tech_inquiries = Tech_inquiry::getTech_inquiryBO($tech_inquiryRow);
		$tech_inquiry = $tech_inquiries[0];

		$smsApi = new SmsApi();
		$sendto = $tech_inquiry['customerMobile'] != ""? $tech_inquiry['customerMobile'] : $tech_inquiry['custDriverMobile'];

		$message = self::createMessage($tech_inquiry, 'Pickup');
		$smsApi->sendMessage($message,$sendto);
                break;
                
            case "wsSign":
                $fileName = "" . $tech_inquiryId . "_wsSign_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);
                
                $dao->update("tech_inquiry", array(
                    'url_workshop_sign' => "resources/uploads/tech_inquiry/" . $fileName,
                    'txt_wssign_by' => $signBy,
                    'dt_jobcompletiontime' => date("Y-m-d H:i:s")
                ), array(
                    'idn_tech_inquiry' => $tech_inquiryId
	    	));

		$mailerApi = new MailerApi();
        $mailerApi->sendWorkshopCopy($tech_inquiryId);

		$tech_inquiryRow = self::getById($tech_inquiryId);
		$tech_inquiries = Tech_inquiry::getTech_inquiryBO($tech_inquiryRow);
		$tech_inquiry = $tech_inquiries[0];

		$smsApi = new SmsApi();
		$sendto = "";
		/*$sendto = $tech_inquiry['customerMobile'] != "" ? $tech_inquiry['customerMobile'] : customer['custDriverMobile'];*/

		$message = self::createMessage($tech_inquiry, 'DeliveredToWs');
		
		if($sendto != ""){
			$smsApi->sendMessage($message,$sendto);
		}

		$companyApi = new CompanyApi();
	        $company = $companyApi->getCompanyById($tech_inquiry['companyId']);

		if($company[0]['mobileNumber1'] != ""){
			$smsApi->sendMesssage($message, $company[0]['mobileNumber1']);
		}

                break;
                
            case "carInv":
                $fileName = "" . $tech_inquiryId . "_car_" . time() . ".png";
                move_uploaded_file($fileTmp, $uploadDir . $fileName);
                
                $dao->update("tech_inquiry", array(
                    'url_car' => "resources/uploads/tech_inquiry/" . $fileName,
                    'txt_inventorycomments' => $signBy
                ), array(
                    'idn_tech_inquiry' => $tech_inquiryId
                ));
                break;
        }
        
        $_response = array(
            'tech_inquiryId' => $tech_inquiryId,
	    'fileName' => "resources/uploads/tech_inquiry/" . $fileName,
	    'message' => $message,
	    'sentTo' => $sendto
        );
        
        parent::respond(200, json_encode($_response));
    }
    
    public function exportTech_inquiries() 
    {
        $tech_inquiries = self::getAllTech_inquiries();
        
        $fp = fopen(__DIR__ . '/../../resources/tmp/tech_inquiries.csv', 'w'); #WINDOWS fopen(__DIR__ . '\..\..\resources\tmp\tech_inquiries.csv', 'w');
        
        $delimiter = ',';
        $csvHeaders = ["File Number", "Case Date", "Case Type", "Reference", "Driver Name", "Workshop Name", "Customer Name", "Make", "Model", "Car Registration Number", "Customer Mobile", "Customer Email", "Pickup Location", "Drop Location"];
        
        // Save header
        $x = fputcsv($fp, $csvHeaders, $delimiter);
        
        if(count($tech_inquiries) > 0) {
            
            // Save Data
            foreach ($tech_inquiries as $elem) {
                $y = fputcsv($fp, Tech_inquiry::getExportProperties($elem), $delimiter);
            }
        }
        
        // output headers so that the file is downloaded rather than displayed
        header("Content-type: text/csv");
        header("Content-disposition: attachment; filename = tech_inquiries.csv");
        # WINDOWS readfile(__DIR__ . '\..\..\resources\tmp\tech_inquiries.csv');
        readfile(__DIR__ . '/../../resources/tmp/tech_inquiries.csv');
    }
    
    
    public function getById($id)
    {   
        include 'dao/config.php';
        $tech_inquiryRows = mysqli_query($con,"SELECT * from tech_inquiry where idn_tech_inquiry = '".$id."' ");
            foreach ($tech_inquiryRows as $row) {
                if ($row['url_cust_sign'] == null) {
                    $url_cust_sign = "";
                } else{
                    $url_cust_sign = $row['url_cust_sign'];
                }
                $tech_inquiryRow = array(
                    'tech_inquiryId'   => $row['idn_tech_inquiry'],
                    'dealer_id'        => $row['dealer_id'],
                    'role'             => $row['role'],
                    'technician_id'    => $row['technician_id'],
                    'technician_name'  => $row['technician_name'],
                    'custName'         => $row['txt_custname'],
                    'custEmail'        => $row['txt_custemail'],
                    'custMobile'       => $row['txt_custmobile'],
                    'custAltMobile'    => $row['customerAltMobile'],
                    'caseDate'         => $row['dt_casedate'],
                    'carRegNo'         => $row['carRegNo'],
                    'carBrand'         => $row['carBrand'],
                    'carModel'         => $row['carModel'],
                    'customerLocation' => $row['customerLocation'],
                    'custPrblmDesc'    => $row['custPrblmDescription'],
                    'techObservation'  => $row['techObservation'],
                    'carBrand'         => $row['carBrand'],
                    'carBrand'         => $row['carBrand'],
                    'companyId'        => $row['idn_company'],
                    'companyName'      => $company,
                    'isComplete'       => $row['ind_complete'],
                    'custSignPic'      => $url_cust_sign,
                    'custSignBy'       => $row['txt_custsign_by'],
                    'amount'           => $row['num_amount']==null ? "":$row['num_amount']+0
                );
                //echo "<pre>"; print_r($tech_inquiryRow);
                return $tech_inquiryRow;
            }//exit;
    }

    public function createMessage($tech_inquiry, $messageType) 
    {
	$message = "";
	
	$companyApi = new CompanyApi();
	$companyRow = $companyApi->getById($tech_inquiry['companyId']);
	$company = Company::getCompanyBO($companyRow);


	$workshopApi = new WorkshopApi();
	$workshopRow = $workshopApi->getById($tech_inquiry['workshopId']);
	$workshop = Workshop::getWorkshopBO($workshopRow);

    	switch($messageType) {
		case "Pickup":

	$companyName = $company[0]['companyName'];
	echo "<pre>". var_dump($companyName) ."</pre>";

	$message = "Your car has been picked up by ".$companyName." and will be taken to the workshop. For any queries, please contact 0".$company[0]['mobileNumber1'];
			break;

		case "DeliveredToWs":
			$message = "Car ".$tech_inquiry['carRegNo']." has been delivered at the workshop ". $tech_inquiry['workshopName'].". For any queries, please contant the workshop on 0".$workshop[0]['mobileNumber'].".";
		 	break;		
	}
	return $message;
    }
    
    public function getAllTech_inquiries() 
    {   
        date_default_timezone_set('Asia/Kolkata');
        include 'dao/config.php';
        $session = SessionManager::getSession();
        $dao = new GenericDao();
        $tech_inquiryRows = array();
        if ($session['role'] === 'Dealer') {
            $query = mysqli_query($con,"SELECT * from tech_inquiry where dealer_id = '".$session['user-id']."' ORDER BY dt_added DESC ");
        } else {
            $select = mysqli_query($con,"SELECT idn_technician from technician where idn_user = '".$session['user-id']."' ");
            $row = mysqli_fetch_array($select);
            $technician_id = $row['idn_technician'];
            $current_date_time = date("Y-m-d  H:i:s");
            $current_date_time_minus_24_hours = date("Y-m-d H:i:s", strtotime($current_date_time . '-24 Hours'));
            $query = mysqli_query($con,"SELECT * from tech_inquiry where `technician_id` = '".$technician_id."' AND `cust_sign_time` > '".$current_date_time_minus_24_hours."' OR `cust_sign_time` = '0000-00-00 00:00:00' OR cust_sign_time IS NULL ORDER BY dt_added DESC ");
        }
        foreach ($query as $row) {
            $cmpName = mysqli_query($con,"SELECT nme_company from company where idn_company = '".$row['idn_company']."' ");
            $rows = mysqli_fetch_assoc($cmpName);
            $company = $rows['nme_company'];
            if ($row['url_cust_sign'] == null) {
                $url_cust_sign = "";
            } else{
                $url_cust_sign = $row['url_cust_sign'];
            }
            $tech_inquiryRows[] = array(
                'tech_inquiryId'   => $row['idn_tech_inquiry'],
                'dealer_id'        => $row['dealer_id'],
                'role'             => $row['role'],
                'technician_id'    => $row['technician_id'],
                'technician_name'  => $row['technician_name'],
                'custName'         => $row['txt_custname'],
                'custEmail'        => $row['txt_custemail'],
                'custMobile'       => $row['txt_custmobile'],
                'custAltMobile'    => $row['customerAltMobile'],
                'carRegNo'         => $row['carRegNo'],
                'caseDate'         => $row['dt_casedate'],
                'carBrand'         => $row['carBrand'],
                'carModel'         => $row['carModel'],
                'customerLocation' => $row['customerLocation'],
                'custPrblmDesc'    => $row['custPrblmDescription'],
                'techObservation'  => $row['techObservation'],
                'carBrand'         => $row['carBrand'],
                'carBrand'         => $row['carBrand'],
                'companyId'        => $row['idn_company'],
                'companyName'      => $company,
                'isComplete'       => $row['ind_complete'],
                'custSignPic'      => $url_cust_sign,
                'custSignBy'       => $row['txt_custsign_by'],
                'amount'           => $row['num_amount']==null ? "":$row['num_amount']+0
            );
        }
        //echo "<pre>"; print_r($tech_inquiryRows);exit;
        $tech_inquiryBO = Tech_inquiry::getTech_inquiriesBO($tech_inquiryRows);
        return $tech_inquiryBO;
    }
}

?>
