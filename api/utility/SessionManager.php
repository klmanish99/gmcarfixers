<?php 
require_once("utility/exception/UnauthorizedException.php");

class SessionManager {
    
    public static function checkSessionExists() {
        session_start();
        if (isset($_SESSION['user-id'])) {
            $id = $_SESSION['user-id'];
        } else {
            throw new UnauthorizedException("Session is no longer available. Please login again!");
        }
    }
    
    public static function getSession() {
        self::checkSessionExists();

        $session['user-id'] = $_SESSION['user-id'];
        $session['user-name'] = $_SESSION['user-name'];
        $session['cmp-id'] = $_SESSION['cmp-id'];
        $session['role'] = $_SESSION['role'];
        $session['driver-id'] = $_SESSION['driver-id'];
        $session['dealer_id'] = $_SESSION['dealer_id'];
        return $session;
    }
    
    public static function setSession($userId, $userName, $driverId, $cmpId, $role) {
        include 'dao/config.php';
        session_set_cookie_params(86400);
        session_start();
        
        $_SESSION['user-name'] = $userName;
        $_SESSION['user-id'] = $userId;
        $_SESSION['driver-id'] = $driverId;
        $_SESSION['cmp-id'] = $cmpId;
        $_SESSION['role'] = $role;
        if ($role == 'Dealer') {
            $dealer_id = $userId;
        }elseif ($role == 'Company') {
            $sel = mysqli_query($con,"select dealer_id from company where idn_user = $userId ");
            $row = mysqli_fetch_array($sel);
            $dealer_id = $row['dealer_id'];
        }
        $_SESSION['dealer_id'] = $dealer_id;
    }
    
    public static function destroySession() {
        session_start();
        
        // Unset all of the session variables.
        $_SESSION = array();
        
        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
                );
        }
        
        // Finally, destroy the session.
        session_destroy();
    }
}
?>