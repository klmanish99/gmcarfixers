<?php
require_once ("utility/SessionManager.php");

class Inquiry
{

    private $idn_inquiry;

    // inquiryId
    private $idn_company;

    // companyId
    private $idn_driver;

    // driverId
    private $idn_workshop;

    // workshopId
    public function __construct($inquiryId, $companyId, $driverId, $workshopId)
    {
        $this->idn_inquiry = $inquiryId;
        $this->idn_company = $companyId;
        $this->idn_driver = $driverId;
        $this->idn_workshop = $workshopId;
    }

    public static function getInquiryBO($inquiryRows)
    {   
        include 'dao/config.php';
        $inquiryBOList = array();

        if (! empty($inquiryRows)) {

            foreach ($inquiryRows as $row) {
                $inquiry = array(
                    'inquiryId' => $row['idn_inquiry'],
        		    'companyId' => $row['idn_company'],
        		    'companyName' => $row['nme_company'],
                    'companyNumber' => $row['txt_mobile1']==null?"":$row['txt_mobile1']+0,
        		    'driverId' => $row['idn_driver'],
        		    'driverName' => $row['nme_driver'],
        		    'workshopId' => $row['idn_workshop'],
        		    'workshopName' => $row['nme_workshop'],
        		    'workshopEmail' => $row['txt_email'],
        		    'isComplete' => $row['ind_complete'],
                    'caseDate'=>$row['dt_casedate'],
                    'driverArriveHour' => $row['num_driver_arrive_hr'] == null? "" : $row['num_driver_arrive_hr'],
                    'driverArriveMin' => $row['num_driver_arrive_min'] == null? "" : $row['num_driver_arrive_min'],
                    'caseType'=>$row['txt_casetype'],
        		    'fileNo'=>$row['txt_fileno'],
        		    'jobCompletionTime'=>$row['dt_jobcompletiontime']==null?"":$row['dt_jobcompletiontime'],
                    'caseReference'=>$row['txt_casereference'],
                    'make'=>$row['txt_make'],
                    'model'=>$row['txt_model'],
                    'kmsReading'=>$row['int_kms']+0,
                    'carRegNo'=>$row['txt_carregno'],
                    'customerName'=>$row['txt_custname'],
                    'customerEmail'=>$row['txt_custemail'],
                    'customerMobile'=>$row['txt_custmobile']==null?"":$row['txt_custmobile']+0,
                    'custDriverMobile'=>$row['txt_custdrivermobile']==null?"":$row['txt_custdrivermobile']+0,
                    'pickup'=>$row['txt_pickup'],
                    'drop'=>$row['txt_drop'],
                    'driverComments'=>$row['txt_drivercomments'],
                    'carInvPic'=>$row['url_car']==null?"":$row['url_car'],
                    'custSignPic'=>$row['url_cust_sign']==null?"":$row['url_cust_sign'],
                    'custSignBy'=>$row['txt_custsign_by']==null?"":$row['txt_custsign_by'],
                    'workshopSignPic'=>$row['url_workshop_sign']==null?"":$row['url_workshop_sign'],
                    'wsSignBy'=>$row['txt_wssign_by']==null?"":$row['txt_wssign_by'],
                    'carInvComments' => $row['txt_inventorycomments']==null?"":$row['txt_inventorycomments'],
                    'amount' => $row['num_amount']==null ? "":$row['num_amount']+0,
                    'customer_id'       => $row['customer_id']
                );
                array_push($inquiryBOList, $inquiry);
            }
        }

        return $inquiryBOList;
    }
    
    public static function getInquiriesBO($inquiryRows) 
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();
        $inquiryBOList = array();
        
        if (! empty($inquiryRows)) {
            foreach ($inquiryRows as $row) {
                if ($session['role'] == 'Dealer') {
                    $query = mysqli_query($con,"SELECT nme_company from company where idn_company = '".$row['idn_company']."' ");
                        foreach ($query as $value) {
                            $company = $value['nme_company'];
                    }
                } else {
                    $company = $row['nme_company'];
                }
                
                $inquiry = array(
                    'inquiryId' => $row['idn_inquiry'],
        		    'companyId' => $row['idn_company'],
        		    'companyName' => $company,
        		    'companyNumber' => $row['txt_mobile1']==null?"":$row['txt_mobile1']+0,
        		    'driverId' => $row['idn_driver'],
        		    'driverName' => $row['nme_driver'],
        		    'workshopId' => $row['idn_workshop'],
        		    'workshopName' => $row['nme_workshop'],
        		    'workshopEmail' => $row['txt_email'],
        		    'isComplete' => $row['ind_complete'],
                    'caseDate'=>$row['dt_casedate'],
                    'caseType'=>$row['txt_casetype'],
        		    'fileNo'=>$row['txt_fileno'],
        		    'jobCompletionTime'=>$row['dt_jobcompletiontime']==null?"":$row['dt_jobcompletiontime'],
                    'caseReference'=>$row['txt_casereference'],
                    'make'=>$row['txt_make'],
                    'model'=>$row['txt_model'],
                    'kmsReading'=>$row['int_kms']+0,
                    'carRegNo'=>$row['txt_carregno'],
                    'customerName'=>$row['txt_custname'],
                    'customerEmail'=>$row['txt_custemail'],
                    'customerMobile'=>$row['txt_custmobile']==null?"":$row['txt_custmobile']+0,
                    'custDriverMobile'=>$row['txt_custdrivermobile']==null?"":$row['txt_custdrivermobile']+0,
                    'pickup'=>$row['txt_pickup'],
                    'drop'=>$row['txt_drop'],
                    'driverComments'=>$row['txt_drivercomments'],
                    'custSignPic'=>$row['url_cust_sign']==null?"":$row['url_cust_sign'],
                    'workshopSignPic'=>$row['url_workshop_sign']==null?"":$row['url_workshop_sign'],
                    'carInvPic'=>$row['url_car']==null?"":$row['url_car'],
                    'driverName' => $row['nme_driver'],
                    'driverNumber'=>$row['txt_mobile']==null?"":$row['txt_mobile']+0,
                    'workshopName' => $row['nme_workshop'],
                    'workshopNumber'=>$row['txt_workshop_mobile']==null?"":$row['txt_workshop_mobile']+0,
                    'carInvComments' => $row['txt_inventorycomments']==null?"":$row['txt_inventorycomments'],
                    'amount' => $row['num_amount']==null ? "":$row['num_amount']+0
                    
                );
                array_push($inquiryBOList, $inquiry);
            } //exit;
        }
        //echo "<pre>"; print_r($inquiryBOList);exit;
        return $inquiryBOList;
    }
    
    public static function getExportProperties($obj) {
        
        $exportProperties = ["fileNo", "caseDate", "caseType", "caseReference", "driverName", "workshopName", "customerName", "make", "model", "carRegNo", "customerMobile", "customerEmail", "pickup", "drop"];
        
        $result = array();
        
        foreach ($exportProperties as $property) {
            $result[$property] = $obj[$property];
        }
        
        return $result;
    }
}
?>
