<?php

class Towfixers_inquiry
{

    private $idn_towfixers_inquiry;

    // towfixers_inquiryId
    private $idn_company;

    // userId
    private $nme_towfixers_inquiry;

    // towfixers_inquiryName
    private $ind_active;

    public function __construct($towfixers_inquiryId, $id, $name)
    {
        $this->ind_towfixers_inquiry = $towfixers_inquiryId;
        $this->idn_company = $id;
        $this->nme_towfixers_inquiry = $name;
    }

    

    public static function getTowfixers_inquiryBO($towfixers_inquiryRows)
    {   
        date_default_timezone_set('Asia/Kolkata');
        function time_elapsed_string($datetime, $full = false) {
            $now = new DateTime;
            $ago = new DateTime($datetime);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
            );
            foreach ($string as $k => &$v) {
                if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                    unset($string[$k]);
                }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            return $string ? implode(', ', $string) . ' ago' : 'just now';
        }

        $towfixers_inquiryBOList = array();
        if (! empty($towfixers_inquiryRows)) {
            foreach ($towfixers_inquiryRows as $row) {
                $now = new DateTime;
                $time_elapsed = time_elapsed_string($row['dt_added']);
                $towfixers_inquiry = array(
                    'towfixers_inquiryId'   => $row['idn_towfixers_inquiry'],
                    'idn_company'           => $row['idn_company'],
                    'customer_id'           => $row['customer_id'],
                    'car_brand'             => $row['car_brand'],
                    'car_model'             => $row['car_model'],
                    'pickup_location'       => $row['pickup_location'],
                    'drop_add'              => $row['drop_add'],
                    'car_parking_condition' => $row['car_parking_condition'],
                    'brkdown_type'          => $row['brkdown_type'],
                    'tyres_condition'       => $row['tyres_condition'],
                    'service_required'      => $row['service_required'],
                    'additional_info'       => $row['additional_info'],
                    'dt_added'              => $time_elapsed
                );
                array_push($towfixers_inquiryBOList, $towfixers_inquiry);
            }
        }
        //echo "<pre>"; print_r($towfixers_inquiryBOList);exit;
        return $towfixers_inquiryBOList;
    }
}
?>