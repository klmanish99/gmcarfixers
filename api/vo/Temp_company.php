<?php

class Temp_company
{

    private $idn_temp_company;

    // temp_companyId
    private $nme_temp_company;

    // temp_companyName
    private $ind_active;

    public function __construct($temp_companyId, $name)
    {
        $this->ind_temp_company = $temp_companyId;
        $this->nme_temp_company = $name;
    }

    public static function getTemp_companyBO($temp_companyRows)
    {
        $temp_companyBOList = array();

        if (! empty($temp_companyRows)) {
            foreach ($temp_companyRows as $row) {
                $temp_company = array(
                    'temp_companyId' => $row['idn_company'],
                    'temp_companyName' => $row['nme_company'],
                    'email' => $row['txt_email'],
                    'mobileNumber1'=>$row['txt_mobile1']==null?"":$row['txt_mobile1']+0,
                    'mobileNumber2'=>$row['txt_mobile2']==null?"":$row['txt_mobile2']+0,
                    'mobileNumber3'=>$row['txt_mobile3']==null?"":$row['txt_mobile3']+0,
                    'mobileNumber4'=>$row['txt_mobile4']==null?"":$row['txt_mobile4']+0,
                    'mobileNumber5'=>$row['txt_mobile5']==null?"":$row['txt_mobile5']+0,
                    'temp_companyType'=>$row['txt_firm_type'],
                    'address'=>$row['txt_address'],
                    'lat'=>$row['lat'],
                    'lng'=>$row['lng'],
                    'tnc'=>$row['txt_tnc']
                );
                array_push($temp_companyBOList, $temp_company);
            }
        }
        //echo "<pre>"; print_r($temp_companyBOList);
        return $temp_companyBOList;
    }
}
?>