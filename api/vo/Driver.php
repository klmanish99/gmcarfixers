<?php

class Driver
{

    private $idn_driver;

    // driverId
    private $idn_user;

    // userId
    private $nme_driver;

    // driverName
    private $idn_company;

    // companyId
    private $ind_active;

    public function __construct($driverId, $id, $name, $companyId)
    {
        $this->ind_driver = $driverId;
        $this->ind_user = $id;
        $this->nme_driver = $name;
        $this->idn_company = $companyId;
    }

    // public function getDriverVO ($driverRows) {
    // $driverVOList = array();

    // if(!empty($driverRows)) {
    // foreach($driverRows as $row) {
    // $driver = array('driverId' => $row['idn_driver'], 'driverName' => $row['nme_driver'],
    // 'userId' =>
    // );
    // array_push($driverVOList, $driver);
    // }
    // }

    // return $driverVOList;
    // }
    public static function getDriverBO($driverRows)
    {
        $driverBOList = array();

        if (! empty($driverRows)) {
            foreach ($driverRows as $row) {
                $str = str_replace("-","/",$row['dt_dob']);
                $dob = date('d/m/Y',strtotime($str));
                $driver = array(
                    'driverId' => $row['idn_driver'],
                    'driverName' => $row['nme_driver'],
                    'email'=>$row['txt_email'],
                    'mobileNumber'=>$row['txt_mobile']==null?"":$row['txt_mobile']+0,
                    'address'=>$row['txt_address'],
                    'alternateNo'=>$row['txt_alternateno']==null?"":$row['txt_alternateno']+0,
                    'dob'=>$dob,
                    'profilePic' => $row['url_profile']==null?"":$row['url_profile'],
                    'licensePic' => $row['url_license']==null?"":$row['url_license'],
                    'idProofPic' => $row['url_idproof']==null?"":$row['url_idproof']
                );
                array_push($driverBOList, $driver);
            }
        }

        return $driverBOList;
    }
}
?>