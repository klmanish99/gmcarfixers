<?php

class Company
{

    private $idn_company;

    // companyId
    private $nme_company;

    // companyName
    private $ind_active;

    public function __construct($companyId, $name)
    {
        $this->ind_company = $companyId;
        $this->nme_company = $name;
    }

    public static function getCompanyBO($companyRows)
    {
        $companyBOList = array();

        if (! empty($companyRows)) {
            foreach ($companyRows as $row) {
                $company = array(
                    'companyId' => $row['idn_company'],
                    'companyName' => $row['nme_company'],
                    'email' => $row['txt_email'],
                    'mobileNumber1'=>$row['txt_mobile1']==null?"":$row['txt_mobile1']+0,
                    'mobileNumber2'=>$row['txt_mobile2']==null?"":$row['txt_mobile2']+0,
                    'mobileNumber3'=>$row['txt_mobile3']==null?"":$row['txt_mobile3']+0,
                    'mobileNumber4'=>$row['txt_mobile4']==null?"":$row['txt_mobile4']+0,
                    'mobileNumber5'=>$row['txt_mobile5']==null?"":$row['txt_mobile5']+0,
                    'companyType'=>$row['txt_firm_type'],
                    'address'=>$row['txt_address'],
                    'address_old'=>$row['txt_address'],
                    'tnc'=>$row['txt_tnc']
                );
                array_push($companyBOList, $company);
            }
        }

        return $companyBOList;
    }
}
?>