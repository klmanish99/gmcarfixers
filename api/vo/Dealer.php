<?php

class Dealer
{

    private $idn_dealer;

    // dealerId
    private $nme_dealer;

    // dealerName
    private $ind_active;

    public function __construct($dealerId, $name)
    {
        $this->ind_dealer = $dealerId;
        $this->nme_dealer = $name;
    }

    public static function getDealerBO($dealerRows)
    {
        $dealerBOList = array();

        if (! empty($dealerRows)) {
            foreach ($dealerRows as $row) {
                $dealer = array(
                    'dealerId' => $row['idn_dealer'],
                    'dealerName' => $row['nme_dealer'],
                    'email' => $row['txt_email'],
                    'mobileNumber1'=>$row['txt_mobile1']==null?"":$row['txt_mobile1']+0,
                    'mobileNumber2'=>$row['txt_mobile2']==null?"":$row['txt_mobile2']+0,
                    'mobileNumber3'=>$row['txt_mobile3']==null?"":$row['txt_mobile3']+0,
                    'mobileNumber4'=>$row['txt_mobile4']==null?"":$row['txt_mobile4']+0,
                    'mobileNumber5'=>$row['txt_mobile5']==null?"":$row['txt_mobile5']+0,
                    'dealerType'=>$row['txt_firm_type'],
                    'address'=>$row['txt_address'],
                    'tnc'=>$row['txt_tnc']
                );
                array_push($dealerBOList, $dealer);
            }
        }

        return $dealerBOList;
    }
}
?>