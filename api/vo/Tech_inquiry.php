<?php
require_once ("utility/SessionManager.php");

class Tech_inquiry
{

    private $idn_tech_inquiry;

    // tech_inquiryId
    private $idn_company;

    // companyId
    private $idn_driver;

    // driverId
    private $idn_workshop;

    // workshopId
    public function __construct($tech_inquiryId, $companyId, $driverId, $workshopId)
    {
        $this->idn_tech_inquiry = $tech_inquiryId;
        $this->idn_company = $companyId;
        $this->idn_driver = $driverId;
        $this->idn_workshop = $workshopId;
    }

    public static function getTech_inquiryBO($tech_inquiryRows)
    {   
        include 'dao/config.php';
        $session = SessionManager::getSession();
        $tech_inquiryBOList = array();
        if (! empty($tech_inquiryRows)) {
            if ($session['role'] === 'Dealer') {
                $query = mysqli_query($con,"SELECT nme_company from company where idn_company = '".$tech_inquiryRows['companyId']."' ");
                $row = mysqli_fetch_assoc($query);
                $company = $row['nme_company'];
                $tech_inquiryRows = mysqli_query($con,"SELECT * from tech_inquiry where dealer_id = '".$session['user-id']."' AND idn_tech_inquiry = '".$tech_inquiryRows['tech_inquiryId']."' ");
            } else {
                $query = mysqli_query($con,"SELECT nme_company from company where idn_company = '".$tech_inquiryRows['companyId']."' ");
                $row = mysqli_fetch_assoc($query);
                $company = $row['nme_company'];
                $technician_id = $tech_inquiryRows['technician_id'];
                $tech_inquiryRows = mysqli_query($con,"SELECT * from tech_inquiry where technician_id = '".$technician_id."' AND idn_tech_inquiry = '".$tech_inquiryRows['tech_inquiryId']."' ");
            }
            foreach ($tech_inquiryRows as $row) {
                if ($row['url_cust_sign'] == null) {
                    $url_cust_sign = "";
                } else{
                    $url_cust_sign = $row['url_cust_sign'];
                }
                $tech_inquiry = array(
                    'tech_inquiryId'   => $row['idn_tech_inquiry'],
                    'dealer_id'        => $row['dealer_id'],
                    'role'             => $row['role'],
                    'technician_id'    => $row['technician_id'],
                    'technician_name'  => $row['technician_name'],
                    'custName'         => $row['txt_custname'],
                    'custEmail'        => $row['txt_custemail'],
                    'custMobile'       => $row['txt_custmobile'],
                    'custAltMobile'    => $row['customerAltMobile'],
                    'carRegNo'         => $row['carRegNo'],
                    'caseDate'         => $row['dt_casedate'],
                    'carBrand'         => $row['carBrand'],
                    'carModel'         => $row['carModel'],
                    'customerLocation' => $row['customerLocation'],
                    'custPrblmDesc'    => $row['custPrblmDescription'],
                    'techObservation'  => $row['techObservation'],
                    'carBrand'         => $row['carBrand'],
                    'carBrand'         => $row['carBrand'],
                    'isPrblmSolved'    => $row['isPrblmSolved'],
                    'isTowed'          => $row['isTowed'],
                    'companyId'        => $row['idn_company'],
                    'companyName'      => $company,
                    'isComplete'       => $row['ind_complete'],
                    'custSignPic'      => $url_cust_sign,
                    'custSignBy'       => $row['txt_custsign_by'],
                    'amount'           => $row['num_amount']==null ? "":$row['num_amount']+0
                );
                
                array_push($tech_inquiryBOList, $tech_inquiry);
            }
        }
        return $tech_inquiryBOList;
    }
    
    public static function getTech_inquiriesBO($tech_inquiryRows) 
    {  
        include 'dao/config.php';
        $session = SessionManager::getSession();
        $tech_inquiryBOList = array();
        if (! empty($tech_inquiryRows)) {
            if ($session['role'] === 'Dealer' ) {
                $query = mysqli_query($con,"SELECT * from tech_inquiry where dealer_id = '".$session['user-id']."' AND ind_active = '1' ORDER BY dt_added DESC ");
            } else {
                $technician_id = $tech_inquiryRows[0]['technician_id'];
                $current_date_time = date("Y-m-d  H:i:s");
                $current_date_time_minus_24_hours = date("Y-m-d H:i:s", strtotime($current_date_time . '-24 Hours'));
                $query = mysqli_query($con,"SELECT * from tech_inquiry where `technician_id` = '".$technician_id."' AND `cust_sign_time` > '".$current_date_time_minus_24_hours."' OR `cust_sign_time` = '0000-00-00 00:00:00' OR cust_sign_time IS NULL ORDER BY dt_added DESC ");
            }
            foreach ($query as $row) {
                $cmpName = mysqli_query($con,"SELECT nme_company from company where idn_company = '".$row['idn_company']."' ");
                $run = mysqli_fetch_assoc($cmpName);
                $company = $run['nme_company'];
                if ($row['url_cust_sign'] == null) {
                    $url_cust_sign = "";
                } else{
                    $url_cust_sign = $row['url_cust_sign'];
                }
                $tech_inquiry = array(
                    'tech_inquiryId'   => $row['idn_tech_inquiry'],
                    'dealer_id'        => $row['dealer_id'],
                    'role'             => $row['role'],
                    'technician_id'    => $row['technician_id'],
                    'technician_name'  => $row['technician_name'],
                    'custName'         => $row['txt_custname'],
                    'custEmail'        => $row['txt_custemail'],
                    'custMobile'       => $row['txt_custmobile'],
                    'custAltMobile'    => $row['customerAltMobile'],
                    'caseDate'         => $row['dt_casedate'],
                    'carRegNo'         => $row['carRegNo'],
                    'carBrand'         => $row['carBrand'],
                    'carModel'         => $row['carModel'],
                    'customerLocation' => $row['customerLocation'],
                    'custPrblmDesc'    => $row['custPrblmDescription'],
                    'techObservation'  => $row['techObservation'],
                    'carBrand'         => $row['carBrand'],
                    'carBrand'         => $row['carBrand'],
            	    'companyId'        => $row['idn_company'],
            	    'companyName'      => $company,
            	    'isComplete'       => $row['ind_complete'],
                    'custSignPic'      => $url_cust_sign,
                    'custSignBy'       => $row['txt_custsign_by'],
                    'amount'           => $row['num_amount']==null ? "":$row['num_amount']+0
                );
                array_push($tech_inquiryBOList, $tech_inquiry);
            }
        }
        //echo "<pre>"; print_r($tech_inquiryBOList);exit;
        return $tech_inquiryBOList;
    }
    
    public static function getExportProperties($obj) {
        
        $exportProperties = ["fileNo", "caseDate", "caseType", "caseReference", "driverName", "workshopName", "customerName", "make", "model", "carRegNo", "customerMobile", "customerEmail", "pickup", "drop"];
        
        $result = array();
        
        foreach ($exportProperties as $property) {
            $result[$property] = $obj[$property];
        }
        
        return $result;
    }
}
?>
