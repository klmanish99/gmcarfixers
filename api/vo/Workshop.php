<?php

class Workshop
{

    private $idn_workshop;

    // workshopId
    private $idn_company;

    // userId
    private $nme_workshop;

    // workshopName
    private $ind_active;

    public function __construct($workshopId, $id, $name)
    {
        $this->ind_workshop = $workshopId;
        $this->idn_company = $id;
        $this->nme_workshop = $name;
    }

    // public function getWorkshopVO ($workshopRows) {
    // $workshopVOList = array();

    // if(!empty($workshopRows)) {
    // foreach($workshopRows as $row) {
    // $workshop = array('workshopId' => $row['idn_workshop'], 'workshopName' => $row['nme_workshop'],
    // 'userId' =>
    // );
    // array_push($workshopVOList, $workshop);
    // }
    // }

    // return $workshopVOList;
    // }
    public static function getWorkshopBO($workshopRows)
    {
        $workshopBOList = array();

        if (! empty($workshopRows)) {
            foreach ($workshopRows as $row) {
                $workshop = array(
                    'workshopId' => $row['idn_workshop'],
                    'workshopName' => $row['nme_workshop'],
                    'email'=>$row['txt_email'],
                    'mobileNumber'=>$row['txt_mobile']==null?"":$row['txt_mobile']+0,
                    'address'=>$row['txt_address']
                );
                array_push($workshopBOList, $workshop);
            }
        }

        return $workshopBOList;
    }
}
?>