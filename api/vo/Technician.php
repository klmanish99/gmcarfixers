<?php

class Technician
{

    private $idn_technician;

    // technicianId
    private $idn_user;

    // userId
    private $nme_technician;

    // technicianName
    private $idn_company;

    // companyId
    private $ind_active;

    public function __construct($technicianId, $id, $name, $companyId)
    {
        $this->ind_technician = $technicianId;
        $this->ind_user = $id;
        $this->nme_technician = $name;
        $this->idn_company = $companyId;
    }

    // public function getTechnicianVO ($technicianRows) {
    // $technicianVOList = array();

    // if(!empty($technicianRows)) {
    // foreach($technicianRows as $row) {
    // $technician = array('technicianId' => $row['idn_technician'], 'technicianName' => $row['nme_technician'],
    // 'userId' =>
    // );
    // array_push($technicianVOList, $technician);
    // }
    // }

    // return $technicianVOList;
    // }
    public static function getTechnicianBO($technicianRows)
    {
        $technicianBOList = array();

        if (! empty($technicianRows)) {
            foreach ($technicianRows as $row) {
                $str = str_replace("-","/",$row['dt_dob']);
                $dob = date('d/m/Y',strtotime($str));
                $technician = array(
                    'technicianId' => $row['idn_technician'],
                    'technicianName' => $row['nme_technician'],
                    'email'=>$row['txt_email'],
                    'mobileNumber'=>$row['txt_mobile']==null?"":$row['txt_mobile']+0,
                    'address'=>$row['txt_address'],
                    'alternateNo'=>$row['txt_alternateno']==null?"":$row['txt_alternateno']+0,
                    'dob'=>$dob,
                    'profilePic' => $row['url_profile']==null?"":$row['url_profile'],
                    'licensePic' => $row['url_license']==null?"":$row['url_license'],
                    'idProofPic' => $row['url_idproof']==null?"":$row['url_idproof']
                );
                array_push($technicianBOList, $technician);
            }
        }

        return $technicianBOList;
    }
}
?>