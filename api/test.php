<?php
/*
 * DB Class
 * This class is used for database related (connect, insert, update, and delete) operations
 * with PHP Data Objects (PDO)
 * @author    CodexWorld.com
 * @url       http://www.codexworld.com
 * @license   http://www.codexworld.com/license
 */
require_once 'dao/GenericDao.php';
class test{
  private $id;
  private $name;

  public function __construct($id, $name){
   $this->id = $id;
   $this->name = $name;
  }
  
  public function setId ($id) {
      $this->id = $id;
  }
  
  public function getId () {
      return $this->id;
  }
  
  public function setName ($name) {
      $this->name = $name;
  }
  
  public function getName () {
      return $this->name;
  }
//   'id' => $this->getId(),
  public function getTestObj () {
      return array ('name' => $this->getName());
  }
}

$db = new GenericDao();
$test = new test(5,"Ronak");

// echo $test->getId();
// echo $test->getName();
// $db->insert("test",$test->getTestObj())
// $objs = $db->getRows("test", array('where' => array('name' => 'Ronak')));
$objs = $db -> getRows("test");
foreach($objs as $obj) {
    echo $obj['id'];
    echo $obj['name'];
}
// $db->update("test", $test->getTestObj(), array( 'id' => 5));
?>
