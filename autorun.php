<?php
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('Asia/Kolkata');

$con = mysqli_connect("localhost","root","","towfixers");
if (!$con)
  {
  echo "Failed to connect to MySQL (towfixers)";
}
$con2 = mysqli_connect("localhost","root","","carfixers");
if (!$con2)
  {
  echo "Failed to connect to MySQL (carfixers)";
}

$current_date = date('Y-m-d');
$current_dateTime = date('Y-m-d H:i:s');

$getInquiryInfo = mysqli_query($con2, "SELECT * FROM `towfixers_inquiry` WHERE `service_required` = 'Immediately' AND date(`dt_added`) < '".$current_date."' ");
if ($getInquiryInfo->num_rows > 0) {
	foreach ($getInquiryInfo as $value) {
		$remove = mysqli_query($con2, "DELETE FROM `towfixers_inquiry` WHERE `service_required` = 'Immediately' AND date(`dt_added`) < '".$current_date."' ");
		if ($remove) {
			mysqli_query($con2, "UPDATE `quotation` SET `status` = '0' WHERE `customer_id` = '".$value['customer_id']."' ");
		}
	}
}

$query = mysqli_query($con2, "SELECT * FROM `towfixers_inquiry` WHERE `service_required` != 'Immediately' ");
foreach ($query as $data) {
	$str_dt = substr($data['service_required'], 6, 10);
	$str_tm = substr($data['service_required'], 23, 7);
	$dt = date('Y-m-d', strtotime(str_replace('.', '/', $str_dt)));
	$tm = date("H:i:s", strtotime($str_tm));
	$dtTime = $dt ." ". $tm;
	if ($dtTime < $current_dateTime) {
		$remove = mysqli_query($con2, "DELETE FROM `towfixers_inquiry` WHERE `service_required` != 'Immediately' AND `customer_id` = '".$data['customer_id']."' AND `idn_company` = '".$data['idn_company']."' ");
		if ($remove) {
			mysqli_query($con2, "UPDATE `quotation` SET `status` = '0' WHERE `customer_id` = '".$data['customer_id']."' AND `company_id` = '".$data['idn_company']."' ");
		}
	}
}
?>