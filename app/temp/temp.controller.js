(function(angular){
	
	angular
		.module('temp', []);
	
	angular
		.module('temp')
		.controller('temp.controller', TempController);
	
	TempController.$inject = [];
	
	function TempController() {
		var vm = this;
		
		vm.add = addTemp;
		
		function addTemp() {
			console.log('Adding Temp');
		}
	}
})(window.angular);