(function (angular) {

    angular
        .module('dealer')
        .controller('dealer.addEdit.controller', AddEditDealerController);

    AddEditDealerController.$inject = ['$state', '$stateParams', 'toastr', 'dealer.service'];

    function AddEditDealerController($state, $stateParams, toastr, dealerService) {
        var vm = this;

        vm.types = ['Proprietorship','Partnership','LLP','Private Limited'];

        vm.mode = 'add';
        vm.addEditDealer = {
            'dealerName': '',
            'userName': '',
            'email':'',
            'mobileNumber1':'',
            'mobileNumber2':'',
            'mobileNumber3':'',
            'mobileNumber4':'',
            'mobileNumber5':'',
            'password': '',
            'verifyPassword':'',
            'dealerType':'',
            'address':'',
            'tnc':''
        };

        vm.edit = editDealer;
        vm.add = addDealer;

        activate();

        // ************************ //
        function activate() {
            vm.mode = angular.isString($state.params.dealerId) ? 'edit' : 'add';

            if(vm.mode === 'edit') {
                getDealer();
            }

            console.log($state.params);
        }

        function getDealer() {
            vm.isLoading = true;
            dealerService.getDealer($state.params.dealerId)
                .then(function (response) {
                    vm.isLoading = false;
                    arr = JSON.parse(response.data.data);
                    vm.addEditDealer = arr[0];            
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function addDealer() {
            vm.isLoading = true;

            dealerService.addDealer(vm.addEditDealer)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('dealers');
                    toastr.success("Dealer Added!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function editDealer() {
            vm.isLoading = true;

            dealerService.editDealer($state.params.dealerId, vm.addEditDealer)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('dealers');
                    toastr.success("Dealer Editted!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }
    }
})(window.angular);