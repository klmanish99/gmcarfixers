(function (angular) {

    angular
        .module('dealer')
        .service('dealer.service', DealerService);

    DealerService.$inject = ['$http'];

    function DealerService($http) {
        // alert('in3333');

        // alert(JSON.stringify($http.get('dealer')));
        return {
            getDealers: getDealers,
            getDealer: getDealerById,
            addDealer: addDealer,
            editDealer: editDealer,
            deleteDealer: deleteDealer,
            exportDealers: exportDealers
        };        

        function addDealer(payload) {
            return $http.post('dealer', payload);
        }

        function editDealer(dealerId, payload) {
            return $http.post('dealer/'+dealerId, payload);
        }

        function getDealers() {
            return $http.get('dealer');
        }
        
        function getDealerById(dealerId) {
            return $http.get('dealer/'+dealerId);
        }

        function deleteDealer(dealerId) {
            return $http.delete('dealer/' + dealerId);
        }

        function exportDealers() {
            return $http.get('dealer/export', {responseType:'arraybuffer'});
        }
    }

})(window.angular);