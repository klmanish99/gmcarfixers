(function (angular){

    angular
        .module('dealer')
        .controller('dealer.controller', DealerController);

    DealerController.$inject = ['$state', '$window', 'toastr', 'dealer.service'];

    function DealerController($state, $window, toastr, dealerService) {
        var vm = this;

        vm.dealers = [];
        vm.delete = deleteDealer;
        vm.move = moveToAddDealerPage;
        vm.showExport = false;
        vm.export = exportDealers;
        
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getDealers();
        }

        function getDealers() {
            //alert(JSON.stringify(dealerService));
            dealerService.getDealers()
                .then(function (response){
                    //alert(JSON.stringify(response));
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.dealers = data;
                    
                    angular.forEach(vm.dealers, function (dealer) {
                    	if(angular.isNumber(dealer.mobileNumber1)) {
                    		dealer.mobileNumberHref = "tel:0"+dealer.mobileNumber1;
                    	}
                    })
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddDealerPage(dealerId) {
            $state.go('addEditDealer', {'dealerId': dealerId});
        }

        function exportDealers() {
            vm.exporting = true;
            dealerService.exportDealers()
                .then(function (response) {
                    vm.exporting = false;
                    var byteArray = new Uint8Array(response.data);
                    var blob = new Blob([byteArray], { type: 'text/csv;charset=UTF-16'});
                    var fileURL = URL.createObjectURL(blob);
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.style = "display: none";
                    a.href = fileURL;
                    a.download = "dealers.csv"; //you may assign this value from header as well 
                    a.click();
                    window.URL.revokeObjectURL(fileURL)
                    
                }, function (error) {
                    vm.exporting = false;
                    toastr.error("Unable to export dealers. Please try again later!");
                });
        }   

        function deleteDealer(dealerId) {
        	if($window.confirm("Are you sure you wish to delete the dealer?")) {
	            vm.isLoading = true;
	
	            dealerService.deleteDealer(dealerId)
	                .then(function (response) {
	                    getDealers();
	                    toastr.success("Dealer deleted!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
        	}
        }
    }
})(window.angular);