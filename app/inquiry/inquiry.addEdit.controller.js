(function (angular) {

    angular
        .module('inquiry')
        .controller('inquiry.addEdit.controller', AddEditInquiryController);

    AddEditInquiryController.$inject = ['$state', '$stateParams', '$window', 'toastr', 'inquiry.service', 'workshop.service', 'driver.service', 'loginFactory', '$scope', '$http', '$timeout'];

    function AddEditInquiryController($state, $stateParams, $window, toastr, inquiryService, workshopService, driverService, loginFactory, $scope, $http, $timeout) {
        var vm = this;

        vm.mode = 'add';
        vm.hasCallMade = false;
        vm.mailing1 = false;
        vm.mailing2 = false;
        vm.loginFactory = loginFactory;
        vm.setValue = setValue;
        var temp = new Date();
        //alert(JSON.stringify(loginFactory.role));
        vm.addEditInquiry = {
            'driverId': '',
            'workshopId': '',
            'isComplete': '0',
            'customerName':'',
            'customerEmail':'',
            'customerMobile':'',
            'custDriverMobile':'',
            'caseDate':((temp.getDate() < 10)?"0":"") + temp.getDate() +"/"+(((temp.getMonth()+1) < 10)?"0":"") + (temp.getMonth()+1) +"/"+ temp.getFullYear(),
            'driverArriveHour': '',
            'driverArriveMin': '',
            'serviceType':'',
            'estimatedArriveMin':'',
            'caseType':'',
            'fileNo':'',
            'caseReference':'',
            'make':'',
            'model':'',
            'kmsReading':'',
            'carRegNo':'',
            'pickup':'',
            'drop':'',
            'driverComments':'',
            'amount': '',
            'dealerId': loginFactory.userId,
            'company_id':'',
            'role':loginFactory.role
        };
    
        vm.types = ['Call Center','Direct'];

        vm.workshops = [];
        vm.drivers = [];

        vm.edit = editInquiry;
        vm.add = addInquiry;
        
        vm.assignCurrentTime = assignCurrentTime;
        vm.call = call;
        vm.mail = mail;

        $scope.user = null;
        $scope.users = null;
            // Fetch data
          $scope.loadUsers = function($index) {

            //var searchText = vm.temp_companies[$index].user;
            //alert("in2");
            // Use timeout to simulate a 650ms request.
            return $timeout(function() {
              $scope.users =  $scope.users  ||
              $http({
                    method: 'post',
                    url: 'getData.php',
                    data: {user_id:loginFactory.userId,role:loginFactory.role}
                    }).then(function successCallback(response) {
                        $scope.users = response.data;
                       // alert(JSON.stringify(response.data[0]));
                    }, function(response) {
                          $scope.result = "Error";
                          $scope.content = response;
                      });
            }, 650);
          }
        function setValue (temp_companyId,$index,$event){
            var companyId = $scope.users[$index].id;
            //alert(companyId);
        }
        activate();

        // ************************ //
        function activate() {
            vm.mode = angular.isString($state.params.inquiryId) ? 'edit' : 'add';

            if(vm.mode === 'edit') {
                getInquiry();
            }

            vm.isLoading = true;
            getWorkshops();
            getDrivers();

            console.log($state.params);

        }

        function getWorkshops() {
            vm.isWorkshopLoading = true;
            workshopService.getWorkshops()
                .then(function (response){
                    vm.isWorkshopLoading = false;
                    vm.isLoading = vm.isWorkshopLoading || vm.isDriverLoading || vm.isDriverLoading;
                    data = JSON.parse(response.data.data);
                    vm.workshops = data;

                }, function (error){
                    vm.isWorkshopLoading = false;
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function getDrivers() {
            vm.isDriverLoading = true;
            driverService.getDrivers()
                .then(function (response){
                    vm.isDriverLoading = false;
                    vm.isLoading = vm.isWorkshopLoading || vm.isDriverLoading || vm.isDriverLoading;
                    data = JSON.parse(response.data.data);
                    vm.drivers = data;
                }, function (error){
                    vm.isDriverLoading = false;
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function getInquiry() {
            //alert(JSON.stringify(data[0]));
            vm.isInquiryLoading = true;
            inquiryService.getInquiry($state.params.inquiryId)
                .then(function (response) {
                    vm.isInquiryLoading = false;
                    vm.isLoading = vm.isWorkshopLoading || vm.isDriverLoading || vm.isDriverLoading;
                    arr = JSON.parse(response.data.data);
                    vm.addEditInquiry = arr[0];
                    if(angular.isNumber(vm.addEditInquiry.customerMobile)) {
                    	vm.addEditInquiry.customerNumberHref = "tel:0" + vm.addEditInquiry.customerMobile;
                	}
                	
                	if(angular.isNumber(vm.addEditInquiry.custDriverMobile)) {
                		vm.addEditInquiry.custDriverMobileHref = "tel:0" + vm.addEditInquiry.custDriverMobile;
                	}
                }, function (error) {
                    vm.isInquiryLoading = false;
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function formatRequest(request) {
            request.driverId = parseInt(request.driverId);
            request.workshopId = parseInt(request.workshopId);

            return request;
        }

        function addInquiry() {
        	if(!vm.hasCallMade) {
        		vm.hasCallMade = true;
	            vm.isLoading = true;
	
	            vm.addEditInquiry = formatRequest(vm.addEditInquiry);    
	            inquiryService.addInquiry(vm.addEditInquiry)
	                .then(function (response) {
	                    vm.isLoading = false;
	                    $state.go('inquiries');
                        //alert(JSON.stringify(vm.addEditInquiry));
	                    toastr.success("Inquiry Added!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                })
	                .finally(function () {
	                	vm.hasCallMade = false;
	                });
        	}
        }

        function editInquiry() {
        	if(!vm.hasCallMade) {
        		vm.hasCallMade = true;
	            vm.isLoading = true;
                //alert(JSON.stringify(vm.addEditInquiry));
	            inquiryService.editInquiry($state.params.inquiryId, vm.addEditInquiry)
	                .then(function (response) {
	                    vm.isLoading = false;
	                    $state.go('inquiries');
	                    toastr.success("Inquiry Editted!");
                        //alert(JSON.stringify(vm.addEditInquiry.company_id));
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                })
	                .finally(function (){
	                	vm.hasCallMade = false;
	                });
        	}
        }
        
        function assignCurrentTime() {
        	var now = new Date();
        	
        	vm.addEditInquiry.driverArriveHour = now.getHours();
        	vm.addEditInquiry.driverArriveMin = (now.getMinutes() - (now.getMinutes() % 5));

        }

        function call(whom) {
        	var a = document.createElement("a");
        	if((whom === 'customer' && !angular.isString(vm.addEditInquiry.customerNumberHref)) ||
        	   (whom !== 'customer' && !angular.isString(vm.addEditInquiry.custDriverMobileHref))) {
        		return;
        	}
        	
        	a.href = whom === 'customer' ? vm.addEditInquiry.customerNumberHref : vm.addEditInquiry.custDriverMobileHref;
        	
        	if(!angular.isString(a.href) || a.href === '')
        		return;
        	
            document.body.appendChild(a);
            a.style = "display: none";
            a.href = whom === 'customer' ? vm.addEditInquiry.customerNumberHref : vm.addEditInquiry.custDriverMobileHref;
            a.click();
        }
        
        function mail(whom) {
        	if(whom === 'customer' && angular.isString(vm.addEditInquiry.inquiryId) && vm.addEditInquiry.customerEmail && vm.addEditInquiry.customerEmail !== '') {
        		if($window.confirm("Are you sure you wish to send a customer copy of the job sheet?")) {
    	            vm.mailing2 = true;
    	
    	            inquiryService.mailCustomer($state.params.inquiryId)
    	                .then(function (response) {
    	                	vm.mailing2 = false;
    	                	toastr.success("Job sheet copy sent to the customer.");
    	                }, function (error) {
    	                	vm.mailing2 = false;;
    	                    errorMessage = error.data;
    	                    toastr.error(errorMessage.data); 
    	                });
            	}
        	} else if (whom === 'workshop' && angular.isString(vm.addEditInquiry.inquiryId) && angular.isString(vm.addEditInquiry.workshopEmail) && vm.addEditInquiry.workshopEmail && vm.addEditInquiry.workshopEmail !== '') {
        		if($window.confirm("Are you sure you wish to send a workshop copy of the job sheet?")) {
    	            vm.mailing1 = true;
    	
    	            inquiryService.mailWorkshop($state.params.inquiryId)
    	                .then(function (response) {
    	                	vm.mailing1 = false;
    	                	toastr.success("Job sheet copy sent to the workshop.");
    	                }, function (error) {
    	                	vm.mailing1 = false;;
    	                    errorMessage = error.data;
    	                    toastr.error(errorMessage.data); 
    	                });
            	}
        	}
        	
        	
        }
    }
})(window.angular);