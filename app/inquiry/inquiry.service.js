(function (angular) {

    angular
        .module('inquiry')
        .service('inquiry.service', InquiryService);

    InquiryService.$inject = ['$http'];

    function InquiryService($http) {
        return {
            getInquiries: getInquiries,
            getInquiry: getInquiryById,
            addInquiry: addInquiry,
            editInquiry: editInquiry,
            deleteInquiry: deleteInquiry,
            exportInquiries: exportInquiries,
            mailCustomer: mailCustomerCopy,
            mailWorkshop: mailWorkshopCopy
        };        

        function addInquiry(payload) {
            return $http.post('inquiry', payload);
        }

        function editInquiry(inquiryId, payload) {
            return $http.post('inquiry/'+inquiryId, payload);
        }

        function getInquiries() {
            return $http.get('inquiry');
        }
        
        function exportInquiries() {
            return $http.get('inquiry/export', {responseType:'arraybuffer'});
        }
        
        function getInquiryById(inquiryId) {
            return $http.get('inquiry/'+inquiryId);
        }

        function deleteInquiry(inquiryId) {
            return $http.delete('inquiry/' + inquiryId);
        }
        
        function mailCustomerCopy(inquiryId){
        	return $http.get('mail/'+inquiryId+'/customer');
        }
        
        function mailWorkshopCopy(inquiryId){
        	return $http.get('mail/'+inquiryId+'/workshop');
        }
    }

})(window.angular);