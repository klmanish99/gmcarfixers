(function (angular) {
	
	angular
		.module('inquiry')
		.controller('inquiry.generate.controller', GenerateInquiryController);
	
	GenerateInquiryController.$inject = ['$state', '$stateParams'];
	
	function GenerateInquiryController($state, $stateParams) {
		var vm = this;
		
		vm.inquiry = {};
		
		vm.download = download;
		
		activate();
		
		function activate() {
			vm.inquiry = angular.isObject($state.params.inquiry) ? $state.params.inquiry : {};
		}
		
		function download() {
			html2canvas(document.getElementById('invoice'), {
	            onrendered: function (canvas) {
	                var data = canvas.toDataURL();
	                var docDefinition = {
	                    content: [{
	                        image: data,
	                        width: 500,
	                    }]
	                };
	                pdfMake.createPdf(docDefinition).download("test.pdf");
	            }
	        });
		}
	}
})(window.angular);