(function (angular){

    angular
        .module('inquiry')
        .controller('inquiry.controller', InquiryController);

    InquiryController.$inject = ['$scope', '$state', '$timeout', '$mdDialog', 'toastr', 'inquiry.service', 'loginFactory'];

    function InquiryController($scope, $state, $timeout, $mdDialog, toastr, inquiryService, loginFactory) {
        var vm = this;

        vm.inquiries = [];
        
        vm.delete = deleteInquiry;
        vm.move = moveToAddInquiryPage;
        vm.upload = moveToUploadImages;
        vm.export = exportInquiries;
        vm.generate = generateInvoice;
        vm.showExport = false;
        vm.openMenu = openMenu;
        vm.loginFactory = loginFactory;
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getInquiries();
        }
        
        function getInquiries() {
            inquiryService.getInquiries()
                .then(function (response){
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.inquiries = data;
                    
                    angular.forEach(vm.inquiries, function(inquiry) {
                    	if(angular.isNumber(inquiry.driverNumber)) {
                    		inquiry.driverNumberHref = "tel:0" + inquiry.driverNumber;
                    	}
                    	
                    	if(angular.isNumber(inquiry.workshopNumber)) {
                    		inquiry.workshopNumberHref = "tel:0" + inquiry.workshopNumber;
                    	}
                    	
                    	if(angular.isNumber(inquiry.customerMobile)) {
                    		inquiry.customerNumberHref = "tel:0" + inquiry.customerMobile;
                    	}
                    	
                    	if(angular.isNumber(inquiry.custDriverMobile)) {
                    		inquiry.custDriverMobileHref = "tel:0" + inquiry.custDriverMobile;
                    	}
                    });
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddInquiryPage(inquiryId) {
            $state.go('addEditInquiry', {'inquiryId': inquiryId});
        }

        function deleteInquiry(inquiryId) {
            vm.isLoading = true;

            inquiryService.deleteInquiry(inquiryId)
                .then(function (response) {
                    getInquiries();
                    toastr.success("Inquiry deleted!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }
        
        function exportInquiries() {
        	vm.exporting = true;
        	inquiryService.exportInquiries()
        		.then(function (response) {
        			vm.exporting = false;
        			var byteArray = new Uint8Array(response.data);
        			var blob = new Blob([byteArray], { type: 'text/csv;charset=UTF-16'});
        	        var fileURL = URL.createObjectURL(blob);
    	            var a = document.createElement("a");
    	            document.body.appendChild(a);
    	            a.style = "display: none";
    	            a.href = fileURL;
    	            a.download = "inquiries.csv"; //you may assign this value from header as well 
    	            a.click();
    	            window.URL.revokeObjectURL(fileURL)
        			
        		}, function (error) {
        			vm.exporting = false;
        			toastr.error("Unable to export inquiries. Please try again later!");
        		});
        }   

        function generateInvoice(inquiry) {
        	$state.go('invoice', { 'inquiry': inquiry});
        }
        
        function moveToUploadImages(inquiryId) {
        	$state.go('uploadInquiryImages', {'inquiryId': inquiryId});
        }
        
        function openMenu($mdMenu, ev) {
        	originatorEv = ev;
        	$mdMenu.open(ev);
        }
    }
})(window.angular);