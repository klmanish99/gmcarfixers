(function (angular){

    angular
        .module('technician')
        .controller('technician.controller', TechnicianController);

    TechnicianController.$inject = ['$state', '$window', 'toastr', 'technician.service'];

    function TechnicianController($state, $window, toastr, technicianService) {
        var vm = this;

        vm.technicians = [];
        vm.delete = deleteTechnician;
        vm.move = moveToAddTechnicianPage;
        vm.upload = moveToUploadImages;
        
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getTechnicians();
        }

        function getTechnicians() {
            technicianService.getTechnicians()
                .then(function (response){
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.technicians = data;
                    
                    angular.forEach(vm.technicians, function(technician) {
                		if(angular.isNumber(technician.mobileNumber)) {
                			technician.mobileNumberHref = "tel:0"+technician.mobileNumber;
                		}
                		if(angular.isNumber(technician.alternateNo)) {
                			technician.alternateNoHref = "tel:0"+technician.alternateNo;
                		}
                    });
                    
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddTechnicianPage(technicianId) {
            $state.go('addEditTechnician', {'technicianId': technicianId});
        }

        function deleteTechnician(technicianId) {
        	if($window.confirm("Are you sure you wish to delete the technician?")) {
	            vm.isLoading = true;
	
	            technicianService.deleteTechnician(technicianId)
	                .then(function (response) {
	                    getTechnicians();
	                    toastr.success("Technician deleted!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
        	}
        }
        
        function moveToUploadImages(technicianId) {
        	$state.go('uploadTechnicianImages', {'technicianId': technicianId});
        }
    }
})(window.angular);