(function(angular) {

	angular.module('technician').controller('technician.addEdit.controller',
			AddEditTechnicianController);

	AddEditTechnicianController.$inject = [ '$state', 'Upload', '$stateParams',
			'toastr', 'technician.service', 'login.service', 'loginFactory' ];

	function AddEditTechnicianController($state, Upload, $stateParams, toastr,
			technicianService, loginService, loginFactory) {
		var vm = this;
		var userId = loginFactory.userId;
		vm.mode = 'add';
		vm.hasCallMade = false;
		vm.addEditTechnician = {
			'technicianName' : '',
			'dealer_id' : userId,
			'userName' : '',
			'password' : '',
			'address' : '',
			'mobileNumber' : '',
			'alternateNo' : '',
			'dob' : '',
			'email' : '',
			'verifyPassword' : '',
			'profilePic': '',
			'licensePic': '',
			'idProofPic': ''
		};

		vm.view = 'detail';
		vm.edit = editTechnician;
		vm.add = addTechnician;

		activate();

		// ************************ //
		function activate() {
			vm.mode = angular.isString($state.params.technicianId) ? 'edit' : 'add';

			if (vm.mode === 'edit') {
				getTechnician();
			}

			console.log($state.params);
		}

		function getTechnician() {
			vm.isLoading = true;
			technicianService.getTechnician($state.params.technicianId).then(
					function(response) {
						vm.isLoading = false;
						arr = JSON.parse(response.data.data);
						vm.addEditTechnician = arr[0];
					}, function(error) {
						vm.isLoading = false;
						errorMessage = error.data;
						toastr.error(errorMessage.data);
					});
		}

		function addTechnician() {
			if(!vm.hasCallMade){
				vm.hasCallMade = true;
				vm.isLoading = true;
	
				technicianService.addTechnician(vm.addEditTechnician).then(function(response) {
					vm.isLoading = false;
					resp = JSON.parse(response.data.data);
					vm.addEditTechnician.technicianId = resp.technicianId;
					//transitionToUpload();
					$state.go('technicians');
					//toastr.success("Technician Added!");
				}, function(error) {
					vm.isLoading = false;
					errorMessage = error.data;
					toastr.error(errorMessage.data);
				})
				.finally(function() {
					vm.hasCallMade = false;
				});
			}
		}

		function editTechnician() {
			if(!vm.hasCallMade) {
				vm.hasCallMade = true;
				vm.isLoading = true;
	
				technicianService.editTechnician($state.params.technicianId, vm.addEditTechnician)
					.then(function(response) {
						vm.isLoading = false;
						//transitionToUpload();
						$state.go('technicians');
						//toastr.success("Technician Editted!");
					}, function(error) {
						vm.isLoading = false;
						errorMessage = error.data;
						toastr.error(errorMessage.data);
					})
					.finally(function() {
						vm.hasCallMade = false;
					});
			}
		}
	}
})(window.angular);