(function (angular) {

    angular
        .module('technician')
        .service('technician.service', TechnicianService);

    TechnicianService.$inject = ['$http'];

    function TechnicianService($http) {
        return {
            getTechnicians: getTechnicians,
            getTechnician: getTechnicianById,
            addTechnician: addTechnician,
            editTechnician: editTechnician,
            deleteTechnician: deleteTechnician
        };        

        function addTechnician(payload) {
            return $http.post('technician', payload);
        }

        function editTechnician(technicianId, payload) {
            return $http.post('technician/'+technicianId, payload);
        }

        function getTechnicians() {
            return $http.get('technician');
        }
        
        function getTechnicianById(technicianId) {
            return $http.get('technician/'+technicianId);
        }

        function deleteTechnician(technicianId) {
            return $http.delete('technician/' + technicianId);
        }
    }

})(window.angular);