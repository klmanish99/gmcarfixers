(function (angular){

    angular
        .module('temp_company')
        .controller('temp_company.controller', Temp_companyController);

    Temp_companyController.$inject = ['$state', '$window', 'toastr', 'temp_company.service','loginFactory', '$scope', '$http', '$timeout'];

    function Temp_companyController($state, $window, toastr, temp_companyService,loginFactory, $scope, $http, $timeout) {
        var vm = this;

        vm.temp_companies = [];
        vm.delete = deleteTemp_company;
        vm.move = move_companyById;
        vm.loginFactory = loginFactory;
        //vm.setValue = setValue
        //alert(JSON.stringify(vm.loginFactory));
        $scope.user = null;
        $scope.users = null;
            // Fetch data
          $scope.loadUsers = function($index) {

            //var searchText = vm.temp_companies[$index].user;
            //alert("in2");
            // Use timeout to simulate a 650ms request.
            return $timeout(function() {
              $scope.users =  $scope.users  ||
              $http({
                    method: 'post',
                    url: 'getData.php',
                    data: {user_id:loginFactory.userId,role:loginFactory.role}
                    }).then(function successCallback(response) {
                        $scope.users = response.data;
                        //alert(JSON.stringify(response.data[0]));
                    }, function(response) {
                          $scope.result = "Error";
                          $scope.content = response;
                      });
            }, 650);
          }
          /*function setValue(temp_company,$index,$event){
              alert(temp_company);
              //alert(temp_company);
              //alert(temp_companyId);
              //alert($index);
              //alert($event);
          }*/
          $scope.setValue = function(temp_companyId,index,$event){
                var cmpny_id = $scope.users[index].id;

                alert(cmpny_id);
                alert(temp_companyId);
                //alert(index);
                $scope.searchText = $scope.searchResult[index].name;
                $scope.searchId = $scope.searchResult[index].id;
                alert($scope.searchText);
                alert($scope.searchId);
                $scope.searchResult = {};
                $event.stopPropagation();
            }
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getTemp_companies();
        }

        function getTemp_companies() {
           // alert('in');
           //alert(JSON.stringify(temp_companyService));
            temp_companyService.getTemp_companies()
                .then(function (response){
                    //alert(JSON.stringify(response));
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.temp_companies = data;
                    
                    angular.forEach(vm.temp_companies, function (temp_company) {
                    	if(angular.isNumber(temp_company.mobileNumber1)) {
                    		temp_company.mobileNumberHref = "tel:0"+temp_company.mobileNumber1;
                    	}
                    })
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function move_companyById(temp_companyId) {
            //alert('ID :'+temp_companyId);
            vm.isLoading = true;
            temp_companyService.move_companyById(temp_companyId)
                    .then(function (response) {
                        getTemp_companies();
                        toastr.success("Company added");
                    }, function (error) {
                        vm.isLoading = false;
                        errorMessage = error.data;
                        toastr.error(errorMessage.data); 
                    });
            //$state.go('addEditTemp_company', {'companyId': temp_companyId});
        }

        function deleteTemp_company(temp_companyId,index,$event) {
                var cmpny_id = $scope.users[index].id;
                //alert(cmpny_id);
                //alert(temp_companyId);
         
        	if($window.confirm("Do you really want to assign to this company?")) {
	            vm.isLoading = true;
	            temp_companyService.deleteTemp_company(temp_companyId,cmpny_id)
	                .then(function (response) {
	                    getTemp_companies();
	                    toastr.success("Company Assigned");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
        	//}
        }
    }
}
})(window.angular);