(function (angular) {

    angular
        .module('temp_company')
        .service('temp_company.service', Temp_companyService);

    Temp_companyService.$inject = ['$http'];

    function Temp_companyService($http) {
            //alert('in3333');

        //alert(JSON.stringify($http.get('temp_company')));
        
        return {
            getTemp_companies: getTemp_companies,
            getTemp_company: getTemp_companyById,
            addTemp_company: addTemp_company,
            editTemp_company: editTemp_company,
            deleteTemp_company: deleteTemp_company,
            move_companyById: move_companyById,
        };        

        function addTemp_company(payload) {
            return $http.post('temp_company', payload);
        }

        function editTemp_company(temp_companyId, payload) {
            return $http.post('temp_company/'+temp_companyId, payload);
        }

        function getTemp_companies() {
            return $http.get('temp_company');
        }
        
        function getTemp_companyById(temp_companyId) {
            return $http.get('temp_company/'+temp_companyId);
        }

        function move_companyById(temp_companyId) {
            return $http.get('temp_company/'+temp_companyId);
        }

        function deleteTemp_company(temp_companyId, cmpny_id) {
            return $http.delete('temp_company/' + temp_companyId +'123456789123456789'+ cmpny_id);
        }
    }

})(window.angular);