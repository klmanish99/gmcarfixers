(function (angular){

    angular
        .module('temp_company')
        .controller('temp_company.controller', Temp_companyController);

    Temp_companyController.$inject = ['$state', '$window', 'toastr', 'temp_company.service', '$scope', '$http'];

    function Temp_companyController($state, $window, toastr, temp_companyService, $scope, $http) {
        var vm = this;

        vm.temp_companies = [];
        vm.delete = deleteTemp_company;
        vm.move = move_companyById;
            // Fetch data
            $scope.fetchUsers = function(temp_companyId,$index,$event){
                //alert($index);
                //alert(JSON.stringify(vm.temp_companies));
                //anjurkar harshalialert(JSON.stringify(vm.temp_companies[$index].searchText));
                //alert($scope.searchText);
                var searchText = vm.temp_companies[$index].searchText;
                //alert(searchText);
                var searchText_len = searchText.trim().length;
                //alert(searchText_len);
                // Check search text length
                if(searchText_len > 0){
                    //alert("in");
                    $http({
                    method: 'post',
                    url: 'getData.php',
                    data: {searchText:searchText}
                    }).then(function successCallback(response) {
                        //alert("Success");
                        //alert(JSON.stringify(response));
                        $scope.searchResult = response.data;
                    
                    }, function(response) {
                        //alert("Error");
                          $scope.result = "Error";
                          $scope.content = response;
                      });
                }else{
                    //alert("else");
                    $scope.searchResult = {};
                }
                
            }
            // Set value to search box
            $scope.setValue = function(temp_companyId,index,$event){
                alert(temp_companyId);
                $scope.searchText = $scope.searchResult[index].name;
                $scope.searchId = $scope.searchResult[index].id;
                alert($scope.searchText);
                alert($scope.searchId);
                $scope.searchResult = {};
                $event.stopPropagation();
            }

            
            function searchboxClicked ($event,$index){
                //alert($index);
                $event.stopPropagation();
            }

            /*$scope.containerClicked = function(){
                $scope.searchResult = {};
            }*/
            

        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getTemp_companies();
        }

        function getTemp_companies() {
           // alert('in');
           //alert(JSON.stringify(temp_companyService));
            temp_companyService.getTemp_companies()
                .then(function (response){
                    //alert(JSON.stringify(response));
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.temp_companies = data;
                    
                    angular.forEach(vm.temp_companies, function (temp_company) {
                    	if(angular.isNumber(temp_company.mobileNumber1)) {
                    		temp_company.mobileNumberHref = "tel:0"+temp_company.mobileNumber1;
                    	}
                    })
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function move_companyById(temp_companyId) {
            //alert('ID :'+temp_companyId);
            vm.isLoading = true;
            temp_companyService.move_companyById(temp_companyId)
                    .then(function (response) {
                        getTemp_companies();
                        toastr.success("Company added");
                    }, function (error) {
                        vm.isLoading = false;
                        errorMessage = error.data;
                        toastr.error(errorMessage.data); 
                    });
            //$state.go('addEditTemp_company', {'companyId': temp_companyId});
        }

        function deleteTemp_company(temp_companyId) {
        	//if($window.confirm("Are you sure you wish to delete the temp_company?")) {
	            vm.isLoading = true;
	            temp_companyService.deleteTemp_company(temp_companyId)
	                .then(function (response) {
	                    getTemp_companies();
	                    toastr.success("Company Assigned");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
        	//}
        }
    }
})(window.angular);