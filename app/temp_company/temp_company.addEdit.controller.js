(function (angular) {

    angular
        .module('temp_company')
        .controller('temp_company.addEdit.controller', AddEditTemp_companyController);

    AddEditTemp_companyController.$inject = ['$state', '$stateParams', 'toastr', 'temp_company.service', 'login.service', 'loginFactory'];

    function AddEditTemp_companyController($state, $stateParams, toastr, temp_companyService, loginService, loginFactory) {
        var vm = this;
        var userId = loginFactory.userId;
        vm.types = ['Proprietorship','Partnership','LLP','Private Limited'];

        vm.mode = 'add';
        vm.addEditTemp_company = {
            'temp_companyName': '',
            'userName': '',
            'email':'',
            'mobileNumber1':'',
            'mobileNumber2':'',
            'mobileNumber3':'',
            'mobileNumber4':'',
            'mobileNumber5':'',
            'dealer_id':userId,
            'password': '',
            'verifyPassword':'',
            'temp_companyType':'',
            'address':'',
            'tnc':''
        };

        vm.edit = editTemp_company;
        vm.add = addTemp_company;

        activate();

        // ************************ //
        function activate() {
            vm.mode = angular.isString($state.params.temp_companyId) ? 'edit' : 'add';

            if(vm.mode === 'edit') {
                getTemp_company();
            }

            console.log($state.params);
        }

        function getTemp_company() {
            vm.isLoading = true;
            temp_companyService.getTemp_company($state.params.temp_companyId)
                .then(function (response) {
                    vm.isLoading = false;
                    arr = JSON.parse(response.data.data);
                    vm.addEditTemp_company = arr[0];            
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function addTemp_company() {
            vm.isLoading = true;

            temp_companyService.addTemp_company(vm.addEditTemp_company)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('temp_companies');
                    toastr.success("Temp_company Added!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function editTemp_company() {
            vm.isLoading = true;

            temp_companyService.editTemp_company($state.params.temp_companyId, vm.addEditTemp_company)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('temp_companies');
                    toastr.success("Temp_company Editted!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }
    }
})(window.angular);