(function (angular) {

    angular
        .module('contactUs')
        .service('contactUs.service', ContactUsService);

    ContactUsService.$inject = ['$http'];

    function ContactUsService($http) {
        return {
            getContactUss: getContactUss,
            getContactUs: getContactUsById,
            addContactUs: addContactUs,
            editContactUs: editContactUs,
            deleteContactUs: deleteContactUs
        };        

        function addContactUs(payload) {
            return $http.post('contactUs', payload);
        }

        function editContactUs(contactUsId, payload) {
            return $http.post('contactUs/'+contactUsId, payload);
        }

        function getContactUss() {
            //return $http.get('contactUs');
        }
        
        function getContactUsById(contactUsId) {
            return $http.get('contactUs/'+contactUsId);
        }

        function deleteContactUs(contactUsId) {
            return $http.delete('contactUs/' + contactUsId);
        }
    }

})(window.angular);