(function (angular){

    angular
        .module('contactUs')
        .controller('contactUs.controller', ContactUsController);

    ContactUsController.$inject = ['$state', '$window', 'toastr', 'contactUs.service'];

    function ContactUsController($state, $window, toastr, contactUsService) {
        var vm = this;

        vm.contactUss = [];
        vm.delete = deleteContactUs;
        vm.move = moveToAddContactUsPage;
        
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getContactUss();
        }

        function getContactUss() {
            contactUsService.getContactUss()
                .then(function (response){
                    //alert(JSON.stringify(response));
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.contactUss = data;
                    
                    angular.forEach(vm.contactUss, function (contactUs) {
                    	if(angular.isNumber(contactUs.mobileNumber1)) {
                    		contactUs.mobileNumberHref = "tel:0"+contactUs.mobileNumber1;
                    	}
                    })
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddContactUsPage(contactUsId) {
            $state.go('addEditContactUs', {'contactUsId': contactUsId});
        }

        function deleteContactUs(contactUsId) {
        	if($window.confirm("Are you sure you wish to delete the contactUs?")) {
	            vm.isLoading = true;
	
	            contactUsService.deleteContactUs(contactUsId)
	                .then(function (response) {
	                    getContactUss();
	                    toastr.success("ContactUs deleted!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
        	}
        }
    }
})(window.angular);