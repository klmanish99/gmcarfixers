(function (angular) {

    angular
        .module('login')
        .service('loginFactory', LoginFactory);

    LoginFactory.$inject = [];

    function LoginFactory() {

    	this.isLoggedIn = false;
    	this.role = '';
    	this.pageHeading = '';
        this.pageHeadColor = '';
    	this.dealerId = '';
    }
})(window.angular);