(function (angular) {
    
    angular
        .module('login')
        .controller('login.controller', LoginController);

    LoginController.$inject = ['$scope','$state', 'toastr', 'login.service', 'loginFactory'];

    function LoginController($scope, $state, toastr, loginService, loginFactory) {
        var vm = this;
        // Request Payload
        vm.credentials = {
                'user': '',
                'password': ''
        };
        
        vm.login = login;
        vm.move = moveToForgotPassword;
        vm.hasCallMade = false;

        activate();

        // **************************** //

        function activate(){
        }

        function login() {
        	if(!vm.hasCallMade) {
        		vm.hasCallMade = true;
	            loginService.login(vm.credentials)
	                .then(function (response){
	                    data = JSON.parse(response.data.data);
                        //alert(JSON.stringify(response));
	                    loginFactory.userName = data['user-name'];
	                    loginFactory.isLoggedIn = true;
	                    loginFactory.role = data.role;
                        loginFactory.userId = data['user-id'];
                        loginFactory.dealerId = data['dealer_id'];
                        //alert(JSON.stringify(loginFactory));
	                    $scope.$emit('login');
                        if(loginFactory.role === 'Master') {
                            $state.go("dealers");
                        }
                        else if(loginFactory.role === 'Dealer') {
                            $state.go("inquiries");
                        }
                        else if(loginFactory.role === 'Technician') {
                            $state.go("tech_inquiries");
                        }
                        else if(loginFactory.role === 'Company') {
                            $state.go("towfixers_inquiries");
                        }
                        else {
                            $state.go("inquiries");
                        }
	                }, function (error) {
	                    toastr.error(error.data.data);
	                })
	                .finally(function () {
	                	vm.hasCallMade = false;
	                });
        	}
        }
        
        function moveToForgotPassword() {
        	$state.go('forgotPassword');        
        }
    }
})(window.angular);