(function (angular) {
    
    angular
        .module('login')
        .controller('login.forgotPassword.controller', ForgotPasswordController);

    ForgotPasswordController.$inject = ['$scope','$state', 'toastr', 'login.service', 'loginFactory'];

    function ForgotPasswordController($scope, $state, toastr, loginService, loginFactory) {
        var vm = this;
        // Request Payload
        vm.forgotCredentials = {
                'user': '',
                'password': '',
                'newPassword': '',
                'confirmNewPassword':''
        };
        
        $scope.user = {};

        vm.change = change;	
        vm.getUser=getUser;
        
        activate();

        // **************************** //

        function activate(){
        	vm.userFound = false;
        	vm.hasCallMade = false;
        	vm.page = 'user';
        	vm.mobile = '';
        }
        
        function change() {
        	if(!vm.hasCallMade) {
        		
	        	if(vm.forgotCredentials.newPassword !== vm.forgotCredentials.confirmNewPassword) {
	        		return;
	        	}
	        	
	        	if((vm.user.txt_role === 'Company' && vm.mobile != vm.user.txt_mobile1) || 
	        		(vm.user.txt_role === 'Driver' && vm.mobile != vm.user.txt_mobile)) {
	        		toastr.error('Incorrect Mobile Number. Please enter your registered mobile number');
	        		return;
	        	}
	        	
	        	vm.hasCallMade = true;
	        	loginService.change(vm.forgotCredentials)
		            .then(function (response){
		            	toastr.success("Password has been updated");
		                $state.go('login', {});
		            }, function (error) {
		                toastr.error(error.data.data);
		            }).
		            finally(function () {
		            	vm.hasCallMade = false;
		            });
        	}
        }
        
        function getUser(userId)
        {
        	if(angular.isString(userId)) {
	        	loginService.getUser(userId)
	        		.then(function (response) {
	        			vm.userFound = true;
	        			vm.user = JSON.parse(response.data.data);
	        			vm.page = 'pass';
	        		}, function (error) {
	        			toastr.error(error.data.data);
	        		})
	        }
        }
    }
})(window.angular);