(function (angular){
    angular
        .module('login')
        .service('login.service', LoginService);

    LoginService.$inject = ['$http'];

    function LoginService($http) {
        return {
            login: login,
            logout: logout,
            change: change,
            getUser: getUser,
            getUserId: getUserId
        };

        function login(payload) {
            return $http.post('user/login',payload);
        }

        function logout() {
            return $http.post('user/logout');
        }
        
        function change(payload) {
        	return $http.post('user/change',payload);
        }
        
        function getUser(userName) {
        	return $http.get('user/ids?id='+userName);
        }
        function getUserId(userId) {
            return $http.get('user/ids?id='+userId);
        }

    }
})(window.angular);