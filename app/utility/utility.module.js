(function (angular){

    angular
        .module('utility', []);

    angular
        .module('utility')
        .config(function (toastrConfig){
            angular.extend(toastrConfig, {
                allowHtml: true,
                closeButton: true,
                closeHtml: '<button>&times;</button>',
                positionClass: 'toast-bottom-center',
                extendedTimeOut: 1000,
                iconClasses: {
                    error: 'toast-error',
                    info: 'toast-info',
                    success: 'toast-success',
                    warning: 'toast-warning'
                },  
                messageClass: 'toast-message',
                onHidden: null,
                onShown: null,
                onTap: null,
                progressBar: false,
                tapToDismiss: true,
                templates: {
                toast: 'directives/toast/toast.html',
                progressbar: 'directives/progressbar/progressbar.html'
                },
                timeOut: 10000,
                titleClass: 'toast-title',
                toastClass: 'toast'
            });
        });
})(window.angular);