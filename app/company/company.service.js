(function (angular) {

    angular
        .module('company')
        .service('company.service', CompanyService);

    CompanyService.$inject = ['$http'];

    function CompanyService($http) {
        return {
            getCompanies: getCompanies,
            getCompany: getCompanyById,
            addCompany: addCompany,
            editCompany: editCompany,
            deleteCompany: deleteCompany
        };        

        function addCompany(payload) {
            return $http.post('company', payload);
        }

        function editCompany(companyId, payload) {
            return $http.post('company/'+companyId, payload);
        }

        function getCompanies() {
            return $http.get('company');
        }
        
        function getCompanyById(companyId) {
            return $http.get('company/'+companyId);
        }

        function deleteCompany(companyId) {
            return $http.delete('company/' + companyId);
        }
    }

})(window.angular);