(function (angular) {

    angular
        .module('company')
        .controller('company.addEdit.controller', AddEditCompanyController);

    AddEditCompanyController.$inject = ['$state', '$stateParams', 'toastr', 'company.service', 'login.service', 'loginFactory'];

    function AddEditCompanyController($state, $stateParams, toastr, companyService, loginService, loginFactory) {
        var vm = this;
        var userId = loginFactory.userId;
        vm.types = ['Proprietorship','Partnership','LLP','Private Limited'];

        vm.mode = 'add';
        vm.addEditCompany = {
            'companyName': '',
            'userName': '',
            'email':'',
            'mobileNumber1':'',
            'mobileNumber2':'',
            'mobileNumber3':'',
            'mobileNumber4':'',
            'mobileNumber5':'',
            'dealer_id':userId,
            'password': '',
            'verifyPassword':'',
            'companyType':'',
            'address':'',
            'tnc':''
        };

        vm.edit = editCompany;
        vm.add = addCompany;

        activate();

        // ************************ //
        function activate() {
            vm.mode = angular.isString($state.params.companyId) ? 'edit' : 'add';

            if(vm.mode === 'edit') {
                getCompany();
            }

            console.log($state.params);
        }

        function getCompany() {
            vm.isLoading = true;
            companyService.getCompany($state.params.companyId)
                .then(function (response) {
                    vm.isLoading = false;
                    arr = JSON.parse(response.data.data);
                    vm.addEditCompany = arr[0];            
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function addCompany() {
            vm.isLoading = true;

            companyService.addCompany(vm.addEditCompany)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('companies');
                    toastr.success("Company Added!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function editCompany() {
            vm.isLoading = true;

            companyService.editCompany($state.params.companyId, vm.addEditCompany)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('companies');
                    toastr.success("Company Editted!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }
    }
})(window.angular);