(function (angular){

    angular
        .module('company')
        .controller('company.controller', CompanyController);

    CompanyController.$inject = ['$state', '$window', 'toastr', 'company.service'];

    function CompanyController($state, $window, toastr, companyService) {
        var vm = this;

        vm.companies = [];
        vm.delete = deleteCompany;
        vm.move = moveToAddCompanyPage;
        
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getCompanies();
        }

        function getCompanies() {
            companyService.getCompanies()
                .then(function (response){
                    //alert(JSON.stringify(response));
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.companies = data;
                    
                    angular.forEach(vm.companies, function (company) {
                    	if(angular.isNumber(company.mobileNumber1)) {
                    		company.mobileNumberHref = "tel:0"+company.mobileNumber1;
                    	}
                    })
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddCompanyPage(companyId) {
            $state.go('addEditCompany', {'companyId': companyId});
        }

        function deleteCompany(companyId) {
        	if($window.confirm("Are you sure you wish to delete the company?")) {
	            vm.isLoading = true;
	
	            companyService.deleteCompany(companyId)
	                .then(function (response) {
	                    getCompanies();
	                    toastr.success("Company deleted!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
        	}
        }
    }
})(window.angular);