(function (angular) {

    angular
        .module('towfixers_inquiry')
        .service('towfixers_inquiry.service', Towfixers_inquiryService);

    Towfixers_inquiryService.$inject = ['$http'];

    function Towfixers_inquiryService($http) {
        return {
            getTowfixers_inquiries: getTowfixers_inquiries,
            getTowfixers_inquiry: getTowfixers_inquiryById,
            addTowfixers_inquiry: addTowfixers_inquiry,
            editTowfixers_inquiry: editTowfixers_inquiry,
            deleteTowfixers_inquiry: deleteTowfixers_inquiry
        };        

        function addTowfixers_inquiry(payload) {
            return $http.post('towfixers_inquiry', payload);
        }

        function editTowfixers_inquiry(towfixers_inquiryId, payload) {
            return $http.post('towfixers_inquiry/'+towfixers_inquiryId, payload);
        }

        function getTowfixers_inquiries() {
            return $http.get('towfixers_inquiry');
        }
        
        function getTowfixers_inquiryById(towfixers_inquiryId) {
            return $http.get('towfixers_inquiry/'+towfixers_inquiryId);
        }

        function deleteTowfixers_inquiry(towfixers_inquiryId) {
            return $http.delete('towfixers_inquiry/' + towfixers_inquiryId);
        }
    }

})(window.angular);