(function (angular){

    angular
        .module('towfixers_inquiry')
        .controller('towfixers_inquiry.controller', Towfixers_inquiryController);

    Towfixers_inquiryController.$inject = ['$state', '$window', 'toastr', 'towfixers_inquiry.service'];

    function Towfixers_inquiryController($state, $window, toastr, towfixers_inquiryService) {
        var vm = this;

        vm.towfixers_inquiries = [];
        vm.delete = deleteTowfixers_inquiry;
        vm.move = moveToAddTowfixers_inquiryPage;
        
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getTowfixers_inquiries();
        }

        function getTowfixers_inquiries() {
            towfixers_inquiryService.getTowfixers_inquiries()
                .then(function (response){
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.towfixers_inquiries = data;
                    //alert(JSON.stringify(vm.towfixers_inquiries));
                    angular.forEach(vm.towfixers_inquiries, function(ws) {
                    	if(angular.isNumber(ws.mobileNumber)) {
                    		ws.mobileNumberHref = "tel:0" + ws.mobileNumber;
                    	}
                    });
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddTowfixers_inquiryPage(towfixers_inquiryId) {
            $state.go('addEditTowfixers_inquiry', {'towfixers_inquiryId': towfixers_inquiryId});
        }

        function deleteTowfixers_inquiry(towfixers_inquiryId) {
            if($window.confirm("Are you sure you wish to delete the towfixers_inquiry?")) {
            	vm.isLoading = true;

	            towfixers_inquiryService.deleteTowfixers_inquiry(towfixers_inquiryId)
	                .then(function (response) {
	                    getTowfixers_inquiries();
	                    toastr.success("Towfixers_inquiry deleted!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
            }
        }
    }
})(window.angular);