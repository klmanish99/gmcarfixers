(function (angular) {

    angular
        .module('towfixers_inquiry')
        .controller('towfixers_inquiry.addEdit.controller', AddEditController);

    AddEditController.$inject = ['$state', '$stateParams', 'toastr', 'towfixers_inquiry.service'];

    function AddEditController($state, $stateParams, toastr, towfixers_inquiryService) {
        var vm = this;

        vm.mode = 'add';
        vm.addEditTowfixers_inquiry = {
            'towfixers_inquiryName': '',
            'email':'',
            'mobileNumber':'',
            'address':''
        };

        vm.edit = editTowfixers_inquiry;
        vm.add = addTowfixers_inquiry;
        vm.typesOfService = ['Flatbed Truck Towing',
                             'Hydraulic Underlift Towing',
                             'Battery Jump Start Service',
                             'Normal Towing',
                             'Roadside Assistance Allied Service'
                             ];

        activate();

        // ************************ //
        function activate() {
            vm.mode = angular.isString($state.params.towfixers_inquiryId) ? 'edit' : 'add';

            if(vm.mode === 'edit') {
                getTowfixers_inquiry();
            }

            console.log($state.params);
        }

        function getTowfixers_inquiry() {
            vm.isLoading = true;
            towfixers_inquiryService.getTowfixers_inquiry($state.params.towfixers_inquiryId)
                .then(function (response) {
                    vm.isLoading = false;
                    arr = JSON.parse(response.data.data);
                    vm.addEditTowfixers_inquiry = arr[0]; 
                    //alert(JSON.stringify(vm.addEditTowfixers_inquiry));
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function addTowfixers_inquiry() {
            vm.isLoading = true;

            towfixers_inquiryService.addTowfixers_inquiry(vm.addEditTowfixers_inquiry)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('towfixers_inquiries');
                    toastr.success("Towfixers_inquiry Added!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function editTowfixers_inquiry() {
            vm.isLoading = true;

            towfixers_inquiryService.editTowfixers_inquiry($state.params.towfixers_inquiryId, vm.addEditTowfixers_inquiry)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('towfixers_inquiries');
                    toastr.success("Towfixers_inquiry Editted!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }
    }
})(window.angular);