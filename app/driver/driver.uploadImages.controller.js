(function (angular){
	
	angular
		.module('driver')
		.controller('driver.uploadImages.controller', UploadDriverController);
	
	UploadDriverController.$inject = ['$state', 'Upload', '$stateParams',
		'toastr', 'driver.service' ];
	
	function UploadDriverController($state, Upload, $stateParams, 
			toastr, driverService) {
		
		var vm = this;
	
		vm.addEditDriver = {};
		
		vm.profilePic = '';
		vm.licensePic = '',
		vm.idProofPic = '';
		
		vm.profileProgress = 0;
		vm.licenseProgress = 0;
		vm.idProofProgress = 0;
		
		vm.uploadProfilePic = uploadProfilePic;
		vm.uploadLicensePic = uploadLicensePic;
		vm.uploadIdProofPic = uploadIdProofPic;
		
		vm.maximize = maximizeImage;
		
		activate();
		
		// ************************** //
		
		function activate() {
			getDriver();
		}
		
		function getDriver() {
			vm.isLoading = true;
			driverService.getDriver($state.params.driverId).then(
					function(response) {
						vm.isLoading = false;
						arr = JSON.parse(response.data.data);
						vm.addEditDriver = arr[0];
					}, function(error) {
						vm.isLoading = false;
						errorMessage = error.data;
						toastr.error(errorMessage.data);
					});
		}
		
		function maximizeImage(imgId) {
			// Get the modal
			var modal = document.getElementById('myModal');

			// Get the image and insert it inside the modal - use its "alt" text as a caption
			var img = document.getElementById(imgId);
			var modalImg = document.getElementById("img01");
			//var captionText = document.getElementById("caption");
			img.onclick = function(){
			    modal.style.display = "block";
			    modalImg.src = this.src;
			    //captionText.innerHTML = this.alt;
			}

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("closeButton")[0];

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() { 
			    modal.style.display = "none";
			}
		}
		
		function uploadProfilePic(profilePic) {
			Upload.upload({
				url : 'driver/'+ vm.addEditDriver.driverId +'/upload',
				data : {
					file : profilePic,
					type : 'profile'
				}
			}).then(
				function(resp) {
					response = JSON.parse(resp.data.data);
					vm.addEditDriver.profilePic = response.fileName;
				},
				function(resp) {
					errorMessage = error.data;
					toastr.error(errorMessage.data);
				},
				function(evt) {
					vm.profileProgress = parseInt(100.0 * evt.loaded/ evt.total);
					var elem = document.getElementById('profileProgressBar');
					elem.style.width = vm.profileProgress + "%";
				});
		}
		
		function uploadLicensePic(licensePic) {
			Upload.upload({
				url : 'driver/'+ vm.addEditDriver.driverId +'/upload',
				data : {
					file : licensePic,
					type : 'license'
				}
			}).then(
				function(resp) {
					response = JSON.parse(resp.data.data);
					vm.addEditDriver.licensePic = response.fileName;
				},
				function(resp) {
					errorMessage = error.data;
					toastr.error(errorMessage.data);
				},
				function(evt) {
					vm.licenseProgress = parseInt(100.0 * evt.loaded/ evt.total);
					var elem = document.getElementById('licenseProgressBar');
					elem.style.width = vm.licenseProgress + "%";
				});
		}
		
		function uploadIdProofPic(idProofPic) {
			Upload.upload({
				url : 'driver/'+ vm.addEditDriver.driverId +'/upload',
				data : {
					file : idProofPic,
					type : 'idproof'
				}
			}).then(
				function(resp) {
					response = JSON.parse(resp.data.data);
					vm.addEditDriver.idProofPic = response.fileName;
				},
				function(resp) {
					errorMessage = error.data;
					toastr.error(errorMessage.data);
				},
				function(evt) {
					vm.idProofProgress = parseInt(100.0 * evt.loaded/ evt.total);
					var elem = document.getElementById('idProofProgressBar');
					elem.style.width = vm.idProofProgress + "%";
				});
		}

	}
})(window.angular);
