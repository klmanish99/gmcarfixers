(function (angular) {

    angular
        .module('driver')
        .service('driver.service', DriverService);

    DriverService.$inject = ['$http'];

    function DriverService($http) {
        return {
            getDrivers: getDrivers,
            getDriver: getDriverById,
            addDriver: addDriver,
            editDriver: editDriver,
            deleteDriver: deleteDriver
        };        

        function addDriver(payload) {
            return $http.post('driver', payload);
        }

        function editDriver(driverId, payload) {
            return $http.post('driver/'+driverId, payload);
        }

        function getDrivers() {
            return $http.get('driver');
        }
        
        function getDriverById(driverId) {
            return $http.get('driver/'+driverId);
        }

        function deleteDriver(driverId) {
            return $http.delete('driver/' + driverId);
        }
    }

})(window.angular);