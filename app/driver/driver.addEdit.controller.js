(function(angular) {

	angular.module('driver').controller('driver.addEdit.controller',
			AddEditDriverController);

	AddEditDriverController.$inject = [ '$state', 'Upload', '$stateParams',
			'toastr', 'driver.service' ];

	function AddEditDriverController($state, Upload, $stateParams, toastr,
			driverService) {
		var vm = this;

		vm.mode = 'add';
		vm.hasCallMade = false;
		vm.addEditDriver = {
			'driverName' : '',
			'userName' : '',
			'password' : '',
			'address' : '',
			'mobileNumber' : '',
			'alternateNo' : '',
			'dob' : '',
			'email' : '',
			'verifyPassword' : '',
			'profilePic': '',
			'licensePic': '',
			'idProofPic': ''
		};

		vm.view = 'detail';
		vm.edit = editDriver;
		vm.add = addDriver;

		activate();

		// ************************ //
		function activate() {
			vm.mode = angular.isString($state.params.driverId) ? 'edit' : 'add';

			if (vm.mode === 'edit') {
				getDriver();
			}

			console.log($state.params);
		}

		function getDriver() {
			vm.isLoading = true;
			driverService.getDriver($state.params.driverId).then(
					function(response) {
						vm.isLoading = false;
						arr = JSON.parse(response.data.data);
						vm.addEditDriver = arr[0];
					}, function(error) {
						vm.isLoading = false;
						errorMessage = error.data;
						toastr.error(errorMessage.data);
					});
		}

		function addDriver() {
			if(!vm.hasCallMade){
				vm.hasCallMade = true;
				vm.isLoading = true;
	
				driverService.addDriver(vm.addEditDriver).then(function(response) {
					vm.isLoading = false;
					resp = JSON.parse(response.data.data);
					vm.addEditDriver.driverId = resp.driverId;
					//transitionToUpload();
					$state.go('drivers');
					//toastr.success("Driver Added!");
				}, function(error) {
					vm.isLoading = false;
					errorMessage = error.data;
					toastr.error(errorMessage.data);
				})
				.finally(function() {
					vm.hasCallMade = false;
				});
			}
		}

		function editDriver() {
			if(!vm.hasCallMade) {
				vm.hasCallMade = true;
				vm.isLoading = true;
	
				driverService.editDriver($state.params.driverId, vm.addEditDriver)
					.then(function(response) {
						vm.isLoading = false;
						//transitionToUpload();
						$state.go('drivers');
						//toastr.success("Driver Editted!");
					}, function(error) {
						vm.isLoading = false;
						errorMessage = error.data;
						toastr.error(errorMessage.data);
					})
					.finally(function() {
						vm.hasCallMade = false;
					});
			}
		}
	}
})(window.angular);