(function (angular){

    angular
        .module('driver')
        .controller('driver.controller', DriverController);

    DriverController.$inject = ['$state', '$window', 'toastr', 'driver.service'];

    function DriverController($state, $window, toastr, driverService) {
        var vm = this;

        vm.drivers = [];
        vm.delete = deleteDriver;
        vm.move = moveToAddDriverPage;
        vm.upload = moveToUploadImages;
        
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getDrivers();
        }

        function getDrivers() {
            driverService.getDrivers()
                .then(function (response){
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.drivers = data;
                    
                    angular.forEach(vm.drivers, function(driver) {
                		if(angular.isNumber(driver.mobileNumber)) {
                			driver.mobileNumberHref = "tel:0"+driver.mobileNumber;
                		}
                		if(angular.isNumber(driver.alternateNo)) {
                			driver.alternateNoHref = "tel:0"+driver.alternateNo;
                		}
                    });
                    
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddDriverPage(driverId) {
            $state.go('addEditDriver', {'driverId': driverId});
        }

        function deleteDriver(driverId) {
        	if($window.confirm("Are you sure you wish to delete the driver?")) {
	            vm.isLoading = true;
	
	            driverService.deleteDriver(driverId)
	                .then(function (response) {
	                    getDrivers();
	                    toastr.success("Driver deleted!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
        	}
        }
        
        function moveToUploadImages(driverId) {
        	$state.go('uploadDriverImages', {'driverId': driverId});
        }
    }
})(window.angular);