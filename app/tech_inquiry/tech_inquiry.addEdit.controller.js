(function (angular) {

    angular
        .module('tech_inquiry')
        .controller('tech_inquiry.addEdit.controller', AddEditTech_inquiryController);

    AddEditTech_inquiryController.$inject = ['$state', '$stateParams', '$window', 'toastr', 'tech_inquiry.service', 'workshop.service', 'driver.service', 'loginFactory', '$scope', '$http', '$timeout'];

    function AddEditTech_inquiryController($state, $stateParams, $window, toastr, tech_inquiryService, workshopService, driverService, loginFactory, $scope, $http, $timeout) {
        var vm = this;

        vm.mode = 'add';
        vm.hasCallMade = false;
        vm.mailing1 = false;
        vm.mailing2 = false;
        vm.loginFactory = loginFactory;
        vm.setValue = setValue;
        vm.prblmSolved = false;
        vm.carTowed = false;
        var temp = new Date();
        //alert(JSON.stringify(loginFactory.role));
        vm.addEditTech_inquiry = {
            'isComplete': '0',
            'caseDate':((temp.getDate() < 10)?"0":"") + temp.getDate() +"/"+(((temp.getMonth()+1) < 10)?"0":"") + (temp.getMonth()+1) +"/"+ temp.getFullYear(),
            'carRegNo':'',
            'carBrand':'',
            'carModel':'',
            'customerLocation':'',
            'techObservation':'',
            'amount': '',
            'dealerId': loginFactory.userId,
            'role':loginFactory.role
        };
    
        vm.prblmSolvedTypes = ['Yes','No'];
        vm.isCarTowed = ['Yes','No'];

        vm.workshops = [];
        vm.drivers = [];

        vm.edit = editTech_inquiry;
        vm.add = addTech_inquiry;
        
        vm.selectOptionsPrblm = selectOptionsPrblm;
        vm.selectOptionsTowe = selectOptionsTowe;
        vm.call = call;
        vm.mail = mail;

        $scope.user = null;
        $scope.users = null;
            // Fetch data
          $scope.loadUsers = function($index) {

            //var searchText = vm.temp_companies[$index].user;
            //alert("in2");
            // Use timeout to simulate a 650ms request.
            return $timeout(function() {
              $scope.users =  $scope.users  ||
              $http({
                    method: 'post',
                    url: 'getTechnicianData.php',
                    data: {user_id:loginFactory.userId,role:loginFactory.role}
                    }).then(function successCallback(response) {
                        $scope.users = response.data;
                       // alert(JSON.stringify(response.data[0]));
                    }, function(response) {
                          $scope.result = "Error";
                          $scope.content = response;
                      });
            }, 650);
          }
          $scope.loadVendor = function($index) {

            //var searchText = vm.temp_companies[$index].user;
            //alert("in2");
            // Use timeout to simulate a 650ms request.
            return $timeout(function() {
              $scope.users =  $scope.users  ||
              $http({
                    method: 'post',
                    url: 'getData.php',
                    data: {user_id:loginFactory.userId,role:loginFactory.role}
                    }).then(function successCallback(response) {
                        $scope.users = response.data;
                       // alert(JSON.stringify(response.data[0]));
                    }, function(response) {
                          $scope.result = "Error";
                          $scope.content = response;
                      });
            }, 650);
          }
        function setValue (temp_companyId,$index,$event){
            var companyId = $scope.users[$index].id;
            //alert(companyId);
        }

        activate();

        // ************************ //
        function activate() {
            vm.mode = angular.isString($state.params.tech_inquiryId) ? 'edit' : 'add';

            if(vm.mode === 'edit') {
                getTech_inquiry();
            }

            vm.isLoading = true;
            getWorkshops();
            getDrivers();

            console.log($state.params);

        }

        function getWorkshops() {
            vm.isWorkshopLoading = true;
            workshopService.getWorkshops()
                .then(function (response){
                    vm.isWorkshopLoading = false;
                    vm.isLoading = vm.isWorkshopLoading || vm.isDriverLoading || vm.isDriverLoading;
                    data = JSON.parse(response.data.data);
                    vm.workshops = data;

                }, function (error){
                    vm.isWorkshopLoading = false;
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function getDrivers() {
            vm.isDriverLoading = true;
            driverService.getDrivers()
                .then(function (response){
                    vm.isDriverLoading = false;
                    vm.isLoading = vm.isWorkshopLoading || vm.isDriverLoading || vm.isDriverLoading;
                    data = JSON.parse(response.data.data);
                    vm.drivers = data;
                }, function (error){
                    vm.isDriverLoading = false;
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function getTech_inquiry() {
            //alert(JSON.stringify(data));
            vm.isTech_inquiryLoading = true;
            tech_inquiryService.getTech_inquiry($state.params.tech_inquiryId)
                .then(function (response) {
                    vm.isTech_inquiryLoading = false;
                    vm.isLoading = vm.isWorkshopLoading || vm.isDriverLoading || vm.isDriverLoading;
                    arr = JSON.parse(response.data.data);
                    //alert(JSON.stringify(arr[0]));
                    vm.addEditTech_inquiry = arr[0];
                    if(angular.isNumber(vm.addEditTech_inquiry.customerMobile)) {
                    	vm.addEditTech_inquiry.customerNumberHref = "tel:0" + vm.addEditTech_inquiry.customerMobile;
                	}
                	
                	if(angular.isNumber(vm.addEditTech_inquiry.custDriverMobile)) {
                		vm.addEditTech_inquiry.custDriverMobileHref = "tel:0" + vm.addEditTech_inquiry.custDriverMobile;
                	}
                }, function (error) {
                    vm.isTech_inquiryLoading = false;
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function formatRequest(request) {
            //request.driverId = parseInt(request.driverId);
            //request.workshopId = parseInt(request.workshopId);

            return request;
        }

        function addTech_inquiry() {
        	if(!vm.hasCallMade) {
        		vm.hasCallMade = true;
	            vm.isLoading = true;
	
	            vm.addEditTech_inquiry = formatRequest(vm.addEditTech_inquiry);    
	            tech_inquiryService.addTech_inquiry(vm.addEditTech_inquiry)
	                .then(function (response) {
	                    vm.isLoading = false;
	                    $state.go('tech_inquiries');
                        //alert(JSON.stringify(vm.addEditTech_inquiry));
	                    toastr.success("Tech_inquiry Added!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                })
	                .finally(function () {
	                	vm.hasCallMade = false;
	                });
        	}
        }

        function editTech_inquiry() {
        	if(!vm.hasCallMade) {
        		vm.hasCallMade = true;
	            vm.isLoading = true;
	            tech_inquiryService.editTech_inquiry($state.params.tech_inquiryId, vm.addEditTech_inquiry)
	                .then(function (response) {
	                    vm.isLoading = false;
	                    $state.go('tech_inquiries');
	                    toastr.success("Tech_inquiry Editted!");
                        //alert(JSON.stringify(vm.addEditTech_inquiry.company_id));
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                })
	                .finally(function (){
	                	vm.hasCallMade = false;
	                });
        	}
        }
        
        function selectOptionsPrblm(prblmSolvedType) {
            if (prblmSolvedType == 'No') {
               vm.prblmSolved = true;
            } else {
                vm.prblmSolved = false;
            }
        }
        function selectOptionsTowe(isCarTowe) {
            if(isCarTowe == 'Yes') {
               vm.carTowed = true;
            } else {
                vm.carTowed = false;
            }
        }
        function call(whom) {
        	var a = document.createElement("a");
        	if((whom === 'customer' && !angular.isString(vm.addEditTech_inquiry.customerNumberHref)) ||
        	   (whom !== 'customer' && !angular.isString(vm.addEditTech_inquiry.custDriverMobileHref))) {
        		return;
        	}
        	
        	a.href = whom === 'customer' ? vm.addEditTech_inquiry.customerNumberHref : vm.addEditTech_inquiry.custDriverMobileHref;
        	
        	if(!angular.isString(a.href) || a.href === '')
        		return;
        	
            document.body.appendChild(a);
            a.style = "display: none";
            a.href = whom === 'customer' ? vm.addEditTech_inquiry.customerNumberHref : vm.addEditTech_inquiry.custDriverMobileHref;
            a.click();
        }
        
        function mail(whom) {
        	if(whom === 'customer' && angular.isString(vm.addEditTech_inquiry.tech_inquiryId) && vm.addEditTech_inquiry.customerEmail && vm.addEditTech_inquiry.customerEmail !== '') {
        		if($window.confirm("Are you sure you wish to send a customer copy of the job sheet?")) {
    	            vm.mailing2 = true;
    	
    	            tech_inquiryService.mailCustomer($state.params.tech_inquiryId)
    	                .then(function (response) {
    	                	vm.mailing2 = false;
    	                	toastr.success("Job sheet copy sent to the customer.");
    	                }, function (error) {
    	                	vm.mailing2 = false;;
    	                    errorMessage = error.data;
    	                    toastr.error(errorMessage.data); 
    	                });
            	}
        	} else if (whom === 'workshop' && angular.isString(vm.addEditTech_inquiry.tech_inquiryId) && angular.isString(vm.addEditTech_inquiry.workshopEmail) && vm.addEditTech_inquiry.workshopEmail && vm.addEditTech_inquiry.workshopEmail !== '') {
        		if($window.confirm("Are you sure you wish to send a workshop copy of the job sheet?")) {
    	            vm.mailing1 = true;
    	
    	            tech_inquiryService.mailWorkshop($state.params.tech_inquiryId)
    	                .then(function (response) {
    	                	vm.mailing1 = false;
    	                	toastr.success("Job sheet copy sent to the workshop.");
    	                }, function (error) {
    	                	vm.mailing1 = false;;
    	                    errorMessage = error.data;
    	                    toastr.error(errorMessage.data); 
    	                });
            	}
        	}
        	
        	
        }
    }
})(window.angular);