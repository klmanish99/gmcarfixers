(function (angular){
	
	angular
		.module('tech_inquiry')
		.controller('tech_inquiry.uploadImages.controller', UploadTech_inquiryController);
	
	UploadTech_inquiryController.$inject = ['$scope','$state', 'Upload', '$stateParams',
		'toastr', 'tech_inquiry.service', 'loginFactory' ];
	
	function UploadTech_inquiryController($scope, $state, Upload, $stateParams, 
			toastr, tech_inquiryService, loginFactory) {
		
		var vm = this;
	
		vm.tech_inquiry = {};
		
		vm.custSignProgress = 0;
		vm.licenseProgress = 0;
		vm.carInvProgress = 0;
		
		vm.uploadCustomerSign = uploadCustomerSign;
		vm.uploadWorkshopSign = uploadWorkshopSign;
		vm.uploadCarInventory = uploadCarInventory;
		vm.loginFactory = loginFactory;
		vm.maximize = maximizeImage;
		activate();
		
		// ************************** //
		
		function activate() {
			getTech_inquiry();
		}
		
		function getTech_inquiry() {
			vm.isLoading = true;
			tech_inquiryService.getTech_inquiry($state.params.tech_inquiryId).then(
					function(response) {
						vm.isLoading = false;
						arr = JSON.parse(response.data.data);
						vm.tech_inquiry = arr[0];
					}, function(error) {
						vm.isLoading = false;
						errorMessage = error.data;
						toastr.error(errorMessage.data);
					});
		}
		
		function maximizeImage(imgId) {
			// Get the modal
			var modal = document.getElementById('myModal');

			// Get the image and insert it inside the modal - use its "alt" text as a caption
			var img = document.getElementById(imgId);
			var modalImg = document.getElementById("img01");
			//var captionText = document.getElementById("caption");
			img.onclick = function(){
			    modal.style.display = "block";
			    modalImg.src = this.src;
			    //captionText.innerHTML = this.alt;
			}

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("closeButton")[0];

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() { 
			    modal.style.display = "none";
			}
		}
		
		function dataURLtoFile(dataurl, filename) {
		    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
		        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
		    while(n--){
		        u8arr[n] = bstr.charCodeAt(n);
		    }
		    return new File([u8arr], filename, {type:mime});
		}
		
		function uploadCustomerSign() {
			var signature = $scope.acceptCust();
			var customerSignImageFile = dataURLtoFile(signature.dataUrl, vm.tech_inquiry.tech_inquiryId + "customerSign.png");
			
			Upload.upload({
				url : 'tech_inquiry/'+ vm.tech_inquiry.tech_inquiryId +'/upload',
				data : {
					file : customerSignImageFile,
					type : 'custSign',
					signBy: vm.tech_inquiry.custSignBy
				}
			}).then(
				function(resp) {
					var response = JSON.parse(resp.data.data);
					vm.tech_inquiry.custSignPic = response.fileName;
					$state.go('tech_inquiries');

				},
				function(error) {
					var errorMessage = error.data;
					//toastr.error(errorMessage.data);
					$state.go('tech_inquiries');
				},
				function(evt) {
					vm.custSignProgress = parseInt(100.0 * evt.loaded/ evt.total);
					var elem = document.getElementById('custSignProgressBar');
					elem.style.width = vm.custSignProgress + "%";
					$state.go('tech_inquiries');
				});
		}
		
		function uploadWorkshopSign() {
			var signature = $scope.acceptWorkshop();
			
			var workshopSignImageFile = dataURLtoFile(signature.dataUrl, vm.tech_inquiry.tech_inquiryId + "workshopSign.png");
			
			Upload.upload({
				url : 'tech_inquiry/'+ vm.tech_inquiry.tech_inquiryId +'/upload',
				data : {
					file : workshopSignImageFile,
					type : 'wsSign',
					signBy: vm.tech_inquiry.wsSignBy
				}
			}).then(
				function(resp) {
					response = JSON.parse(resp.data.data);
					vm.tech_inquiry.workshopSignPic = response.fileName;
				},
				function(resp) {
					errorMessage = error.data;
					toastr.error(errorMessage.data);
				},
				function(evt) {
					vm.wsSignProgress = parseInt(100.0 * evt.loaded/ evt.total);
					var elem = document.getElementById('wsSignProgressBar');
					elem.style.width = vm.wsSignProgress + "%";
				});
		}
		
		function uploadCarInventory() {
			var carInv = $scope.acceptCarInventory();
			
			var carInvImageFile = dataURLtoFile(carInv.dataUrl, vm.tech_inquiry.tech_inquiryId + "carInv.png");
			
			Upload.upload({
				url : 'tech_inquiry/'+ vm.tech_inquiry.tech_inquiryId +'/upload',
				data : {
					file : carInvImageFile,
					type : 'carInv',
					signBy: vm.tech_inquiry.carInvComments
				}
			}).then(
				function(resp) {
					response = JSON.parse(resp.data.data);
					vm.tech_inquiry.carInvPic = response.fileName;
				},
				function(resp) {
					errorMessage = error.data;
					toastr.error(errorMessage.data);
				},
				function(evt) {
					vm.carInvProgress = parseInt(100.0 * evt.loaded/ evt.total);
					var elem = document.getElementById('carInvProgressBar');
					elem.style.width = vm.carInvProgress + "%";
				});
		}

	}
})(window.angular);