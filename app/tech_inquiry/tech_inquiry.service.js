(function (angular) {

    angular
        .module('tech_inquiry')
        .service('tech_inquiry.service', Tech_inquiryService);

    Tech_inquiryService.$inject = ['$http'];

    function Tech_inquiryService($http) {
        return {
            getTech_inquiries: getTech_inquiries,
            getTech_inquiry: getTech_inquiryById,
            addTech_inquiry: addTech_inquiry,
            editTech_inquiry: editTech_inquiry,
            deleteTech_inquiry: deleteTech_inquiry,
            exportTech_inquiries: exportTech_inquiries,
            mailCustomer: mailCustomerCopy,
            mailWorkshop: mailWorkshopCopy
        };        

        function addTech_inquiry(payload) {
            return $http.post('tech_inquiry', payload);
        }

        function editTech_inquiry(tech_inquiryId, payload) {
            return $http.post('tech_inquiry/'+tech_inquiryId, payload);
        }

        function getTech_inquiries() {
            return $http.get('tech_inquiry');
        }
        
        function exportTech_inquiries() {
            return $http.get('tech_inquiry/export', {responseType:'arraybuffer'});
        }
        
        function getTech_inquiryById(tech_inquiryId) {
            return $http.get('tech_inquiry/'+tech_inquiryId);
        }

        function deleteTech_inquiry(tech_inquiryId) {
            return $http.delete('tech_inquiry/' + tech_inquiryId);
        }
        
        function mailCustomerCopy(tech_inquiryId){
        	return $http.get('mail/'+tech_inquiryId+'/customer');
        }
        
        function mailWorkshopCopy(tech_inquiryId){
        	return $http.get('mail/'+tech_inquiryId+'/workshop');
        }
    }

})(window.angular);