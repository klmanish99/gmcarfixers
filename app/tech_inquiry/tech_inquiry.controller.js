(function (angular){

    angular
        .module('tech_inquiry')
        .controller('tech_inquiry.controller', Tech_inquiryController);

    Tech_inquiryController.$inject = ['$scope', '$state', '$window', '$timeout', '$mdDialog', 'toastr', 'tech_inquiry.service', 'loginFactory'];

    function Tech_inquiryController($scope, $state, $window, $timeout, $mdDialog, toastr, tech_inquiryService, loginFactory) {
        var vm = this;

        vm.tech_inquiries = [];
        
        vm.delete = deleteTech_inquiry;
        vm.move = moveToAddTech_inquiryPage;
        vm.upload = moveToUploadImages;
        vm.export = exportTech_inquiries;
        vm.generate = generateInvoice;
        vm.showExport = false;
        vm.openMenu = openMenu;
        vm.loginFactory = loginFactory;
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getTech_inquiries();
        }
        
        function getTech_inquiries() {
            tech_inquiryService.getTech_inquiries()
                .then(function (response){
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    //alert(JSON.stringify(data));
                    vm.tech_inquiries = data;
                    
                    angular.forEach(vm.tech_inquiries, function(tech_inquiry) {
                    	if(angular.isNumber(tech_inquiry.driverNumber)) {
                    		tech_inquiry.driverNumberHref = "tel:0" + tech_inquiry.driverNumber;
                    	}
                    	
                    	if(angular.isNumber(tech_inquiry.workshopNumber)) {
                    		tech_inquiry.workshopNumberHref = "tel:0" + tech_inquiry.workshopNumber;
                    	}
                    	
                    	if(angular.isNumber(tech_inquiry.customerMobile)) {
                    		tech_inquiry.customerNumberHref = "tel:0" + tech_inquiry.customerMobile;
                    	}
                    	
                    	if(angular.isNumber(tech_inquiry.custDriverMobile)) {
                    		tech_inquiry.custDriverMobileHref = "tel:0" + tech_inquiry.custDriverMobile;
                    	}
                    });
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddTech_inquiryPage(tech_inquiryId) {
            $state.go('addEditTech_inquiry', {'tech_inquiryId': tech_inquiryId});
        }

        function deleteTech_inquiry(tech_inquiryId) {
            if($window.confirm("Are you sure you wish to delete the Tech Inquiry?")) {
                vm.isLoading = true;
                tech_inquiryService.deleteTech_inquiry(tech_inquiryId)
                    .then(function (response) {
                        getTech_inquiries();
                        toastr.success("Tech_inquiry deleted!");
                    }, function (error) {
                        vm.isLoading = false;
                        errorMessage = error.data;
                        toastr.error(errorMessage.data); 
                    });
            }
        }
        
        function exportTech_inquiries() {
        	vm.exporting = true;
        	tech_inquiryService.exportTech_inquiries()
        		.then(function (response) {
        			vm.exporting = false;
        			var byteArray = new Uint8Array(response.data);
        			var blob = new Blob([byteArray], { type: 'text/csv;charset=UTF-16'});
        	        var fileURL = URL.createObjectURL(blob);
    	            var a = document.createElement("a");
    	            document.body.appendChild(a);
    	            a.style = "display: none";
    	            a.href = fileURL;
    	            a.download = "tech_inquiries.csv"; //you may assign this value from header as well 
    	            a.click();
    	            window.URL.revokeObjectURL(fileURL)
        			
        		}, function (error) {
        			vm.exporting = false;
        			toastr.error("Unable to export tech_inquiries. Please try again later!");
        		});
        }   

        function generateInvoice(tech_inquiry) {
        	$state.go('invoice', { 'tech_inquiry': tech_inquiry});
        }
        
        function moveToUploadImages(tech_inquiryId) {
        	$state.go('uploadTech_inquiryImages', {'tech_inquiryId': tech_inquiryId});
        }
        
        function openMenu($mdMenu, ev) {
        	originatorEv = ev;
        	$mdMenu.open(ev);
        }
    }
})(window.angular);