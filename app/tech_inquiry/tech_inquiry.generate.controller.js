(function (angular) {
	
	angular
		.module('tech_inquiry')
		.controller('tech_inquiry.generate.controller', GenerateTech_inquiryController);
	
	GenerateTech_inquiryController.$inject = ['$state', '$stateParams'];
	
	function GenerateTech_inquiryController($state, $stateParams) {
		var vm = this;
		
		vm.tech_inquiry = {};
		
		vm.download = download;
		
		activate();
		
		function activate() {
			vm.tech_inquiry = angular.isObject($state.params.tech_inquiry) ? $state.params.tech_inquiry : {};
		}
		
		function download() {
			html2canvas(document.getElementById('invoice'), {
	            onrendered: function (canvas) {
	                var data = canvas.toDataURL();
	                var docDefinition = {
	                    content: [{
	                        image: data,
	                        width: 500,
	                    }]
	                };
	                pdfMake.createPdf(docDefinition).download("test.pdf");
	            }
	        });
		}
	}
})(window.angular);