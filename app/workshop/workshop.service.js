(function (angular) {

    angular
        .module('workshop')
        .service('workshop.service', WorkshopService);

    WorkshopService.$inject = ['$http'];

    function WorkshopService($http) {
        return {
            getWorkshops: getWorkshops,
            getWorkshop: getWorkshopById,
            addWorkshop: addWorkshop,
            editWorkshop: editWorkshop,
            deleteWorkshop: deleteWorkshop
        };        

        function addWorkshop(payload) {
            return $http.post('workshop', payload);
        }

        function editWorkshop(workshopId, payload) {
            return $http.post('workshop/'+workshopId, payload);
        }

        function getWorkshops() {
            return $http.get('workshop');
        }
        
        function getWorkshopById(workshopId) {
            return $http.get('workshop/'+workshopId);
        }

        function deleteWorkshop(workshopId) {
            return $http.delete('workshop/' + workshopId);
        }
    }

})(window.angular);