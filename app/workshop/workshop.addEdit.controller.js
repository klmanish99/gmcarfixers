(function (angular) {

    angular
        .module('workshop')
        .controller('workshop.addEdit.controller', AddEditController);

    AddEditController.$inject = ['$state', '$stateParams', 'toastr', 'workshop.service'];

    function AddEditController($state, $stateParams, toastr, workshopService) {
        var vm = this;

        vm.mode = 'add';
        vm.addEditWorkshop = {
            'workshopName': '',
            'email':'',
            'mobileNumber':'',
            'address':''
        };

        vm.edit = editWorkshop;
        vm.add = addWorkshop;

        activate();

        // ************************ //
        function activate() {
            vm.mode = angular.isString($state.params.workshopId) ? 'edit' : 'add';

            if(vm.mode === 'edit') {
                getWorkshop();
            }

            console.log($state.params);
        }

        function getWorkshop() {
            vm.isLoading = true;
            workshopService.getWorkshop($state.params.workshopId)
                .then(function (response) {
                    vm.isLoading = false;
                    arr = JSON.parse(response.data.data);
                    vm.addEditWorkshop = arr[0];            
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function addWorkshop() {
            vm.isLoading = true;

            workshopService.addWorkshop(vm.addEditWorkshop)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('workshops');
                    toastr.success("Workshop Added!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function editWorkshop() {
            vm.isLoading = true;

            workshopService.editWorkshop($state.params.workshopId, vm.addEditWorkshop)
                .then(function (response) {
                    vm.isLoading = false;
                    $state.go('workshops');
                    toastr.success("Workshop Editted!");
                }, function (error) {
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }
    }
})(window.angular);