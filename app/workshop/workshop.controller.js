(function (angular){

    angular
        .module('workshop')
        .controller('workshop.controller', WorkshopController);

    WorkshopController.$inject = ['$state', '$window', 'toastr', 'workshop.service'];

    function WorkshopController($state, $window, toastr, workshopService) {
        var vm = this;

        vm.workshops = [];
        vm.delete = deleteWorkshop;
        vm.move = moveToAddWorkshopPage;
        
        activate();

        // **************** //

        function activate() {
            vm.isLoading = true;

            getWorkshops();
        }

        function getWorkshops() {
            workshopService.getWorkshops()
                .then(function (response){
                    vm.isLoading = false;
                    data = JSON.parse(response.data.data);
                    vm.workshops = data;
                    
                    angular.forEach(vm.workshops, function(ws) {
                    	if(angular.isNumber(ws.mobileNumber)) {
                    		ws.mobileNumberHref = "tel:0" + ws.mobileNumber;
                    	}
                    });
                }, function (error){
                    vm.isLoading = false;
                    errorMessage = error.data;
                    toastr.error(errorMessage.data); 
                });
        }

        function moveToAddWorkshopPage(workshopId) {
            $state.go('addEditWorkshop', {'workshopId': workshopId});
        }

        function deleteWorkshop(workshopId) {
            if($window.confirm("Are you sure you wish to delete the workshop?")) {
            	vm.isLoading = true;

	            workshopService.deleteWorkshop(workshopId)
	                .then(function (response) {
	                    getWorkshops();
	                    toastr.success("Workshop deleted!");
	                }, function (error) {
	                    vm.isLoading = false;
	                    errorMessage = error.data;
	                    toastr.error(errorMessage.data); 
	                });
            }
        }
    }
})(window.angular);