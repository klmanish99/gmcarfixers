(function(angular) {
	
	app.constant('appGlobal', {'role': 'None'});

	app.config(function($stateProvider, $urlRouterProvider, $locationProvider){
		$locationProvider.hashPrefix('');
		$stateProvider
			.state("inquiries", {
				url: "/inquiries",
				templateUrl: "app/inquiry/inquiry.html",
				data : {requireLogin : true }, 
			})
			.state("addEditInquiry", {
				url: "/addEditInquiry",
				params: {'inquiryId' : null},
				templateUrl: "app/inquiry/addEditInquiry.html",
				data : {requireLogin : true }, 
			})
			.state("tech_inquiries", {
				url: "/tech_inquiries",
				templateUrl: "app/tech_inquiry/tech_inquiry.html",
				data : {requireLogin : true }, 
			})
			.state("addEditTech_inquiry", {
				url: "/addEditTech_inquiry",
				params: {'tech_inquiryId' : null},
				templateUrl: "app/tech_inquiry/addEditTech_inquiry.html",
				data : {requireLogin : true }, 
			})
			.state("uploadInquiryImages", {
				 url: "/inquiries/upload",
				 params: {'inquiryId': null},
				 templateUrl : "app/inquiry/uploadInquiryImages.html",
				 data : {requireLogin : true },
			 })
			/////
			.state("uploadTech_inquiryImages", {
				 url: "/tech_inquiries/upload",
				 params: {'tech_inquiryId': null},
				 templateUrl : "app/tech_inquiry/uploadTech_inquiryImages.html",
				 data : {requireLogin : true },
			 })
			/////
			.state("drivers", {
				 url: "/drivers",
				 templateUrl : "app/driver/driver.html",
				 data : {requireLogin : true },
			 })
			 .state("addEditDriver", {
				 url: "/drivers/add",
				 params: {'driverId': null},
				 templateUrl : "app/driver/addEditDriver.html",
				 data : {requireLogin : true },
			 })
			 .state("uploadDriverImages", {
				 url: "/drivers/upload",
				 params: {'driverId': null},
				 templateUrl : "app/driver/uploadDriverImages.html",
				 data : {requireLogin : true },
			 })
			 .state("workshops", {
				url: "/workshops",
				 templateUrl : "app/workshop/workshop.html",
				 data : {requireLogin : true },
			 })

			 .state("addEditWorkshop", {
				 url: "/workshops/add",
				 params: {'workshopId': null},
				 templateUrl : "app/workshop/addEditWorkshop.html",
				 data : {requireLogin : true },
			 })

			 /////
			 .state("towfixers_inquiries", {
				url: "/towfixers_inquiries",
				 templateUrl : "app/towfixers_inquiry/towfixers_inquiry.html",
				 data : {requireLogin : true },
			 })
			 
			 .state("addEditTowfixers_inquiry", {
				 url: "/towfixers_inquiries/add",
				 params: {'towfixers_inquiryId': null},
				 templateUrl : "app/towfixers_inquiry/addEditTowfixers_inquiry.html",
				 data : {requireLogin : true },
			 })
			 /////
			 .state("companies", {
				url: "/companies",
				 templateUrl : "app/company/company.html",
				 data : {requireLogin : true },
			 })
			 .state("technicians", {
				url: "/technicians",
				 templateUrl : "app/technician/technician.html",
				 data : {requireLogin : true },
			 })
			 .state("addEditTechnician", {
				 url: "/technicians/add",
				 params: {'technicianId': null},
				 templateUrl : "app/technician/addEditTechnician.html",
				 data : {requireLogin : true },
			 })
			 .state("contactUss", {
				url: "/contactUss",
				 templateUrl : "app/contactUs/contactUs.html",
				 data : {requireLogin : true },
			 })
			 .state("addEditCompany", {
				 url: "/companies/add",
				 params: {'companyId': null},
				 templateUrl : "app/company/addEditCompany.html",
				 data : {requireLogin : true },
			 })
			 .state("temp_companies", {
				 url: "/temp_companies",
				 templateUrl : "app/temp_company/temp_company.html",
				 data : {requireLogin : true },
			 })
			 .state("addEditTemp_company", {
				 url: "/temp_companies/add",
				 params: {'temp_companyId': null},
				 templateUrl : "app/temp_company/addEditTemp_company.html",
				 data : {requireLogin : true },
			 })
			 .state("dealers", {
				url: "/dealers",
				 templateUrl : "app/dealer/dealer.html",
				 data : {requireLogin : true },
			 })
			 .state("addEditDealer", {
				 url: "/dealers/add",
				 params: {'dealerId': null},
				 templateUrl : "app/dealer/addEditDealer.html",
				 data : {requireLogin : true },
			 })
			 .state("login", {
				 url: "/login",
				 templateUrl: "app/utility/login/login.html"
			 })
			 .state("forgotPassword", {
				 url: "/forgotPassword",
				 templateUrl: "app/utility/login/forgotPassword.html",
			 })
			 .state("invoice", {
				 url: "/invoice",
				 params: {'inquiry': null},
				 templateUrl: "app/inquiry/generateInvoice.html",
				 data : {requireLogin : true }
			 })
			 $urlRouterProvider.otherwise("login");
	 });

	 app.run(function ($rootScope, $state, $location, $http, loginFactory) {

		var eligibleStates = {
			'Driver': ['login', 'inquiries', 'addEditInquiry', 'uploadInquiryImages', 'workshops'],
			'Company': ['login', 'inquiries', 'addEditInquiry', 'uploadInquiryImages', 'invoice', 'drivers', 'addEditDriver', 'uploadDriverImages', 'workshops', 'addEditWorkshop', 'towfixers_inquiries', 'addEditTowfixers_inquiry'],
			'Dealer': ['login', 'inquiries', 'tech_inquiries', 'addEditTech_inquiry', 'addEditInquiry', 'uploadTech_inquiryImages', 'uploadInquiryImages', 'invoice', 'drivers', 'addEditDriver', 'uploadDriverImages', 'workshops', 'addEditWorkshop', 'companies','contactUss', 'temp_companies', 'dealers', 'technicians', 'addEditTechnician', 'addEditCompany', 'addEditTemp_company', 'addEditDealer'],
			'Master': ['login', 'inquiries', 'addEditInquiry', 'uploadInquiryImages', 'invoice', 'drivers', 'addEditDriver', 'uploadDriverImages', 'workshops', 'addEditWorkshop', 'companies','contactUss', 'temp_companies', 'dealers', 'technicians', 'addEditTechnician', 'addEditCompany', 'addEditTemp_company', 'addEditDealer']
		};

		$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {

			if(fromState.name === "") 
			{
				$http.get('user/session')
					.then(function (response) {
						session = JSON.parse(response.data.data);
						//alert(JSON.stringify(session));
						if(!angular.isString(session.userId) || session.userId === "") {
							loginFactory.isLoggedIn = false;
							$state.go('login');
							event.preventDefault();
							return;
						} else {
							//alert(JSON.stringify(session));
							loginFactory.isLoggedIn = true;
							loginFactory.userName = session.userName;
							loginFactory.role = session.role;
							loginFactory.userId = session.userId;
							loginFactory.dealerId = session.dealerId;
							if(toState.name === "login"){
								if(loginFactory.role === 'Master') {
									$state.go("dealers");
								}
								else if(loginFactory.role === 'Dealer') {
									$state.go("inquiries");
								}
								else {
									$state.go("companies");
								}
								event.preventDefault();
								return;
								
							} 
							else if(eligibleStates[loginFactory.role].indexOf(toState.name) > -1) {
								$state.go(toState.name);
								event.preventDefault();
								return;
							} else {
								event.preventDefault();
								return;
							}
						}
					});
			}
			else 
			{	//alert(JSON.stringify(toState));
				var shouldLogin = toState.data !== undefined
						&& toState.data.requireLogin 
						&& !loginFactory.isLoggedIn ;
				
				if(loginFactory.isLoggedIn === true && toState.name === "login") {
					event.preventDefault();
					return;
				}

				// NOT authenticated - wants any private stuff
				if(shouldLogin)
				{
					$state.go('login');
					event.preventDefault();
					return;
				}

				if(loginFactory.isLoggedIn) 
				{	
					if(eligibleStates[loginFactory.role].indexOf(toState.name) == -1 ) {
						event.preventDefault();
						return;
					}
				}
			}
			
			setPageHeading(toState && toState.name);
		});
		
		function setPageHeading(stateName) {
			//alert(JSON.stringify(session.role));
			if(stateName === 'login') {
				loginFactory.pageHeading = '';
				loginFactory.pageHeadColor = '';
			} else if(stateName === 'inquiries' || stateName === 'addEditInquiry' || stateName === 'uploadInquiryImages') {
				loginFactory.pageHeading = 'Inquiries';
				loginFactory.pageHeadColor = '';
			} else if(stateName === 'tech_inquiries' || stateName === 'addEditTech_inquiry') {
				loginFactory.pageHeading = 'Tech Inquiries';
				loginFactory.pageHeadColor = '';
			} else if(stateName === 'drivers' || stateName === 'addEditDriver' || stateName === 'uploadDriverImages') {
				loginFactory.pageHeading = 'Drivers';
				loginFactory.pageHeadColor = '';
			} else if (stateName === 'workshops' || stateName === 'addEditWorkshop') {
				loginFactory.pageHeading = 'Workshops';
				loginFactory.pageHeadColor = '';
			}  else if (stateName === 'towfixers_inquiries' || stateName === 'addEditTowfixers_inquiry') {
				loginFactory.pageHeading = 'Towfixers inquiries';
				loginFactory.pageHeadColor = 'background:#c50101';
			}  else if (stateName === 'companies' || stateName === 'addEditCompany') {
				loginFactory.pageHeading = 'Vendors';
				loginFactory.pageHeadColor = '';
			}  else if (stateName === 'temp_companies' || stateName === 'addEditTemp_company') {
				loginFactory.pageHeading = 'Tmp Vendors';
				loginFactory.pageHeadColor = '';
			}  else if (stateName === 'dealers' || stateName === 'addEditDealer') {
				loginFactory.pageHeading = 'Dealers';
				loginFactory.pageHeadColor = '';
			}  else if (stateName === 'technicians' || stateName === 'addEditTechnician') {
				loginFactory.pageHeading = 'Technicians';
				loginFactory.pageHeadColor = '';
			}  else if (stateName === 'contactUss') {
				loginFactory.pageHeading = 'Contact Us';
				loginFactory.pageHeadColor = '';
			}
		}
	});
})(window.angular);