(function (angular){

    angular
        .module('gmCarFixers')
        .controller('home.controller', HomeController)

    HomeController.$inject = ['$scope', '$state', '$browser', '$mdSidenav', 'login.service', 'loginFactory'];

    function HomeController($scope, $state, $browser, $mdSidenav, loginService, loginFactory) {
        var vm = this;

        vm.loginFactory = loginFactory;
        vm.toggleSideNav = toggleSideNav;
        vm.openMenu = openMenu;
        vm.goToHome = goToHome;
        vm.page = 'Inquiry';
        vm.logout = logout;
        
        activate();

		// ***************************************** //

        function activate(){
        }

        function goToHome() {
        	if(vm.loginFactory.role === 'Master') {
				$state.go("dealers");
			}
            else if(vm.loginFactory.role === 'Dealer' || vm.loginFactory.role === 'Driver') {
                $state.go("inquiries");
            }
            else if(vm.loginFactory.role === 'Technician'){
                $state.go("tech_inquiries");
            }
            else if(vm.loginFactory.role === 'Company'){
                $state.go("towfixers_inquiries");
            }
            else {
				$state.go("companies", {});
			}
        }
        
        function logout() {
            loginService.logout()
                .then(function (response) {
                    loginFactory.isLoggedIn = false;
                    loginFactory.role = '';
                    $state.go('login', {});

                    console.log(response.data);
                }, function (error) {
                    console.log(error);
                });
        }

        function toggleSideNav(menuId, page) {
        	
        	if(angular.isString(page)) {
        		if(page === 'Driver') {
        			$state.go('drivers', {});
        		} else if(page === 'Company') {
        			$state.go('companies', {});
                } else if(page === 'Temp_company') {
                    $state.go('temp_companies', {});
        		} else if(page === 'Dealer') {
                    $state.go('dealers', {});
                } else if(page === 'Workshop') {
        			$state.go('workshops', {});
        		}  else if(page === 'Towfixers_inquiry') {
                    $state.go('towfixers_inquiries', {});
                } else if(page === 'Inquiry') {
        			$state.go('inquiries', {});
        		} else if(page === 'Tech_inquiry') {
                    $state.go('tech_inquiries', {});
                } else if(page === 'Technician') {
                    $state.go('technicians', {});
                } else if(page === 'ContactUs') {
                    $state.go('contactUss', {});
                }

        	}
        	
            $mdSidenav(menuId).toggle();            
        }

        function openMenu($mdMenu, ev) {
           originatorEv = ev;
           $mdMenu.open(ev);
        }

        $scope.$on('login', function (event, args) {
            vm.loginFactory = loginFactory;
            //alert(JSON.stringify(vm.loginFactory));
        });
    }
})(window.angular);