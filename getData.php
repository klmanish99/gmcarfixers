<?php

include 'api/dao/config.php';

$data = json_decode(file_get_contents("php://input"));
//echo "<pre>"; print_r($data);exit;
if ($data->role == 'Master') {
	$sel = mysqli_query($con,"select * from company where ind_active = 1 ");
	$data = array();

	while ($row = mysqli_fetch_array($sel)) {
	    $data[] = array("id"=>$row['idn_company'], "name"=>$row['nme_company']);
	}
}
elseif ($data->role == 'Dealer') {
	$sel = mysqli_query($con,"SELECT * FROM `company` c LEFT JOIN company_to_dealer cd ON (c.idn_company = cd.company_id) WHERE cd.dealer_id = '".$data->user_id."' AND `ind_active` = 1 ");
	$data = array();

	while ($row = mysqli_fetch_array($sel)) {
	    $data[] = array("id"=>$row['idn_company'], "name"=>$row['nme_company']);
	}
}
elseif ($data->role == 'Technician') {
	$query = mysqli_query($con,"SELECT `dealer_id` FROM `technician` WHERE idn_user = '".$data->user_id."' ");
	$row = mysqli_fetch_array($query);
	$dealerId = $row['dealer_id'];
	$sel = mysqli_query($con,"SELECT * FROM `company` c LEFT JOIN company_to_dealer cd ON (c.idn_company = cd.company_id) WHERE cd.dealer_id = '".$dealerId."' AND `ind_active` = 1 ");
	$data = array();

	while ($row = mysqli_fetch_array($sel)) {
	    $data[] = array("id"=>$row['idn_company'], "name"=>$row['nme_company']);
	}
}
echo json_encode($data);
